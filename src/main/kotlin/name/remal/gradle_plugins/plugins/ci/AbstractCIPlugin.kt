package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.ApplyOnRootProjectMixin

@ApplyPluginClasses(CommonCIPlugin::class)
abstract class AbstractCIPlugin : BaseReflectiveProjectPlugin(), ApplyOnRootProjectMixin
