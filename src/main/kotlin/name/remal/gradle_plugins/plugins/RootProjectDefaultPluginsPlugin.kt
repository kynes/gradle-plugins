package name.remal.gradle_plugins.plugins

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPluginClassesAtTheEnd
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.CurrentProjectIsARootProjectMixin
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.environment_variables.EnvironmentVariablesPlugin

@Plugin(
    id = "name.remal.root-project-default-plugins",
    description = "Plugin that applies many different plugins by default to the root project. See documentation for full applied plugins list.",
    tags = ["common", "default"]
)
@ApplyPluginClasses(
    CommonSettingsPlugin::class,
    EnvironmentVariablesPlugin::class
)
@ApplyPluginClassesAtTheEnd(DefaultPluginsPlugin::class)
class RootProjectDefaultPluginsPlugin : BaseReflectiveProjectPlugin(), CurrentProjectIsARootProjectMixin
