package name.remal.gradle_plugins.dsl.extensions

import name.remal.createParentDirectories
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.gradle_plugins.testing.dsl.evaluate
import name.remal.newTempDir
import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.file.RelativePath
import org.gradle.api.tasks.SourceSet
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import java.io.File
import java.util.Random

class ProjectExtensionsTest : BaseProjectTest() {

    @Test
    fun newRegularFileProperty() {
        val property = project.newRegularFileProperty()
        val file = project.file("file")
        property.set(project.layout.file(project.provider { File("file") }))
        val propertyFile = property.asFile.get()
        assertEquals(file, propertyFile)
    }

    @Test
    fun newDirectoryProperty() {
        val property = project.newDirectoryProperty()
        val file = project.file("dir")
        property.set(project.provider { project.layout.projectDirectory.dir("dir") })
        val propertyFile = property.asFile.get()
        assertEquals(file, propertyFile)
    }

    @Test
    fun executeSetupActions() {
        val list = mutableListOf<Int>()
        project.afterEvaluateOrNow(5) { _ -> list.add(5) }
        project.afterEvaluateOrNow(4) { _ -> list.add(4) }
        project.setupTasksAfterEvaluateOrNow(3) { _ -> list.add(3) }
        project.setupTasksDependenciesAfterEvaluateOrNow(2) { _ -> list.add(2) }
        project.atTheEndOfAfterEvaluationOrNow(1) { _ -> list.add(1) }
        project.evaluate()
        assertThat(list).containsExactly(4, 5, 3, 2, 1)
    }

    @Test
    fun containerWithFactory() {
        project.applyPlugin("java")
        val container = project.containerWithFactory(SourceSet::class.java, project.java.sourceSets::create)
        container.configure { it.create("tempSourceSet") }
    }


    @Test
    fun randomizeFileCollection() {
        val files = (1..9).map { project.file("file$it") }
        val shuffledFiles = files.shuffled(Random(1))
        val result = project.randomizeFileCollection(project.files(files), Random(1)).files.toList()
        assertEquals(shuffledFiles, result)
    }

    @Test
    fun `randomizeFileTree - FileTree`() {
        val fileTree = project.files(createTempDirForTree(), createTempDirForTree()).asFileTree
        val shuffledFiles = fileTree.files.filter(File::isFile).shuffled(Random(1))
        val shuffledRelativePaths = mutableListOf<RelativePath>().apply {
            fileTree.visit { if (it.isFile) add(it.relativePath) }
            shuffle(Random(1))
        }

        val result = project.randomizeFileTree(fileTree, Random(1))
        val resultFiles = result.files.filter(File::isFile)
        assertEquals(shuffledFiles, resultFiles)
        val resultRelativePaths = mutableListOf<RelativePath>().apply {
            result.visit {
                if (it.isFile) add(it.relativePath)
            }
        }
        assertEquals(shuffledRelativePaths, resultRelativePaths)
    }

    @Test
    fun `randomizeFileTree - FileCollection`() {
        val fileCollection = project.files(createTempDirForTree(), createTempDirForTree())
        val shuffledRelativePaths = mutableListOf<RelativePath>().apply {
            fileCollection.asFileTree.visit { if (it.isFile) add(it.relativePath) }
            shuffle(Random(1))
        }

        val result = project.randomizeFileTree(fileCollection, Random(1))
        assertNotEquals(fileCollection.asFileTree.files.filter(File::isFile), result.files.filter(File::isFile))
        val resultRelativePaths = mutableListOf<RelativePath>().apply {
            result.visit { if (it.isFile) add(it.relativePath) }
        }
        assertEquals(shuffledRelativePaths, resultRelativePaths)
    }


    private fun createTempDirForTree(): File {
        return newTempDir().apply {
            resolve("1/1").createParentDirectories().createNewFile()
            resolve("1/2").createParentDirectories().createNewFile()
            resolve("1/3").createParentDirectories().createNewFile()
            resolve("2/1").createParentDirectories().createNewFile()
        }
    }

}
