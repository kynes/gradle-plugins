package name.remal.gradle_plugins.api.packed_dependencies;

import java.util.List;

public interface PackedDependencyInfo {

    String getGroup();

    String getModule();

    String getVersion();

    List<String> getResourceNames();

}
