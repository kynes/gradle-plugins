This plugin executes [Detekt](https://arturbosch.github.io/detekt/) inspections for Kotlin sources. Detekt's core JARs and plugins' JARs are downloaded via `detektCore` and `detektPlugins` configurations accordingly.

The plugin creates `detekt` extension of type `DetektExtension` (it extends [`CodeQualityExtension`](https://docs.gradle.org/current/javadoc/org/gradle/api/plugins/quality/CodeQualityExtension.html)) that allows to configure `detekt*` tasks globally.
