The plugin applies [`name.remal.classes-processing`](name.remal.classes-processing.md) plugin.

&nbsp;

This plugin checks that all compiled classes don't use prohibited classes. To register prohibited classes use `prohibitedClasses` extension of type [`ProhibitedClassesExtension`](#nameremalgradle_pluginspluginsprohibited_classesprohibitedclassesextension).

You can use full class names either patterns:

* `pkg.SomeClass` forbids using `pkg.SomeClass` class
* `pkg.*` forbids using all classes from `pkg` package and all sub-packages
* `pkg.SomeClass*` forbids using `pkg.SomeClass` class and all classes those names start with `pkg.SomeClass` (i.e. `pkg.SomeClass$Inner`)

Also you can prohibit all classes from dependencies. Use `module` method: `module("junit:junit")` prohibits all classes of JUnit 4.

&nbsp;

### `name.remal.gradle_plugins.plugins.prohibited_classes.ProhibitedClassesExtension`
| Property | Type | Description
| --- | :---: | --- |
| `classNames` | <nobr>`MutableSet<String>`</nobr> | Prohibited class names and patterns. |
| `modules` | <nobr><code>MutableMap&lt;String, <a href="#nameremalgradle_pluginspluginsprohibited_classesprohibitedmodule">ProhibitedModule</a>&gt;</code></nobr> | Prohibited class names and patterns. |

| Method | Description
| --- | --- |
| `void add(String className)` | Add class name or pattern in `classNames`. |
| `void add(String... classNames)` | Add class names and patterns in `classNames`. |
| `void add(Iterable<String> classNames)` | Add class names and patterns in `classNames`. |
| `void module(Any notation)` | Prohibits all classes from corresponding to the notation dependency. |
| `void module(Any notation, Action<ProhibitedModule> configurer)` | Prohibits all classes from corresponding to the notation dependency. And allows to configure prohibiting process. |
| `void module(Any notation, Closure configurer)` | Prohibits all classes from corresponding to the notation dependency. And allows to configure prohibiting process. |

### `name.remal.gradle_plugins.plugins.prohibited_classes.ProhibitedModule`
| `permittedClassNames` | <nobr>`MutableSet<String>`</nobr> | Permitted class names and patterns within the module. |

| Method | Description
| --- | --- |
| `void permit(String className)` | Add class name or pattern in `permittedClassNames`. |
| `void permit(String... classNames)` | Add class names and patterns in `permittedClassNames`. |
| `void permit(Iterable<String> classNames)` | Add class names and patterns in `permittedClassNames`. |
