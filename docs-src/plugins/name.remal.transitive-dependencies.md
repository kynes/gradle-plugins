The plugin applies [`name.remal.common-settings`](name.remal.common-settings.md) plugin.

&nbsp;

<a id="transitive-dependencies"></a>
The plugin adds `transitiveDependencies` extension of type [`TransitiveDependenciesExtension`](#nameremalgradle_pluginspluginsdependenciestransitivedependenciesextension).

&nbsp;

### `name.remal.gradle_plugins.plugins.dependencies.TransitiveDependenciesExtension`
| Method | Description
| --- | --- |
| `void exclude(Map<String, String?> excludeProperties)` | Exclude transitive dependencies. Groovy example: `#!groovy exclude(group = "com.group", module = "module")` |
| `void exclude(String? group, String? module)` | Exclude transitive dependencies. |
| `void excludeStaticAnalysisTools()` | Exclude static analysis transitive dependencies. |
