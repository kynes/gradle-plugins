package name.remal.gradle_plugins.plugins.publish.nexus_staging

import java.time.ZonedDateTime

data class NexusStagingRepositoryAction(
    val name: String,
    val started: ZonedDateTime,
    val stopped: ZonedDateTime? = null,
    val events: List<NexusStagingRepositoryEvent> = emptyList()
)
