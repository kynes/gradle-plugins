package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.convention
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler

@Plugin(
    id = "name.remal.dependencies-filter",
    description = "Plugin that provides dependencies 'filter' extension",
    tags = ["dependencies"]
)
class DependenciesFilterPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction(isHidden = true)
    fun DependencyHandler.filterExtension(project: Project) {
        convention.addPlugin("name.remal.dependencies-filter", DependencyHandlerFilterExtension(this, pluginCacheDir, project))
    }

}
