package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME
import org.gradle.api.tasks.SourceSet.TEST_SOURCE_SET_NAME
import org.gradle.api.tasks.SourceSetContainer

val SourceSetContainer.main: SourceSet get() = this[MAIN_SOURCE_SET_NAME]
val SourceSetContainer.test: SourceSet get() = this[TEST_SOURCE_SET_NAME]
