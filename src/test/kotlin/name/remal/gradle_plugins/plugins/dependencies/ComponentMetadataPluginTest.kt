package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_10
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.createWithUniqueName
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.gradle_plugins.testing.dsl.TEST_MAVEN_REPO_DEFAULT_VERSION
import name.remal.gradle_plugins.testing.dsl.testMavenRepository
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.gradle.util.GradleVersion
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ComponentMetadataPluginTest : BaseProjectTest() {

    private lateinit var conf: Configuration

    @Before
    fun before() {
        project.applyPlugin(ComponentMetadataPlugin::class.java)

        project.testMavenRepository {
            component("org.hibernate", "hibernate-validator", "1") { jar() }
            component("org.hibernate", "hibernate-validator", "2") { jar() }
            component("org.hibernate", "hibernate-validator-artifact", "2") { jar() }
        }

        conf = project.configurations.createWithUniqueName()
    }


    @Test
    fun hibernateValidator() {
        addDependency("org.hibernate", "hibernate-validator", "1")
        addDependency("org.hibernate", "hibernate-validator-artifact", "2")

        if (GradleVersion.current() < GRADLE_VERSION_4_10) {
            assertEquals(2, resolvedArtifacts.size)

            assertEquals("org.hibernate", resolvedArtifacts[0].group)
            assertEquals("hibernate-validator", resolvedArtifacts[0].module)
            assertEquals("1", resolvedArtifacts[0].version)

            assertEquals("org.hibernate", resolvedArtifacts[1].group)
            assertEquals("hibernate-validator-artifact", resolvedArtifacts[1].module)
            assertEquals("2", resolvedArtifacts[1].version)

        } else {
            assertEquals(2, resolvedArtifacts.size)

            assertEquals("org.hibernate", resolvedArtifacts[0].group)
            assertEquals("hibernate-validator", resolvedArtifacts[0].module)
            assertEquals("2", resolvedArtifacts[0].version)

            assertEquals("org.hibernate", resolvedArtifacts[1].group)
            assertEquals("hibernate-validator-artifact", resolvedArtifacts[1].module)
            assertEquals("2", resolvedArtifacts[1].version)
        }
    }


    private fun addDependency(group: String, module: String, version: String = TEST_MAVEN_REPO_DEFAULT_VERSION) {
        conf.dependencies.add(project.dependencies.create("$group:$module:$version"))
    }

    private val ResolvedArtifact.group get() = (id.componentIdentifier as ModuleComponentIdentifier).group
    private val ResolvedArtifact.module get() = (id.componentIdentifier as ModuleComponentIdentifier).module
    private val ResolvedArtifact.version get() = (id.componentIdentifier as ModuleComponentIdentifier).version

    private val resolvedArtifacts: List<ResolvedArtifact>
        get() = conf.resolvedConfiguration.resolvedArtifacts.sortedWith(Comparator { a1, a2 ->
            a1.group.compareTo(a2.group).let { if (it != 0) return@Comparator it }
            a1.module.compareTo(a2.module).let { if (it != 0) return@Comparator it }
            a1.version.compareTo(a2.version).let { if (it != 0) return@Comparator it }
            return@Comparator 0
        })

}
