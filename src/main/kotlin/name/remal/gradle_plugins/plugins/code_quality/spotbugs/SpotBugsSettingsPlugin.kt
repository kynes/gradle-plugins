package name.remal.gradle_plugins.plugins.code_quality.spotbugs

import com.github.spotbugs.SpotBugsExtension
import com.github.spotbugs.SpotBugsTask
import name.remal.gradle_plugins.dsl.DoNotGenerateSimpleTest
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.plugins.code_quality.findbugs.BaseFindBugsSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.utils.getVersionProperty
import org.gradle.api.file.FileCollection
import org.gradle.api.model.ObjectFactory
import java.io.File
import javax.inject.Inject

@Plugin(
    id = "name.remal.spotbugs-settings",
    description = "Plugin that configures 'com.github.spotbugs' plugin if it's applied.",
    tags = ["java", "spotbugs"]
)
@WithPlugins(SpotBugsPluginId::class, JavaPluginId::class)
@DoNotGenerateSimpleTest
class SpotBugsSettingsPlugin @Inject constructor(
    objectFactory: ObjectFactory
) : BaseFindBugsSettingsPlugin<SpotBugsExtension, SpotBugsTask>(objectFactory) {

    override val toolName: String = "SpotBugs"

    override val toolLatestVersion: String get() = project.getVersionProperty("spotbugs")

    override val toolArtifactGroup: String = "com.github.spotbugs"
    override val toolArtifactIds: Set<String> = setOf("spotbugs", "spotbugs-annotations")

    override var SpotBugsTask.excludeFilterFile: File?
        get() = excludeFilter
        set(value) {
            excludeFilter = value
        }

    override val SpotBugsTask.classesDirs: FileCollection get() = classes

}
