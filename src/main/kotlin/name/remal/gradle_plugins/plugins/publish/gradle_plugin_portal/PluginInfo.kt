package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

import org.gradle.api.Named

@Suppress("ConstructorParameterNaming")
class PluginInfo(private val _name: String) : Named {

    var id: String? = _name

    var displayName: String? = null

    var description: String? = null

    var tags: MutableSet<String> = mutableSetOf()

    var websiteUrl: String? = null

    var vcsUrl: String? = null


    override fun getName() = _name

}
