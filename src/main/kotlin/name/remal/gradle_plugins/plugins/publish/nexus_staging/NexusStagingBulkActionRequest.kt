package name.remal.gradle_plugins.plugins.publish.nexus_staging

data class NexusStagingBulkActionRequest(
    val stagedRepositoryIds: List<String>,
    val description: String = "",
    val autoDropAfterRelease: Boolean = true
)
