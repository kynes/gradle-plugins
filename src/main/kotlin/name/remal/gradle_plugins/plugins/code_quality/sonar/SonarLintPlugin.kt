package name.remal.gradle_plugins.plugins.code_quality.sonar

import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperties
import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperty
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.SimpleTestAdditionalGradleScript
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.dependsOn
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.findDependencyConstraintVersion
import name.remal.gradle_plugins.dsl.extensions.includeGroupByRegex
import name.remal.gradle_plugins.dsl.extensions.includeModule
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.dsl.extensions.maven
import name.remal.gradle_plugins.dsl.extensions.mavenCentralOrJCenterIfNotAdded
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.gradle_plugins.dsl.utils.parseDependencyNotation
import name.remal.gradle_plugins.plugins.code_quality.BaseCodeQualityReflectiveProjectPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.kotlin.KotlinAnyPluginId
import name.remal.gradle_plugins.utils.findVersionProperty
import name.remal.gradle_plugins.utils.getVersionProperty
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskContainer
import java.io.File

@Plugin(
    id = "name.remal.sonarlint",
    description = "Plugin that executes SonarLint checks without SonarQube server.",
    tags = ["sonar", "sonarlint"]
)
@SimpleTestAdditionalGradleScript(
    """
    sonarlint {
        ignoreFailures = false
    }

    sourceSets.all { sourceSet ->
        tasks[sourceSet.getTaskName('sonarlint', null)].configure {
            ignoreFailures = true
            doLast {
                File reportFile = project.file("build/reports/sonarlint/${'$'}{it.name}/findBugs.xml")
                if (!reportFile.exists()) {
                    throw new GradleException("${'$'}reportFile doesn't exist")
                }
                String reportContent = reportFile.text.trim()
                if (reportContent.isEmpty()) {
                    throw new GradleException("Report content is empty: ${'$'}reportFile")
                }
                List<String> bugTypes = reportContent
                    .findAll(/<BugInstance [^>]*type="([^"]+)"[^>]*>/) { match, type -> type }
                    .unique()
                if (bugTypes.size() == 0
                    || bugTypes.size() >= 2
                    || (bugTypes.size() == 1
                            && bugTypes.get(0) != 'java:S2094'
                            && bugTypes.get(0) != 'squid:S2094' // Deprecated bug ID
                    )
                ) {
                    throw new GradleException(
                        "Report content should have bug type 'java:S2094' and no other bug types: ${'$'}reportFile"
                    )
                }
            }
        }
    }
"""
)
class SonarLintPlugin : BaseCodeQualityReflectiveProjectPlugin<SonarLint, SonarLintExtension>() {

    companion object {
        private val SONAR_PLUGIN_VERSIONS = getStringProperties("sonar-*-plugin.version")
        private val SONAR_PLUGIN_NOTATIONS = getStringProperties("sonar-*-plugin.dependency-notation")
    }


    override val toolVersion: String
        get() {
            val constraintVersion = project.configurations.asSequence()
                .filter { it.name == configurationName }
                .mapNotNull { it.findDependencyConstraintVersion("org.sonarsource.sonarlint.core", "sonarlint-core") }
                .firstOrNull()
            if (constraintVersion != null) {
                return constraintVersion
            }

            return project.findVersionProperty("sonarlint")
                ?: project.getVersionProperty("sonarlint-core")
        }

    override val isToolPluginsConfigurationEnabled: Boolean get() = true


    @PluginAction
    fun RepositoryHandler.`Add SonarSource repository`() {
        val repositoryUrl = project.findProperty("sonarsource.repository").unwrapProviders()?.toString().nullIfEmpty()
            ?: getStringProperty("sonarsource.repository")
        maven("SonarSource", repositoryUrl) {
            it.includeGroupByRegex("org\\.sonarsource(\\..+)?")
        }

        mavenCentralOrJCenterIfNotAdded("SonarLint dependencies") {
            it.includeModule("commons-lang", "commons-lang")
            it.includeModule("com.google.code.findbugs", "jsr305")
        }
    }


    override fun Configuration.configureConfiguration(extension: SonarLintExtension, dependencyHandler: DependencyHandler) {
        defaultDependencies { dependencies ->
            val toolVersion = extension.toolVersion
            dependencies.add(dependencyHandler.createToolDependency("org.sonarsource.sonarlint.core:sonarlint-core:$toolVersion"))
            dependencies.add(dependencyHandler.create("commons-lang:commons-lang:2.6"))
            dependencies.add(dependencyHandler.create("com.google.code.findbugs:jsr305:3.0.2"))
        }
    }


    @PluginAction(order = -90)
    @WithPlugins(JavaPluginId::class)
    fun ConfigurationContainer.`Add java Sonar plugin dependency`(project: Project, dependencyHandler: DependencyHandler) {
        addSonarPluginDependency(project, dependencyHandler, "java")
    }

    @PluginAction(order = -90)
    @WithPlugins(KotlinAnyPluginId::class)
    fun ConfigurationContainer.`Add kotlin Sonar plugin dependency`(project: Project, dependencyHandler: DependencyHandler) {
        addSonarPluginDependency(project, dependencyHandler, "kotlin")
    }

    @PluginAction(order = -90)
    fun ConfigurationContainer.`Add html, javascript, xml Sonar plugin dependencies`(project: Project, dependencyHandler: DependencyHandler) {
        addSonarPluginDependency(project, dependencyHandler, "html", "javascript", "xml")
    }


    @PluginAction
    fun TaskContainer.`Configure SonarLint tasks' Sonar classpath`(configurations: ConfigurationContainer) {
        all(SonarLint::class.java) { task ->
            task.sonarClasspath = configurations.toolConfiguration
            task.sonarPluginsClasspath = configurations.toolPluginsConfiguration
        }
    }

    override fun SonarLint.configureTaskForSourceSet(sourceSet: SourceSet, isTest: Boolean) {
        source(project.provider { sourceSet.allSource })

        if (isTest) {
            isTestSources = true
        }

        val sourceSets = project.java.sourceSets
        doSetup(Int.MIN_VALUE) {
            sourceSet.output.files.filter(File::exists).map(File::getAbsoluteFile).joinToString(",").nullIfEmpty()?.also { sonarProperty("sonar.java.binaries", it) }
            sourceSet.compileClasspath.files.filter(File::exists).map(File::getAbsoluteFile).joinToString(",").nullIfEmpty()?.also { sonarProperty("sonar.java.libraries", it) }

            if (isTest) {
                isTestSources = true
                sonarProperty("sonar.java.test.binaries", sonarProperties["sonar.java.binaries"])
                sonarProperty("sonar.java.test.libraries", sonarProperties["sonar.java.libraries"])

            } else {
                val allBinaries = mutableSetOf<File>()
                val allLibraries = mutableSetOf<File>()
                sourceSets.filter { it.isTest }.forEach {
                    it.output.files.filter(File::exists).mapTo(allBinaries, File::getAbsoluteFile)
                    it.compileClasspath.files.filter(File::exists).mapTo(allLibraries, File::getAbsoluteFile)
                }
                allBinaries.joinToString(",").nullIfEmpty()?.also { sonarProperty("sonar.java.test.binaries", it) }
                allLibraries.joinToString(",").nullIfEmpty()?.also { sonarProperty("sonar.java.test.libraries", it) }
            }
        }

        dependsOn {
            sourceSets.asSequence()
                .filter { it.isTest }
                .mapNotNull(SourceSet::getClassesTaskName)
                .toList()
        }
    }


    private fun ConfigurationContainer.addSonarPluginDependency(project: Project, dependencyHandler: DependencyHandler, vararg sonarPlugins: String) {
        sonarPlugins.forEach { sonarPlugin ->
            val version = "sonar-$sonarPlugin-plugin.version".let { property ->
                project.findProperty(property).unwrapProviders()?.toString().nullIfEmpty()
                    ?: SONAR_PLUGIN_VERSIONS[property]
                    ?: throw IllegalStateException("Last version isn't defined for '$sonarPlugin' Sonar plugin")
            }

            val notation = parseDependencyNotation(
                SONAR_PLUGIN_NOTATIONS["sonar-$sonarPlugin-plugin.dependency-notation"]
                    ?: throw IllegalStateException("Notation isn't defined for '$sonarPlugin' Sonar plugin")
            ).withVersion(version)

            val dependency = dependencyHandler.create(notation.toString())
            toolPluginsConfiguration.dependencies.add(dependency)
        }
    }

}


val SourceSet.sonarlintTaskName: String get() = getTaskName("sonarlint", null)
