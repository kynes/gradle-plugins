package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

import name.remal.SetBuilder
import name.remal.buildSet
import name.remal.gradle_plugins.dsl.PluginId
import name.remal.gradle_plugins.dsl.ProjectPluginClass
import org.gradle.util.GradleVersion

data class PluginInfo(
    val pluginClass: ProjectPluginClass,
    val id: String,
    val isHidden: Boolean = false,
    val description: String = "",
    val tags: Set<String> = setOf(),
    override val conditionMethods: List<ConditionMethodInfo> = listOf(),
    override val minGradleVersion: GradleVersion? = null,
    override val maxGradleVersion: GradleVersion? = null,
    override val requirePluginIds: Set<PluginId> = setOf(),
    override val requireOptionalPluginIds: Set<PluginId> = setOf(),
    override val applyPluginIds: Set<PluginId> = setOf(),
    override val applyOptionalPluginIds: Set<PluginId> = setOf(),
    override val applyPluginClasses: Set<ProjectPluginClass> = setOf(),
    override val applyPluginIdsAtTheEnd: Set<PluginId> = setOf(),
    override val applyOptionalPluginIdsAtTheEnd: Set<PluginId> = setOf(),
    override val applyPluginClassesAtTheEnd: Set<ProjectPluginClass> = setOf(),
    override val actionMethods: List<ActionMethodInfo> = listOf(),
    override val actionsGroups: List<ActionsGroupInfo> = listOf()
) : WithPluginActions {

    override val actions: List<ActionInfo> = super.actions

    val allRequirePluginIds: Set<PluginId> = buildSet { collectAllRequirePluginIds(this, this@PluginInfo) }

    private fun collectAllRequirePluginIds(result: SetBuilder<PluginId>, context: WithRequirements) {
        result.addAll(context.requirePluginIds)

        if (context is WithPluginActions) {
            context.actionMethods.forEach { collectAllRequirePluginIds(result, it) }
            context.actionsGroups.forEach { collectAllRequirePluginIds(result, it) }
        }
    }

}
