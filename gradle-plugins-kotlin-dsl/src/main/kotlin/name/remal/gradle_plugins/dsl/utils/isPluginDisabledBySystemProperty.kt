package name.remal.gradle_plugins.dsl.utils

import name.remal.gradle_plugins.dsl.PluginId

fun isPluginDisabledBySystemProperty(pluginId: PluginId): Boolean {
    return pluginId.allIds.any {
        System.getProperty("plugin-disabled.$it")?.toString()?.toBoolean() == true
    }
}

fun isPluginDisabledBySystemProperty(pluginId: String) = isPluginDisabledBySystemProperty(PluginId(pluginId))
