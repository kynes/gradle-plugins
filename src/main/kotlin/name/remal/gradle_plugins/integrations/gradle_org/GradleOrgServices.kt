package name.remal.gradle_plugins.integrations.gradle_org

import retrofit2.http.GET

interface GradleOrgServices {

    @GET("versions/current")
    fun getCurrentVersion(): VersionInfo

    @GET("versions/all")
    fun getAllVersions(): List<VersionInfo>

}
