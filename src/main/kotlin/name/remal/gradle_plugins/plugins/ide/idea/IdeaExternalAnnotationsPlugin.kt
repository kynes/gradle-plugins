package name.remal.gradle_plugins.plugins.ide.idea

import name.remal.getOrCreateChild
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.CurrentProjectIsARootProjectMixin
import name.remal.gradle_plugins.plugins.ci.SkipForBuildOnCIMixin
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer

@Plugin(
    id = "name.remal.idea-external-annotations",
    description = "Plugin that adds external annotations default link (file://\$USER_HOME\$/idea-external-annotations) to IDEA project libraries.",
    tags = ["idea"]
)
@WithPlugins(IdeaPluginId::class)
@ApplyPluginClasses(IdeaSettingsPlugin::class)
class IdeaExternalAnnotationsPlugin : BaseReflectiveProjectPlugin(), SkipForBuildOnCIMixin {

    @PluginAction(order = Int.MIN_VALUE)
    fun Project.`Apply this plugin for all projects`() {
        rootProject.allprojects {
            it.applyPlugin(IdeaExternalAnnotationsPlugin::class.java)
        }
    }

    @PluginActionsGroup
    inner class `For root project` : CurrentProjectIsARootProjectMixin {

        @PluginAction
        fun ExtensionContainer.addExternalAnnotationsDefaultLink() {
            this[IdeaSettings::class.java].configureIDEAComponent("libraryTable", "libraries/", Action { componentElement ->
                componentElement.getChildren("library").forEach { libraryElement ->
                    val annotationsRootElement = libraryElement.getOrCreateChild("ANNOTATIONS").getOrCreateChild("root")
                    if (null == annotationsRootElement.getAttribute("url")) {
                        annotationsRootElement.setAttribute("url", "file://\$USER_HOME\$/idea-external-annotations")
                    }
                }
            })
        }

        @PluginAction
        fun ExtensionContainer.disableExternalStorageConfigurationManagerComponent() {
            this[IdeaSettings::class.java].removeIDEAComponent("ExternalStorageConfigurationManager")
        }

    }

}
