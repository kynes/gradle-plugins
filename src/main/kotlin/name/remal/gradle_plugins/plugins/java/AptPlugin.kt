package name.remal.gradle_plugins.plugins.java

import name.remal.createDirectories
import name.remal.default
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPlugins
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateConfigurationsPluginAction
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.ExtensionProperty
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_6
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPluginClasses
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.generatedSourcesDir
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.ide.idea.IdeaSettings
import name.remal.gradle_plugins.plugins.ide.idea.IdeaSettingsPlugin
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.util.GradleVersion

private const val APT_ONLY_CONFIGURATION_NAME = "aptOnly"
private const val APT_CONFIGURATION_NAME = "apt"
private const val APT_OPTIONS_EXTENSION_NAME = "aptOptions"

@Plugin(
    id = "name.remal.apt",
    description = "Plugin that makes easier to use Java annotation processors.",
    tags = ["java", "apt", "annotation-processing"]
)
@ApplyPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class)
class AptPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'aptOptions' extension`(): AptOptions = create(APT_OPTIONS_EXTENSION_NAME, AptOptions::class.java)

    @CreateConfigurationsPluginAction
    fun ConfigurationContainer.`Create 'apt' configurations`(sourceSets: SourceSetContainer) {
        val apt = create(APT_CONFIGURATION_NAME) { conf ->
            conf.description = "Java annotation-processing and compile-only dependencies"
            sourceSets.all { sourceSet ->
                this[sourceSet.compileOnlyConfigurationName].extendsFrom(conf)
            }
        }

        create(APT_ONLY_CONFIGURATION_NAME) { conf ->
            conf.extendsFrom(apt)
            conf.description = "Java annotation-processing dependencies not presented in classpath"
            sourceSets.all { sourceSet ->
                if (GradleVersion.current() >= GRADLE_VERSION_4_6) {
                    this[sourceSet.annotationProcessorConfigurationName].extendsFrom(conf)
                } else {
                    this[sourceSet.compileOnlyConfigurationName].extendsFrom(conf)
                }
            }
        }
    }

    @PluginAction
    fun SourceSetContainer.`Setup a dir for generated sources by annotation-processors for 'compileJava' task for all SourceSet`(tasks: TaskContainer) {
        all { sourceSet ->
            tasks.all(JavaCompile::class.java, sourceSet.compileJavaTaskName) { task ->
                val generatedSourcesDir = task.project.generatedSourcesDir.resolve("java-apt/${sourceSet.name}").absoluteFile
                task.options.annotationProcessorGeneratedSourcesDirectory = generatedSourcesDir
                sourceSet.java.srcDir(generatedSourcesDir)

                task.doFirst { generatedSourcesDir.createDirectories() }
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Setup annotation-processor args for all 'compileJava' tasks`() {
        all(JavaCompile::class.java) { task ->
            task.doSetup {
                val aptOptions = task.project.extensions[AptOptions::class.java]
                aptOptions.processorArgs.forEach { name, value ->
                    task.options.compilerArgs.add("-A$name=${value?.toString().default()}")
                }
            }
        }
    }

    @PluginAction
    @WithPluginClasses(IdeaSettingsPlugin::class)
    fun ExtensionContainer.`Add default annotation processing profile`() {
        this[IdeaSettings::class.java].addDefaultAnnotationProcessingProfile()
    }

}


val ConfigurationContainer.aptOnly: Configuration get() = this[APT_ONLY_CONFIGURATION_NAME]
val ConfigurationContainer.apt: Configuration get() = this[APT_CONFIGURATION_NAME]


@Extension("Java annotation-processing settings")
class AptOptions {

    @ExtensionProperty("Annotation-processors arguments")
    var processorArgs: MutableMap<String, Any?> = LinkedHashMap()
        set(value) {
            field = LinkedHashMap(value)
        }

}
