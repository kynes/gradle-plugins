package name.remal.gradle_plugins.api.todo;

import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;

@Documented
@Retention(CLASS)
@Repeatable(FIXME.FIXMEs.class)
public @interface FIXME {

    String value();


    @Documented
    @Retention(CLASS)
    @interface FIXMEs {
        FIXME[] value() default {};
    }

}
