package name.remal.gradle_plugins.plugins.ci

import groovy.lang.Closure
import name.remal.gradle_plugins.dsl.extensions.toRunnable

interface CIExtension {

    var isBuildOnCI: Boolean

    var isBuildOnLocal: Boolean
        get() = !isBuildOnCI
        set(value) {
            isBuildOnCI = !value
        }

    fun forBuildOnCI(action: Runnable)

    fun forBuildOnCI(action: Closure<*>) = forBuildOnCI(action.toRunnable())

    fun forBuildOnLocal(action: Runnable)

    fun forBuildOnLocal(action: Closure<*>) = forBuildOnLocal(action.toRunnable())


    var pipelineId: String?
    var buildId: String?
    var stageName: String?
    var jobName: String?

}

fun CIExtension.forBuildOnCI(action: () -> Unit) = forBuildOnCI(Runnable(action))
fun CIExtension.forBuildOnLocal(action: () -> Unit) = forBuildOnLocal(Runnable(action))
