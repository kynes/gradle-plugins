package name.remal.gradle_plugins.dsl.extensions

import name.remal.default
import name.remal.fromUpperCamelToLowerUnderscore
import name.remal.nullIfEmpty
import org.gradle.api.Task
import org.gradle.api.reporting.ConfigurableReport
import org.gradle.api.reporting.Report.OutputType.DIRECTORY
import org.gradle.api.reporting.Report.OutputType.FILE
import java.io.File

private fun ConfigurableReport.setDestinationForTask(task: Task, reportsDir: File = task.project.reportsDir) {
    if (isDestinationCalculable) {
        destination = calculateDestination(task, reportsDir)
    }
}

fun ConfigurableReport.setDefaultDestinationForTask(task: Task, reportsDir: File = task.project.reportsDir) {
    @Suppress("SENSELESS_COMPARISON")
    if (destination == null) {
        setDestinationForTask(task, reportsDir)
    }
}


private fun ConfigurableReport.setDestinationForTask(task: Task, reportsDirProvider: () -> File) {
    if (isDestinationCalculable) {
        setDestination(task.project.provider { calculateDestination(task, reportsDirProvider()) })
    }
}

fun ConfigurableReport.setDefaultDestinationForTask(task: Task, reportsDirProvider: () -> File) {
    @Suppress("SENSELESS_COMPARISON")
    if (destination == null) {
        setDestinationForTask(task, reportsDirProvider)
    }
}


private fun ConfigurableReport.setDestinationForTask(task: Task) {
    setDestinationForTask(task, { task.project.reportsDir })
}

fun ConfigurableReport.setDefaultDestinationForTask(task: Task) {
    @Suppress("SENSELESS_COMPARISON")
    if (destination == null) {
        setDestinationForTask(task)
    }
}


@Suppress("RedundantElseInWhen")
fun ConfigurableReport.calculateDestination(task: Task, reportsDir: File = task.project.reportsDir): File {
    val classDirName = task.javaClass.reportsBaseDirName

    return when (outputType) {
        DIRECTORY -> File(reportsDir, "$classDirName/${task.name}/$name")
        FILE -> {
            val extension = destinationFileExtension
            val baseName = name.substring(0, name.length - extension.length).trimEnd('-', '_').nullIfEmpty().default("report")
            File(reportsDir, "$classDirName/${task.name}/$baseName.$extension")
        }
        else -> throw UnsupportedOperationException("Unsupported outputType: $outputType")
    }
}

private val redundantSuffixRegex = Regex("^(.*[a-z])Task$")
private val repeatedCharsRegex = Regex("([a-z])_(\\1)")
val Class<out Task>.reportsBaseDirName: String
    get() = unwrapGradleGenerated().simpleName
        .let { redundantSuffixRegex.replace(it, "$1") }
        .fromUpperCamelToLowerUnderscore()
        .let { repeatedCharsRegex.replace(it, "$1-$2") }
        .replace("_", "")


private val ConfigurableReport.isDestinationCalculable: Boolean
    get() = FILE == outputType || DIRECTORY == outputType

