package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.artifact.HasEntries
import org.gradle.api.file.FileCollection

val FileCollection.isNotEmpty get() = !isEmpty


fun FileCollection.toHasEntries(): HasEntries = CachedArtifactsCollection(this)
fun Iterable<FileCollection>.toHasEntries(): HasEntries = CachedArtifactsCollection(this.flatten())

fun FileCollection.getJavaModuleName() = asFileTree.getJavaModuleName()
