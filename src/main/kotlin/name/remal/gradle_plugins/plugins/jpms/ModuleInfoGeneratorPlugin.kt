package name.remal.gradle_plugins.plugins.jpms

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.main
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer

@Plugin(
    id = "name.remal.module-info-generator",
    description = "Plugin that helps to generate `module-info.class` for Java 8 projects.",
    tags = ["java", "jpms", "jigsaw"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class)
class ModuleInfoGeneratorPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun TaskContainer.`Create 'generateModuleInfo' task`(sourceSets: SourceSetContainer) {
        val mainSourceSet = sourceSets.main
        create(mainSourceSet.getTaskName("generate", "moduleInfo"), GenerateModuleInfoTask::class.java) {
            it.sourceSet = mainSourceSet
        }
    }

}
