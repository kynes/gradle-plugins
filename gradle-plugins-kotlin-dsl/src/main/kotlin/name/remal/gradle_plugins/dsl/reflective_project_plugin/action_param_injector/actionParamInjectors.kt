package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.loadServicesList

internal val actionParamInjectors: List<ActionParamInjector<*>> by lazy { loadServicesList(ActionParamInjector::class.java) }
