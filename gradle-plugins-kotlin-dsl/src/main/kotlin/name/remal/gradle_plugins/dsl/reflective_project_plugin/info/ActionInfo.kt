package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

interface ActionInfo : WithRequirements, WithApplyPlugins {
    val order: Int
    val description: String
    val isAfterProjectEvaluation: Boolean

    fun compareTo(other: ActionInfo): Int {
        order.compareTo(other.order).let { if (it != 0) return it }
        description.compareTo(other.description).let { if (it != 0) return it }
        javaClass.name.compareTo(other.javaClass.name).let { if (it != 0) return it }
        return 0
    }
}
