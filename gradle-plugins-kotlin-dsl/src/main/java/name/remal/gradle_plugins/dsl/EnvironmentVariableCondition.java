package name.remal.gradle_plugins.dsl;

import org.gradle.api.Project;

@FunctionalInterface
public interface EnvironmentVariableCondition {

    boolean test(Project project);

}
