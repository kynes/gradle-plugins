package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.tasks.compile.JavaCompile
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class CompileOptionsExtensionsTest : BaseProjectTest() {

    @Test
    fun parameters() {
        val task = project.tasks.createWithUniqueName(JavaCompile::class.java)
        val options = task.options
        assertFalse(options.parameters)
        assertFalse("-parameters" in options.compilerArgs)

        options.parameters = true
        assertTrue(options.parameters)
        assertTrue("-parameters" in options.compilerArgs)

        options.parameters = false
        assertFalse(options.parameters)
        assertFalse("-parameters" in options.compilerArgs)
    }

}
