package name.remal.gradle_plugins.plugins.dependencies.component_metadata

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.AbstractComponentMetadata
import org.gradle.api.Project
import org.gradle.api.artifacts.ComponentMetadataDetails
import org.gradle.api.artifacts.ModuleVersionIdentifier
import javax.inject.Inject

@AutoService
class RemalToolsMetadata @Inject constructor(project: Project) : AbstractComponentMetadata(project) {

    override fun ModuleVersionIdentifier.process(details: ComponentMetadataDetails) {
        if ("$group.".startsWith("name.remal.tools.")) {
            details.belongsTo("name.remal.tools:tools-platform:$version")
        }
    }

}
