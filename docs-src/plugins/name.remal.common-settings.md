The plugin applies these plugins:

* [`idea`](https://docs.gradle.org/current/userguide/idea_plugin.html)
* [`name.remal.idea-settings`](name.remal.idea-settings.md)
* [`name.remal.idea-extended-settings`](name.remal.idea-extended-settings.md)
* [`eclipse`](https://docs.gradle.org/current/userguide/eclipse_plugin.html)
* [`name.remal.eclipse-settings`](name.remal.eclipse-settings.md)
* [`name.remal.gradle-wrapper-settings`](name.remal.gradle-wrapper-settings.md)
* [`name.remal.configuration-extensions`](name.remal.configuration-extensions.md)
* [`name.remal.dependencies-extensions`](name.remal.dependencies-extensions.md)
* [`name.remal.reports-settings`](name.remal.reports-settings.md)
* [`name.remal.component-metadata`](name.remal.component-metadata.md)

&nbsp;

1. Sets duplicates strategy to `WARN` for all tasks that implement [`CopySpec`](https://docs.gradle.org/current/javadoc/org/gradle/api/file/CopySpec.html) interface.
1. Turns ON reproducible file order for archive tasks
1. Disables dependencies transitivity if configuration is not transitive
1. Copies project group to all sub-projects
1. Copies project version to all sub-projects
1. Set `Multi-Release` manifest attribute if the jar file is a multi-release Jar
