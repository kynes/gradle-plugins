package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.UnresolvedDependency

val UnresolvedDependency.notation get() = selector.notation


fun DependencyNotationMatcher.matches(dependency: UnresolvedDependency) = matches(dependency.notation)
fun DependencyNotationMatcher.notMatches(dependency: UnresolvedDependency) = !matches(dependency)
