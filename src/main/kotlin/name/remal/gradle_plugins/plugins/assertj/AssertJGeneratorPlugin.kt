package name.remal.gradle_plugins.plugins.assertj

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPlugins
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateConfigurationsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.SimpleTestAdditionalGradleScript
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.applyLoggingTransitiveDependenciesExcludes
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isCompilingSourceSet
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.testing.TestSourceSetContainer
import name.remal.gradle_plugins.plugins.testing.TestSourceSetsPlugin
import name.remal.gradle_plugins.utils.getVersionProperty
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.AbstractCompile

const val ASSERTJ_GENERATOR_CONFIGURATION_NAME = "assertjGenerator"
const val ASSERTJ_GENERATE_TASK_NAME = "assertjGenerate"

@Plugin(
    id = "name.remal.assertj-generator",
    description = "Plugin that configures AssertJ generator",
    tags = ["assertj"]
)
@ApplyPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, TestSourceSetsPlugin::class)
@SimpleTestAdditionalGradleScript(
    """
dependencies {
    implementation 'org.assertj:assertj-core:+'
}
project.file('src/main/java/pkg/TestDto.java').with {
    parentFile.mkdirs()
    write('''
        package pkg;
        public class TestDto {
            private String value;
            public void setValue(String value) {
                this.value = value;
            }
            public String getValue() {
                return this.value;
            }
        }
    ''', 'UTF-8')
}
"""
)
class AssertJGeneratorPlugin : BaseReflectiveProjectPlugin() {

    @CreateConfigurationsPluginAction
    fun ConfigurationContainer.`Create 'assertjGenerator' configuration`(project: Project, dependencyHandler: DependencyHandler, tasks: TaskContainer) {
        val assertjGeneratorConf = create(ASSERTJ_GENERATOR_CONFIGURATION_NAME) {
            it.description = "Classpath of AssertJ generator"
            it.applyLoggingTransitiveDependenciesExcludes()

            val defaultDependencyVersion = project.getVersionProperty("assertj-assertions-generator")
            it.defaultDependencies {
                it.add(dependencyHandler.create("org.assertj:assertj-assertions-generator:$defaultDependencyVersion"))
            }
        }

        tasks.all(AssertJGenerate::class.java) {
            it.assertjGeneratorClasspath = assertjGeneratorConf
        }
    }

    @PluginAction(order = -100)
    fun TaskContainer.`Create 'assertjGenerate' task`() {
        create(ASSERTJ_GENERATE_TASK_NAME, AssertJGenerate::class.java)
    }

    @PluginAction
    fun TestSourceSetContainer.`Make all compile tasks of all testSourceSets depend on 'assertjGenerate' task`(tasks: TaskContainer) {
        val assertjGenerate = tasks[AssertJGenerate::class.java, ASSERTJ_GENERATE_TASK_NAME]
        all { testSourceSet ->
            testSourceSet.java.srcDir(assertjGenerate.outputDir)
            tasks.all(AbstractCompile::class.java) { compileTask ->
                if (compileTask.isCompilingSourceSet(testSourceSet)) {
                    compileTask.dependsOn(assertjGenerate)
                }
            }
        }
    }

}
