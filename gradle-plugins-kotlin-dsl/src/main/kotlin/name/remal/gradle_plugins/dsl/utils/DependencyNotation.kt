package name.remal.gradle_plugins.dsl.utils

import name.remal.default
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.nullIfEmpty
import name.remal.version.Version
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.artifacts.ModuleVersionSelector
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.gradle.api.artifacts.component.ModuleComponentSelector
import org.gradle.api.publish.maven.MavenPublication
import kotlin.LazyThreadSafetyMode.NONE

class DependencyNotation(
    val group: String,
    val module: String,
    version: String? = null,
    classifier: String? = null,
    extension: String? = null
) : Comparable<DependencyNotation> {

    val version: String = version.default()
    val classifier: String = classifier.default()
    val extension: String = extension.default()

    val parsedVersion: Version? by lazy(NONE) { Version.parseOrNull(version) }

    private val string = buildString {
        append(this@DependencyNotation.group).append(':').append(this@DependencyNotation.module)
        if (this@DependencyNotation.version.isNotEmpty() || this@DependencyNotation.classifier.isNotEmpty() || this@DependencyNotation.extension.isNotEmpty()) append(':').append(this@DependencyNotation.version)
        if (this@DependencyNotation.classifier.isNotEmpty() || this@DependencyNotation.extension.isNotEmpty()) append(':').append(this@DependencyNotation.classifier)
        if (this@DependencyNotation.extension.isNotEmpty()) append('@').append(this@DependencyNotation.extension)
    }

    override fun toString() = string
    override fun equals(other: Any?) = other is DependencyNotation && string == other.string
    override fun hashCode() = 1 + string.hashCode()

    fun compareVersions(other: DependencyNotation): Int {
        val parsedVersion = this.parsedVersion
        val otherParsedVersion = other.parsedVersion
        if (parsedVersion != null && otherParsedVersion != null) {
            return parsedVersion.compareTo(otherParsedVersion)
        } else {
            return version.compareTo(other.version)
        }
    }

    override fun compareTo(other: DependencyNotation): Int {
        group.compareTo(other.group).let { if (it != 0) return it }
        module.compareTo(other.module).let { if (it != 0) return it }
        compareVersions(other).let { if (it != 0) return it }
        classifier.compareTo(other.classifier).let { if (it != 0) return it }
        extension.compareTo(other.extension).let { if (it != 0) return it }
        return 0
    }


    fun withVersion(version: String?) = DependencyNotation(
        group = group,
        module = module,
        version = version,
        classifier = classifier,
        extension = extension
    )

    fun withLatestVersion() = withVersion("+")

    @JvmOverloads
    fun withSnapshotVersion(value: Boolean = true): DependencyNotation {
        if (version.isEmpty()) throw IllegalStateException("Version is empty")
        return withVersion(
            if (value) {
                if (version.contains("-SNAPSHOT")) {
                    version
                } else {
                    "$version-SNAPSHOT"
                }
            } else {
                version.substringBefore("-SNAPSHOT")
            }
        )
    }

    fun withoutSnapshotVersion() = withSnapshotVersion(false)

    fun withoutVersion() = withVersion(null)

    fun withDefaultVersion(version: String) = DependencyNotation(
        group = group,
        module = module,
        version = this.version.nullIfEmpty() ?: version,
        classifier = classifier,
        extension = extension
    )

    fun withDefaultLatestVersion() = withDefaultVersion("+")

    fun withClassifier(classifier: String?) = DependencyNotation(
        group = group,
        module = module,
        version = version,
        classifier = classifier,
        extension = extension
    )

    fun withoutClassifier() = withClassifier(null)

    fun withExtension(extension: String?) = DependencyNotation(
        group = group,
        module = module,
        version = version,
        classifier = classifier,
        extension = extension
    )

    fun withoutExtension() = withExtension(null)


    fun withOnlyGroupAndModule() = DependencyNotation(
        group = group,
        module = module,
        version = null,
        classifier = null,
        extension = null
    )

}


fun parseDependencyNotation(notation: String): DependencyNotation {
    val notationTokens = notation.split(':')
    if (notationTokens.size < 2) throw InvalidDependencyNotationString("Notation have only group: $notation")
    val group = notationTokens[0]
    val module = notationTokens[1]
    val version = notationTokens.getOrNull(2)

    val classifier: String?
    val extension: String?
    val tokens = notationTokens.getOrNull(3).nullIfEmpty()?.split('@')
    if (tokens != null) {
        classifier = tokens[0]
        extension = tokens.getOrNull(1)
    } else {
        classifier = null
        extension = null
    }

    return DependencyNotation(
        group = group,
        module = module,
        version = version,
        classifier = classifier,
        extension = extension
    )
}

class InvalidDependencyNotationString : RuntimeException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}


fun createDependencyNotation(obj: Any): DependencyNotation = obj.unwrapProviders().let {
    when (it) {
        is DependencyNotation -> it
        is CharSequence -> parseDependencyNotation(it.toString())
        is Dependency -> it.notation
        is MavenPublication -> it.notation
        is ModuleComponentIdentifier -> it.notation
        is ModuleComponentSelector -> it.notation
        is ModuleVersionIdentifier -> it.notation
        is ModuleVersionSelector -> it.notation
        is Project -> it.notation
        is ResolvedArtifact -> it.notation
        is ResolvedDependency -> it.notation
        else -> throw IllegalArgumentException("Can't be converted to ${DependencyNotation::class.java.simpleName}: $it")
    }
}
