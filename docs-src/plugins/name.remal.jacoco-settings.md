**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) and [`jacoco`](https://docs.gradle.org/current/userguide/jacoco_plugin.html) plugins are applied.**

&nbsp;

This plugin helps to configure Jacoco.

* It updates Jacoco version to the latest one.
* It turns ON all reports.
* It turns OFF fail on violation.
* If a Jacoco task has not execution files set, it crashes. So the plugin creates temp empty execution file by default.
* It configures Jacoco for all [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) tasks.
