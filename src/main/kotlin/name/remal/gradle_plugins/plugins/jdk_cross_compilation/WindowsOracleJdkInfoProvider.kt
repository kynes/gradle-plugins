package name.remal.gradle_plugins.plugins.jdk_cross_compilation

import name.remal.OS
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.retrieveVersion
import name.remal.gradle_plugins.dsl.utils.executableFileSuffix
import name.remal.nullIfEmpty
import name.remal.version.Version
import org.gradle.api.JavaVersion
import org.gradle.internal.jvm.Jvm
import org.slf4j.LoggerFactory
import java.io.File
import java.lang.System.getenv

@AutoService
class WindowsOracleJdkInfoProvider : JdkInfoProvider {

    companion object {
        private val logger = LoggerFactory.getLogger(WindowsOracleJdkInfoProvider::class.java)

        private val jdkDirs: Map<File, Version> by lazy {
            val programFilesPath = getenv("ProgramFiles").nullIfEmpty()
                ?: getenv("SystemDrive").nullIfEmpty()?.let { "$it\\Program Files" }
                ?: "C:\\Program Files"

            return@lazy File(programFilesPath, "Java").absoluteFile
                .listFiles { file ->
                    file.isDirectory
                        && file.resolve("bin").resolve("javac$executableFileSuffix").isFile
                }
                ?.mapNotNull { dir ->
                    val jvmInfo = Jvm.forHome(dir)
                    var version: Version? = jvmInfo.retrieveVersion()
                    if (version == null) {
                        logger.info("Version can't be retrieved for JVM: {}", jvmInfo.javaHome)
                        return@mapNotNull null
                    }
                    if (version.major == 1 && version.numbersCount >= 2) {
                        // For legacy Java version (for example: 1.8.0_151)
                        version = Version.create(
                            (1 until version.numbersCount)
                                .map(version::getNumberOr0)
                                .toTypedArray()
                                .toIntArray(),
                            version.suffixDelimiter,
                            version.suffix
                        )
                    }
                    return@mapNotNull Pair(dir, version)
                }
                ?.toMap()
                ?: emptyMap()
        }
    }

    override fun provide(javaVersion: JavaVersion): JdkInfo? {
        if (!OS.isWindows()) return null
        jdkDirs.asSequence()
            .filter { it.value.major == javaVersion.majorVersion.toInt() }
            .maxBy { it.value }
            ?.key
            ?.let { return JdkInfo(it) }
        return null
    }

}
