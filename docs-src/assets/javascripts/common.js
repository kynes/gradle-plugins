(function($){

hljs.initHighlightingOnLoad();

$(function(){ setTimeout(function(){
    GitGraph.Template.custom = {};

    GitGraph.Template.custom['minimal'] = (function(){
        var tpl = new GitGraph.Template().get("default");
        tpl.commit = tpl.commit || {};
        tpl.commit.message = tpl.commit.message || {};
        tpl.commit.message.display = false;
        tpl.commit.shouldDisplayTooltipsInCompactMode = false;
        tpl.commit.tag = tpl.commit.tag || {};
        tpl.commit.tag.font = "normal 11pt Arial";
        return tpl;
    })();

    var gitgraphCanvasesCounter = 0;
    $('script[type=gitgraph]').each(function(){
        var $node = $(this);
        var canvasId = 'gitgraph-canvas-' + (++gitgraphCanvasesCounter);
        var template = $node.attr('template');
        var mode = $node.attr('mode') || 'extended';
        var script = [
            '(function(){',
            'var gitgraphCanvasId = "'+canvasId+'"',
            'var gitgraphTemplate = GitGraph.Template.custom["'+template+'"] || "metro";',
            'var gitgraph = new GitGraph({ elementId: gitgraphCanvasId, template: gitgraphTemplate, mode: "'+mode+'", author: "" });',
            $node.text(),
            '})()'
        ].join('\n');
        $node
            .before('<canvas id="'+canvasId+'"></canvas>')
            .after($(document.createElement('script')).text(script))
            .remove();
    });
}, 0); });

$(document).on('click', '[href]', function(){
    var $link = $(this);
    var href = $link.attr('href') || '';
    if (href.match(/^(\w+:)?\/\//)) {
        $link.attr('target', '_blank');
    }
});

$(function(){
    $('.md-footer-copyright').each(function(){
        var $copyright = $(this);
        $copyright.append(' &nbsp; &nbsp; <a href="https://gitlab.com/remal/gradle-plugins/-/jobs"><img alt="master pipeline status" src="https://gitlab.com/remal/gradle-plugins/badges/master/pipeline.svg?'+(new Date().getTime())+'" style="display: inline-block; vertical-align: middle" rel="pipeline-status-image"/></a>');
        //$copyright.append(' &nbsp; &nbsp; <a href="https://gitlab.com/remal/gradle-plugins/-/jobs"><img alt="feature/docs pipeline status" src="https://gitlab.com/remal/gradle-plugins/badges/feature%2Fdocs/pipeline.svg?'+(new Date().getTime())+'" style="display: inline-block; vertical-align: middle" rel="pipeline-status-image"/></a>');
    });
});
setInterval(
    function(){
        $('img[rel~=pipeline-status-image][src]').each(function(){
            var $image = $(this);
            var src = $image.attr('src').toString().replace(/\?.*$/, '');
            src += '?' + new Date().getTime();
            $image.attr('src', src);
        });
    },
    5000
);

})(jQuery);
