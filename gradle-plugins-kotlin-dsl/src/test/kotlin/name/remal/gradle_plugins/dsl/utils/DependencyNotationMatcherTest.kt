package name.remal.gradle_plugins.dsl.utils

import name.remal.gradle_plugins.dsl.extensions.matches
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class DependencyNotationMatcherTest : BaseProjectTest() {

    @Test(expected = InvalidDependencyNotationPattern::class)
    fun `pattern - empty`() {
        DependencyNotationMatcher("")
    }

    @Test(expected = InvalidDependencyNotationPattern::class)
    fun `pattern - group only`() {
        DependencyNotationMatcher("group")
    }

    @Test(expected = InvalidDependencyNotationPattern::class)
    fun `pattern - empty module`() {
        DependencyNotationMatcher("group:")
    }

    @Test
    fun `pattern - asterisk`() {
        DependencyNotationMatcher("*:module").assertMatches("pkg:module:ver")
        DependencyNotationMatcher("pkg:*").assertMatches("pkg:module:ver")
        DependencyNotationMatcher("*:*").assertMatches("pkg:module:ver")
    }

    @Test
    fun `pattern - group + module`() {
        val matcher = DependencyNotationMatcher("top.pkg-*.child:*-module-*")
        matcher.assertDoesNotMatches("top.pkg.child:module")
        matcher.assertMatches("top.pkg-.child:-module-")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:ver")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:ver:sources")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:ver:sources@jar")
    }

    @Test
    fun `pattern - group + module + empty version`() {
        val matcher = DependencyNotationMatcher("top.pkg-*.child:*-module-*:")
        matcher.assertDoesNotMatches("top.pkg.child:module")
        matcher.assertMatches("top.pkg-.child:-module-")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:ver")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:ver:sources")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:ver:sources@jar")
    }

    @Test(expected = InvalidDependencyNotationPattern::class)
    fun `pattern - version with asterisk`() {
        DependencyNotationMatcher("top.pkg-*.child:*-module-*:1.*")
    }

    @Test(expected = InvalidDependencyNotationPattern::class)
    fun `pattern - wrong version range`() {
        DependencyNotationMatcher("top.pkg-*.child:*-module-*:1.+.1")
    }

    @Test
    fun `pattern - group + module + version`() {
        val matcher = DependencyNotationMatcher("top.pkg-*.child:*-module-*:1.+")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:2")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:sources")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:sources@jar")
    }

    @Test
    fun `pattern - group + module + version + classifier`() {
        val matcher = DependencyNotationMatcher("top.pkg-*.child:*-module-*:1.+:classifier")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:jar")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier@jar")
    }

    @Test
    fun `pattern - group + module + version + extension`() {
        val matcher = DependencyNotationMatcher("top.pkg-*.child:*-module-*:1.+:@ext")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:jar")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier@jar")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier@ext")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:@ext")
    }

    @Test
    fun `pattern - group + module + version + classifier + extension`() {
        val matcher = DependencyNotationMatcher("top.pkg-*.child:*-module-*:1.+:classifier@ext")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:@jar")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier@jar")
        matcher.assertDoesNotMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:@ext")
        matcher.assertMatches("top.pkg-smth.child:prefix-module-suffix.end:1.1:classifier@ext")
    }


    private fun DependencyNotationMatcher.assertMatches(notation: String) {
        assertTrue(matches(notation))
        assertTrue(matches(project.dependencies.create(notation)))
    }

    private fun DependencyNotationMatcher.assertDoesNotMatches(notation: String) {
        assertFalse(matches(notation))
        assertFalse(matches(project.dependencies.create(notation)))
    }

}
