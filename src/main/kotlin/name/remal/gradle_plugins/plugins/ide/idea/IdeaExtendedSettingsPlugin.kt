package name.remal.gradle_plugins.plugins.ide.idea

import name.remal.SERVICE_FILE_BASE_PATH
import name.remal.Services.readServiceLines
import name.remal.getOrCreateChild
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.CurrentProjectIsARootProjectMixin
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.SkipIfOnlyCleanTasksAreRequestedToExecuteMixin
import name.remal.gradle_plugins.plugins.code_quality.checkstyle.CheckstylePluginId
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.toSet
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.quality.CheckstyleExtension
import org.jdom2.filter.Filters

@Plugin(
    id = "name.remal.idea-extended-settings",
    description = "Plugin that configures IDEA workspace.",
    tags = ["idea"]
)
@WithPlugins(IdeaPluginId::class)
@ApplyPluginClasses(IdeaSettingsPlugin::class)
class IdeaExtendedSettingsPlugin : BaseReflectiveProjectPlugin(), SkipIfOnlyCleanTasksAreRequestedToExecuteMixin {

    @PluginAction(order = Int.MIN_VALUE)
    fun Project.`Apply this plugin for all projects`() {
        rootProject.allprojects {
            it.applyPlugin(IdeaExtendedSettingsPlugin::class.java)
        }
    }

    @PluginActionsGroup
    inner class `For root project` : CurrentProjectIsARootProjectMixin {

        @PluginAction
        fun ExtensionContainer.`Delegate build to Gradle`() {
            this[IdeaSettings::class.java].configureIDEAComponent("GradleSettings", "gradle.xml", Action {
                it.getOrCreateChild("option", mapOf("name" to "linkedExternalProjectsSettings")).apply {
                    getOrCreateChild("GradleProjectSettings").apply {
                        getOrCreateChild("option", mapOf("name" to "delegatedBuild")).apply {
                            if (getAttributeValue("value").isNullOrEmpty()) {
                                setAttribute("value", "true")
                            }
                        }
                        getOrCreateChild("option", mapOf("name" to "testRunner")).apply {
                            if (getAttributeValue("value").isNullOrEmpty()) {
                                setAttribute("value", "GRADLE")
                            }
                        }
                    }
                }
            })
        }

        @PluginActionsGroup
        @WithPlugins(JavaPluginId::class)
        inner class `If 'java' plugin applied` {

            @PluginAction
            fun ExtensionContainer.`Setup EntryPointsManager - entry points annotations`() {
                this[IdeaSettings::class.java].entryPointAnnotations(
                    readServiceLines("$SERVICE_FILE_BASE_PATH/${EntryPointAnnotation::class.java.name}").toSet()
                )
            }

            @PluginAction
            fun ExtensionContainer.`Setup EntryPointsManager - write annotations`() {
                this[IdeaSettings::class.java].writeAnnotations(
                    readServiceLines("$SERVICE_FILE_BASE_PATH/${WriteAnnotation::class.java.name}").toSet()
                )
            }

            @PluginAction
            fun ExtensionContainer.`Setup NullableNotNullManager - Nullable annotations`() {
                this[IdeaSettings::class.java].nullableAnnotations(
                    readServiceLines("$SERVICE_FILE_BASE_PATH/${NullableAnnotation::class.java.name}").toSet()
                )
            }

            @PluginAction
            fun ExtensionContainer.`Setup NullableNotNullManager - NotNull annotations`() {
                this[IdeaSettings::class.java].notNullAnnotations(
                    readServiceLines("$SERVICE_FILE_BASE_PATH/${NotNullAnnotation::class.java.name}").toSet()
                )
            }

            @PluginAction
            fun ExtensionContainer.`Setup CompilerConfiguration - add default annotation-processing profile`() {
                this[IdeaSettings::class.java].addDefaultAnnotationProcessingProfile()
            }

        }

        @PluginActionsGroup
        @WithPlugins(CheckstylePluginId::class)
        inner class `If 'checkstyle' plugin is applied` {

            @PluginAction
            fun ExtensionContainer.`Set Checkstyle version`() {
                this[IdeaSettings::class.java].configureIDEAComponent("CheckStyle-IDEA", null, Action { element ->
                    val toolVersion: String? = this[CheckstyleExtension::class.java].toolVersion
                    if (toolVersion != null && toolVersion.isNotEmpty()) {
                        element.getDescendants(Filters.element("entry")).forEach { entry ->
                            if (entry.getAttributeValue("key") == "checkstyle-version") {
                                entry.setAttribute("value", toolVersion)
                            }
                        }
                    }
                })
            }

        }

    }

}

