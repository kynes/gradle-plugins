package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.internal.impl

import com.gradle.protocols.ServerResponseBase
import com.gradle.publish.Hasher
import com.gradle.publish.OAuthHttpClient
import com.gradle.publish.protocols.v1.models.ClientPostRequest
import com.gradle.publish.protocols.v1.models.publish.ArtifactType
import com.gradle.publish.protocols.v1.models.publish.ArtifactType.POM
import com.gradle.publish.protocols.v1.models.publish.BuildMetadata
import com.gradle.publish.protocols.v1.models.publish.PublishActivateResponse
import com.gradle.publish.protocols.v1.models.publish.PublishArtifact
import com.gradle.publish.protocols.v1.models.publish.PublishMavenCoordinates
import com.gradle.publish.protocols.v1.models.publish.PublishNewVersionRequest
import com.gradle.publish.protocols.v1.models.publish.PublishNewVersionResponse
import com.gradle.publish.upload.Uploader
import name.remal.buildMap
import name.remal.gradle_plugins.dsl.extensions.isInfoLogEnabled
import name.remal.gradle_plugins.dsl.extensions.logError
import name.remal.gradle_plugins.dsl.extensions.logInfo
import name.remal.gradle_plugins.dsl.extensions.logLifecycle
import name.remal.gradle_plugins.dsl.extensions.logWarn
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.gradle_plugins.dsl.utils.retryIO
import name.remal.gradle_plugins.plugins.publish.BasePublishToMaven
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.PublishToGradlePluginPortalRepository
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.PublishingToGradlePluginPortalException
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.internal.PublishToGradlePluginPortalInvoker
import name.remal.nullIfEmpty
import name.remal.sha256
import name.remal.use
import org.gradle.util.GradleVersion
import java.io.File
import java.net.URI

internal class PublishToGradlePluginPortalInvokerImpl : PublishToGradlePluginPortalInvoker() {

    override fun publish() {
        task.doPublish()
    }


    private val buildMetadata: BuildMetadata = BuildMetadata(GradleVersion.current().version)

    private val pomUrl: String? by lazy { pomDocument.rootElement.getChild("url", BasePublishToMaven.POM_NAMESPACE)?.textTrim.nullIfEmpty() }

    private val pomScmUrl: String? by lazy {
        pomDocument.rootElement.getChild("scm", BasePublishToMaven.POM_NAMESPACE)
            ?.getChild("url", BasePublishToMaven.POM_NAMESPACE)?.textTrim.nullIfEmpty()
    }

    private val mavenCoordinates: PublishMavenCoordinates by lazy {
        PublishMavenCoordinates(
            task.publication.groupId,
            task.publication.artifactId,
            task.publication.version
        )
    }

    private val publishArtifacts: Map<PublishArtifact, File> by lazy {
        buildMap<PublishArtifact, File> {
            task.publication.artifacts.forEach { artifact ->
                val type = ArtifactType.find(artifact.extension, artifact.classifier) ?: return@forEach
                val hash: String = artifact.file.inputStream().use { Hasher.hash(it) }
                put(PublishArtifact(type.name, hash), artifact.file)
            }

            run {
                val pomFile = pomFile
                val hash: String = pomFile.inputStream().use { sha256(it) }
                put(PublishArtifact(POM.name, hash), pomFile)
            }
        }
    }

    private fun PublishToGradlePluginPortalRepository.doPublish() {
        val publishResponses = mutableListOf<PublishNewVersionResponse>()
        notPublishedPlugins.forEach { plugin ->
            logLifecycle("Publishing plugin {} version {}", plugin.id, publication.version)
            val request = PublishNewVersionRequest().also {
                it.buildMetadata = buildMetadata
                it.pluginId = plugin.id ?: throw IllegalStateException("Plugin ID is not set for plugin ${plugin.id}")
                it.pluginVersion = publication.version
                it.displayName = plugin.displayName?.trim().nullIfEmpty() ?: plugin.id
                it.description = plugin.description?.trim().nullIfEmpty() ?: plugin.id
                it.tags = plugin.tags.map(String::toLowerCase)
                it.webSite = plugin.websiteUrl.nullIfEmpty() ?: repository.websiteUrl.nullIfEmpty() ?: pomUrl
                    ?: throw IllegalStateException("Website URL is not set for plugin ${plugin.id}")
                it.vcsUrl = plugin.vcsUrl.nullIfEmpty() ?: repository.vcsUrl.nullIfEmpty() ?: pomScmUrl
                    ?: throw IllegalStateException("VCS URL is not set for plugin ${plugin.id}")
                it.mavenCoordinates = mavenCoordinates
                it.artifacts = publishArtifacts.keys.toList()

                it.webSite = it.webSite.replace("\${pluginId}", it.pluginId)
                it.vcsUrl = it.vcsUrl.replace("\${pluginId}", it.pluginId)
            }

            val response = retryIO { sendGradlePortalRequest(request) }
            publishResponses.add(response)
        }

        if (publishResponses.isEmpty()) {
            logWarn("No plugins were published")
            return
        }

        run {
            val uploadURLs: Map<String, String> = publishResponses.first().publishTo
            publishArtifacts.forEach { publishArtifact, file ->
                val uploadURL = uploadURLs[publishArtifact.hash]
                if (uploadURL == null) {
                    logWarn("Skipping upload of artifact {} as it has been previously uploaded", project.rootProject.relativePath(file))
                    return@forEach
                }

                logLifecycle("Uploading artifact {}", project.rootProject.relativePath(file))
                logInfo("Uploading artifact {} to {}", project.rootProject.relativePath(file), uploadURL)
                retryIO { Uploader.putFile(file, uploadURL) }
            }
        }

        publishResponses.forEach { publishResponse ->
            val activateRequest = publishResponse.nextRequest
            logLifecycle("Activating plugin {} version {}", activateRequest.pluginId, activateRequest.version)
            retryIO { sendGradlePortalRequest(activateRequest) }
        }
    }

    private fun <Response : ServerResponseBase> PublishToGradlePluginPortalRepository.sendGradlePortalRequest(
        request: ClientPostRequest<Response>
    ): Response {
        val baseURI = URI(
            System.getProperty("gradle.portal.url").nullIfEmpty()
                ?: project.findProperty("gradle.portal.url").unwrapProviders()?.toString().nullIfEmpty()
                ?: "https://plugins.gradle.org"
        )

        if (isInfoLogEnabled) {
            logInfo("Requesting POST {}: {}", baseURI.resolve(request.requestProtocolURL()), request.postJsonString)
        }

        val client = OAuthHttpClient(baseURI.toString(), repository.publishKey, repository.publishSecret)
        val response: Response? = client.send(request)

        if (isInfoLogEnabled) {
            logInfo("Response: {}", response?.convertToJsonString())
        }

        if (response == null) {
            throw PublishingToGradlePluginPortalException("Did not get a response from server")
        } else if (response.hasFailed()) {
            throw PublishingToGradlePluginPortalException(buildString {
                append("Request failed")
                if (!response.errorMessage.isNullOrEmpty()) {
                    append(". Server responded with: ").append(response.errorMessage)
                }
            })
        }

        response.errorMessage.nullIfEmpty()?.let { logError("{}", it) }
        when (response) {
            is PublishNewVersionResponse -> response.warningMessage.nullIfEmpty()?.let { logWarn("{}", it) }
            is PublishActivateResponse -> response.successMessage.nullIfEmpty()?.let { logLifecycle("{}", it) }
        }

        return response
    }

}
