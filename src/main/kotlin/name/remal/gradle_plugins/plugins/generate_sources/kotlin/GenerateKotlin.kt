package name.remal.gradle_plugins.plugins.generate_sources.kotlin

import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateClassTask
import org.gradle.api.tasks.CacheableTask
import java.nio.charset.Charset

@BuildTask
@CacheableTask
class GenerateKotlin : BaseGenerateClassTask<GeneratingKotlinClassWriter>() {

    override fun classFile(packageName: String, simpleName: String, action: (writer: GeneratingKotlinClassWriter) -> Unit) {
        val relativePath = buildString {
            if (packageName.isNotEmpty()) append(packageName.replace('.', '/')).append('/')
            append(simpleName).append(".kt")
        }
        addGenerateAction(relativePath) { file ->
            GeneratingKotlinClassWriter(packageName, simpleName, file, relativePath, this, file.writer(Charset.forName(charset))).use(action)
        }
    }

}
