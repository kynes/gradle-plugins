package name.remal.gradle_plugins.dsl.utils

import name.remal.ASM_API
import org.objectweb.asm.ClassVisitor

class SkipOuterAndInnerClassesClassVisitor(api: Int, delegate: ClassVisitor?) : ClassVisitor(api, delegate) {

    constructor(delegate: ClassVisitor?) : this(ASM_API, delegate)

    override fun visitOuterClass(owner: String, name: String?, descriptor: String?) {
        // do nothing
    }

    override fun visitInnerClass(name: String, outerName: String?, innerName: String?, access: Int) {
        // do nothing
    }

}
