package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

import name.remal.gradle_plugins.dsl.PluginId
import name.remal.gradle_plugins.dsl.ProjectPluginClass

interface WithApplyPlugins {
    val applyPluginIds: Set<PluginId>
    val applyPluginClasses: Set<ProjectPluginClass>
    val applyOptionalPluginIds: Set<PluginId>
    val applyPluginIdsAtTheEnd: Set<PluginId>
    val applyPluginClassesAtTheEnd: Set<ProjectPluginClass>
    val applyOptionalPluginIdsAtTheEnd: Set<PluginId>
}
