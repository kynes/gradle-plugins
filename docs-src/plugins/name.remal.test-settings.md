This plugin:

* For all [`AbstractTestTask`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/AbstractTestTask.html) tasks:
    * Turn ON all reports.
    * Show exceptions in logs.
    * Show causes in logs.
    * Show stacktraces in logs.
    * Use [`FULL`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/logging/TestExceptionFormat.html#FULL) exception format.
    * Use only [`GROOVY`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/logging/TestStackTraceFilter.html#GROOVY) stacktrace filter.
    * Log only [`FAILED`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/logging/TestLogEvent.html#FAILED) and [`STANDARD_ERROR`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/logging/TestLogEvent.html#STANDARD_ERROR) events by default.
    * Log all events for [`INFO`](https://docs.gradle.org/current/javadoc/org/gradle/api/logging/LogLevel.html#INFO) log level.
* For all [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) tasks:
    * Enable assertions
    * Set [`junit.jupiter.extensions.autodetection.enabled`](https://junit.org/junit5/docs/current/user-guide/#extensions-registration-automatic-enabling) system property to `true`.
    * Setup test framework based on classpath (if framework hasn't been set yet):
        * If the tasks's classpath has `org.junit.jupiter:*` or `org.junit.vintage:*` explicitly set dependencies, then [`JUnitPlatform`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html#useJUnitPlatform--) is used.
        * If the tasks's classpath has a class with package name `org.junit.jupiter.engine`, then [`JUnitPlatform`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html#useJUnitPlatform--) is used.
        * If the tasks's classpath has `junit:junit` explicitly set dependency, then [`JUnit`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html#useJUnit--) is used.
        * If the tasks's classpath has `org.junit.Test` class, then [`JUnit`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html#useJUnit--) is used.
        * If the tasks's classpath has `org.testng:testng` explicitly set dependency, then [`TestNG`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html#useTestNG--) is used.
        * If the tasks's classpath has `org.testng.annotations.Test` class, then [`TestNG`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html#useTestNG--) is used.
