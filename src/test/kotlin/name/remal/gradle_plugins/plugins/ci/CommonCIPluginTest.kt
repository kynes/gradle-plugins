package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test

class CommonCIPluginTest : BaseProjectTest() {

    @Test
    fun `rootProject delegation works fine`() {
        project.applyPlugin(CommonCIPlugin::class.java)
        val rootCI = project[CIExtension::class.java]
        rootCI.pipelineId = "root"

        val childProject = project.newChildProject("child")
        childProject.applyPlugin(CommonCIPlugin::class.java)
        val childCI = childProject[CIExtension::class.java]
        childCI.pipelineId = "child"

        assertEquals("child", rootCI.pipelineId)
    }

}
