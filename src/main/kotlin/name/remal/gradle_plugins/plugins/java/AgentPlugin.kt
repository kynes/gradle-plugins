package name.remal.gradle_plugins.plugins.java

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPlugins
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateConfigurationsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.from
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.distribution.DistributionContainer
import org.gradle.api.distribution.plugins.DistributionPlugin.MAIN_DISTRIBUTION_NAME
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.jvm.application.tasks.CreateStartScripts
import org.gradle.process.JavaForkOptions
import java.io.File
import java.nio.charset.Charset.defaultCharset

private const val AGENT_CONFIGURATION_NAME = "agent"


@Plugin(
    id = "name.remal.agent",
    description = "Plugin that configures Java agents.",
    tags = ["java", "agent"]
)
@ApplyPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class)
class AgentPlugin : BaseReflectiveProjectPlugin() {

    @CreateConfigurationsPluginAction
    fun ConfigurationContainer.`Create 'agent' configuration`() {
        create(AGENT_CONFIGURATION_NAME) { conf ->
            conf.description = "Java agents"
        }
    }

    @PluginAction
    fun TaskContainer.`Add -javaagent command-line parameter to all JavaForkOptions tasks`() {
        all { task ->
            if (task is JavaForkOptions) {
                task.doSetup {
                    task.project.configurations.agent.files.forEach {
                        task.jvmArgs("-javaagent:${it.absolutePath}")
                    }
                }
            }
        }
    }

    @WithPlugins(JavaApplicationPluginId::class)
    @PluginActionsGroup
    inner class `Setup 'application' plugin is it's applied` {

        @PluginAction
        fun ExtensionContainer.`Add agent libs to main distribution`(project: Project, configurations: ConfigurationContainer) {
            val distribution = this[DistributionContainer::class.java][MAIN_DISTRIBUTION_NAME]
            distribution.contents {
                it.with(project.copySpec().apply {
                    into("lib")
                    from { configurations.agent.files }
                })
            }
        }

        @PluginAction
        fun TaskContainer.`Setup CreateStartScripts task`(configurations: ConfigurationContainer) {
            val agentFiles: Iterable<File> by lazy { configurations.agent.files }
            all(CreateStartScripts::class.java) { task ->
                task.doLast {
                    var script = task.windowsScript.readText(defaultCharset())
                    if (!script.contains("\"%JAVA_EXE%\" %DEFAULT_JVM_OPTS%")) throw IllegalStateException("${task.windowsScript} do not contains '\"%JAVA_EXE%\" %DEFAULT_JVM_OPTS%'")
                    val agentsOpts = agentFiles.joinToString(" ") { "\"-javaagent:%APP_HOME%\\lib\\${it.name}\"" }
                    script = script.replace("\"%JAVA_EXE%\" %DEFAULT_JVM_OPTS%", "\"%JAVA_EXE%\" $agentsOpts %DEFAULT_JVM_OPTS%")
                    task.windowsScript.writeText(script, defaultCharset())
                }
                task.doLast {
                    var script = task.unixScript.readText(defaultCharset())
                    if (!script.contains("exec \"\$JAVACMD\"")) throw IllegalStateException("${task.unixScript} do not contains 'exec \"\$JAVACMD\"'")
                    val agentsOpts = agentFiles.joinToString(" ") { "\"-javaagent:\$APP_HOME/${it.name}\"" }
                    script = script.replace("exec \"\$JAVACMD\"", "exec \"\$JAVACMD\" $agentsOpts")
                    task.unixScript.writeText(script, defaultCharset())
                }
            }
        }

    }

}


val ConfigurationContainer.agent: Configuration get() = this[AGENT_CONFIGURATION_NAME]
