**This plugin works only if current project is a root project.**

The plugin applies these plugins:

* [`name.remal.default-plugins`](name.remal.default-plugins.md)
* [`name.remal.environment-variables`](name.remal.environment-variables.md)
