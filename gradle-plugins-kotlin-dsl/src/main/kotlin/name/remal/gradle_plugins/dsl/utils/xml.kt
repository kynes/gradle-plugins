package name.remal.gradle_plugins.dsl.utils

import name.remal.charset
import name.remal.createParentDirectories
import org.jdom2.Document
import org.jdom2.output.Format
import org.jdom2.output.Format.getCompactFormat
import org.jdom2.output.Format.getPrettyFormat
import org.jdom2.output.Format.getRawFormat
import org.jdom2.output.LineSeparator.UNIX
import org.jdom2.output.XMLOutputter
import java.io.File

private fun Format.defaultSettings() = this
    .setEncoding("UTF-8")
    .setOmitDeclaration(false)
    .setOmitEncoding(false)
    .setLineSeparator(UNIX)

val XML_PRETTY_OUTPUTTER = XMLOutputter(getPrettyFormat().defaultSettings())
val XML_COMPACT_OUTPUTTER = XMLOutputter(getCompactFormat().defaultSettings())
val XML_OUTPUTTER = XMLOutputter(getRawFormat().defaultSettings())


fun XMLOutputter.trimAndOutput(doc: Document, file: File) {
    val content = outputString(doc)
    val trimmedContent = content.trim()
    val bytes = trimmedContent.toByteArray(format.charset)
    file.createParentDirectories().writeBytes(bytes)
}
