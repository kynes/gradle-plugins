package name.remal.gradle_plugins.dsl.extensions

import name.remal.uncheckedCast
import org.gradle.api.InvalidUserDataException
import org.gradle.api.plugins.Convention
import org.gradle.api.plugins.ExtensionContainer

operator fun <T : Any> ExtensionContainer.invoke(name: String, configurer: (T) -> Unit) {
    if (this is Convention) this.plugins[name]?.let { it.configureWith(configurer); return }
    try {
        configure(name, configurer)
    } catch (e: Exception) {
        val isExtensionHasBeenAccessedException = e.javaClass == InvalidUserDataException::class.java
            && e.message == "Cannot configure the '$name' extension after it has been accessed."
        if (isExtensionHasBeenAccessedException) {
            getByName(name).configureWith(configurer)
        } else {
            throw e
        }
    }
}

operator fun <T : Any> ExtensionContainer.invoke(type: Class<T>, configurer: (T) -> Unit) {
    if (this is Convention) this.findPlugin(type)?.let { it.configureWith(configurer); return }
    try {
        configure(type, configurer)

    } catch (e: Exception) {
        val isExtensionHasBeenAccessedException = e.javaClass == InvalidUserDataException::class.java
            && (e.message?.startsWith("Cannot configure the '") ?: false)
            && (e.message?.endsWith("' extension after it has been accessed.") ?: false)
        if (isExtensionHasBeenAccessedException) {
            getByType(type).configureWith(configurer)
        } else {
            throw e
        }
    }
}

operator fun <T : Any> ExtensionContainer.get(name: String): T {
    if (this is Convention) this.plugins[name]?.let { return it.uncheckedCast() }
    return this.getByName(name).uncheckedCast()
}

operator fun <T : Any> ExtensionContainer.get(type: Class<T>): T {
    if (this is Convention) this.findPlugin(type)?.let { return it }
    return this.getByType(type)
}

fun <T : Any> ExtensionContainer.getOrNull(name: String): T? {
    if (this is Convention) this.plugins[name]?.let { return it.uncheckedCast() }
    this.findByName(name)?.let { return it.uncheckedCast() }
    return null
}

fun <T : Any> ExtensionContainer.getOrNull(type: Class<T>): T? {
    if (this is Convention) this.findPlugin(type)?.let { return it }
    this.findByType(type)?.let { return it.uncheckedCast() }
    return null
}

operator fun ExtensionContainer?.contains(type: Class<*>) = this != null && getOrNull(type) != null
operator fun ExtensionContainer?.contains(name: String) = this != null && getOrNull<Any>(name) != null


fun <T> ExtensionContainer.add(publicType: Class<T>, instance: T) = add(publicType, publicType.extensionName, instance)

fun <T> ExtensionContainer.createWithAutoName(instanceType: Class<out T>, vararg args: Any?): T {
    return create(instanceType.extensionName, instanceType, *args)
}

fun <T> ExtensionContainer.getOrCreate(publicType: Class<T>, instanceType: Class<out T>, extensionName: String, onCreate: (T) -> Unit = {}): T {
    if (this is Convention) this.findPlugin(publicType)?.let { return it }
    this.findByType(publicType)?.let { return it }
    return create(publicType, extensionName, instanceType).apply(onCreate)
}

fun <T> ExtensionContainer.getOrCreateWithAutoName(publicType: Class<T>, instanceType: Class<out T>, onCreate: (T) -> Unit = {}) = getOrCreate(
    publicType,
    instanceType,
    publicType.extensionName,
    onCreate
)

fun <T> ExtensionContainer.getOrCreate(publicType: Class<T>, extensionName: String, onCreate: (T) -> Unit = {}) = getOrCreate(publicType, publicType, extensionName, onCreate)

fun <T> ExtensionContainer.getOrCreateWithAutoName(publicType: Class<T>, onCreate: (T) -> Unit = {}) = getOrCreateWithAutoName(publicType, publicType, onCreate)


private val Class<*>.extensionName: String get() = "$$" + name.replace('.', '$')
