package name.remal.gradle_plugins.plugins.code_quality.detekt

import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.plugins.code_quality.BaseCodeQualityExtension
import java.io.File

@Extension
class DetektExtension : BaseCodeQualityExtension() {

    var configFile: File? = null

    var languageVersion: String? = null

    var jvmTarget: String? = null

}
