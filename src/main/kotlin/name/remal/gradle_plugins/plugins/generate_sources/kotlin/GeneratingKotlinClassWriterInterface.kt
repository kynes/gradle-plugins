package name.remal.gradle_plugins.plugins.generate_sources.kotlin

import name.remal.gradle_plugins.plugins.generate_sources.GeneratingClassWriterInterface
import name.remal.escapeKotlin as escapeKotlinImpl

interface GeneratingKotlinClassWriterInterface<Self : GeneratingKotlinClassWriterInterface<Self>> : GeneratingClassWriterInterface<Self> {

    fun escapeKotlin(string: String) = escapeKotlinImpl(string)

    override fun writeSuppressWarnings(vararg warnings: String) {
        append("@Suppress")
        if (warnings.isNotEmpty()) {
            append("(")
            warnings.joinTo(this, ", ", transform = { "\"" + escapeKotlin(it) + "\"" })
            append(")\n")
        }

        "edu.umd.cs.findbugs.annotations.SuppressFBWarnings".let {
            if (isClassInClasspath(it)) {
                append("@").append(it)
                if (warnings.isNotEmpty() && warnings.none { it.equals("all", true) }) {
                    append("(")
                    warnings.joinTo(this, ", ", transform = { "\"" + escapeKotlin(it) + "\"" })
                    append(")\n")
                }
            }
        }
    }

    fun writeSuppressWarningsFor(scope: String, vararg warnings: String) {
        append("@").append(scope).append(":Suppress(")
        warnings.joinTo(this, ", ", transform = { "\"" + escapeKotlin(it) + "\"" })
        append(")\n")

        if ("file" != scope) { // 'file' target doesn't work if annotation isn't annotated by @Target: https://youtrack.jetbrains.com/issue/KT-21472
            "edu.umd.cs.findbugs.annotations.SuppressFBWarnings".let {
                if (isClassInClasspath(it)) {
                    append("@").append(scope).append(':').append(it).append("(")
                    warnings.joinTo(this, ", ", transform = { "\"" + escapeKotlin(it) + "\"" })
                    append(")\n")
                }
            }
        }
    }

    override fun writePackage() {
        if (packageName.isNotEmpty()) {
            append("package ").append(packageName).append("\n")
        }
    }

    override fun writeImport(canonicalClassName: String) {
        append("import ").append(canonicalClassName).append("\n")
    }

    override fun writeStaticImport(canonicalClassName: String, member: String) {
        append("import ").append(canonicalClassName).append('.').append(member).append("\n")
    }

}
