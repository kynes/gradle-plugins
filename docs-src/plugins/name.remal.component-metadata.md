The plugin applies [`name.remal.component-capabilities`](name.remal.component-capabilities.md) plugin.

&nbsp;

Plugin that configures some default [component metadata](https://docs.gradle.org/5.0/userguide/managing_transitive_dependencies.html#sec:version_alignment).

Also, the plugin applies component metadata from [Nebula Gradle Resolution Rules](https://github.com/nebula-plugins/gradle-resolution-rules).
