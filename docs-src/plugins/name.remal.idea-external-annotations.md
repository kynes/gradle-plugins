**This plugin works only if [`idea`](https://docs.gradle.org/current/userguide/idea_plugin.html) plugin is applied.**

The plugin applies [`name.remal.idea-settings`](name.remal.idea-settings.md) plugin.

&nbsp;

This plugin sets IDEA external annotations path to `file://$USER_HOME$/idea-external-annotations` if it's not defined for every dependency.

