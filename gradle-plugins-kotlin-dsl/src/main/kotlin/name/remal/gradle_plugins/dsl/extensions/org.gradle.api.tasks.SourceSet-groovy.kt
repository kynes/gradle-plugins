package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.SourceSet

val SourceSet.groovy: SourceDirectorySet
    get() {
        val extension: Any = convention["groovy"]
        val getter = extension.javaClass.getMethod("getGroovy").apply { isAccessible = true }
        return getter.invoke(extension) as SourceDirectorySet
    }

val SourceSet.compileGroovyTaskName: String get() = this.getCompileTaskName("Groovy")
