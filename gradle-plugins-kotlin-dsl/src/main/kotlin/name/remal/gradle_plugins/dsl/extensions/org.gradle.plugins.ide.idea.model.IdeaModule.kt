package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.artifacts.Configuration
import org.gradle.plugins.ide.idea.model.IdeaModule
import org.gradle.plugins.ide.idea.model.internal.GeneratedIdeaScope
import org.gradle.plugins.ide.idea.model.internal.IdeaDependenciesProvider.SCOPE_MINUS
import org.gradle.plugins.ide.idea.model.internal.IdeaDependenciesProvider.SCOPE_PLUS

fun IdeaModule.addToScope(scope: GeneratedIdeaScope, configuration: Configuration) {
    scopes.computeIfAbsent(scope.name, { mutableMapOf() })
        .computeIfAbsent(SCOPE_PLUS, { mutableListOf() })
        .add(configuration)
}

fun IdeaModule.removeFromScope(scope: GeneratedIdeaScope, configuration: Configuration) {
    scopes.computeIfAbsent(scope.name, { mutableMapOf() })
        .computeIfAbsent(SCOPE_MINUS, { mutableListOf() })
        .add(configuration)
}
