package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

import name.remal.buildMap
import name.remal.buildSet
import name.remal.createParentDirectories
import name.remal.default
import name.remal.encodeURIComponent
import name.remal.forInstantiatedWithPropagatedPackage
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.DESCRIPTION_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.DISPLAY_NAME_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.IMPLEMENTATION_CLASS_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.IS_HIDDEN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.MAX_GRADLE_VERSION_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.MIN_GRADLE_VERSION_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.SNAPSHOT_REGEX
import name.remal.gradle_plugins.dsl.TAGS_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.WEBSITE_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.extensions.autoFileTree
import name.remal.gradle_plugins.dsl.extensions.createWithOptionalUniqueSuffix
import name.remal.gradle_plugins.dsl.extensions.fetchHttpStatus
import name.remal.gradle_plugins.dsl.extensions.forClassLoader
import name.remal.gradle_plugins.dsl.extensions.getRequiredResourceAsStream
import name.remal.gradle_plugins.dsl.extensions.include
import name.remal.gradle_plugins.dsl.extensions.logWarn
import name.remal.gradle_plugins.dsl.extensions.nameWithoutExtension
import name.remal.gradle_plugins.dsl.extensions.tempDir
import name.remal.gradle_plugins.dsl.extensions.visitFiles
import name.remal.gradle_plugins.dsl.utils.retryIO
import name.remal.gradle_plugins.plugins.publish.BasePublishToMavenRepository
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.internal.PublishToGradlePluginPortalInvoker
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.internal.impl.PublishToGradlePluginPortalInvokerImpl
import name.remal.loadProperties
import name.remal.nullIfEmpty
import name.remal.resourceNameToClassName
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.net.URL
import kotlin.LazyThreadSafetyMode.NONE

@BuildTask
class PublishToGradlePluginPortalRepository : BasePublishToMavenRepository<GradlePluginPortalRepository>() {

    @get:Internal
    internal val alreadyPublishedPlugins: List<PluginInfo> by lazy {
        repository.plugins.filter { isPluginPublished(it.id!!, publication.version) }
    }

    @get:Internal
    internal val notPublishedPlugins: List<PluginInfo> by lazy {
        repository.plugins.toMutableList().also { it.removeAll(alreadyPublishedPlugins) }
    }

    private fun isPluginPublished(pluginId: String, version: String): Boolean {
        val url = URL("https://plugins.gradle.org/plugin/${encodeURIComponent(pluginId)}/${encodeURIComponent(version)}")
        return retryIO { 200 == url.fetchHttpStatus() }
    }


    init {
        onlyIf {
            if (repository.publishKey.isNullOrEmpty()) {
                throw IllegalStateException("Repository ${repository.name} doesn't have ${GradlePluginPortalRepository::publishKey.name} property set")
            }
            if (repository.publishSecret.isNullOrEmpty()) {
                throw IllegalStateException("Repository ${repository.name} doesn't have ${GradlePluginPortalRepository::publishSecret.name} property set")
            }
            return@onlyIf true
        }

        onlyIf { _ ->
            if (SNAPSHOT_REGEX.containsMatchIn(publication.version)) {
                logWarn("SNAPSHOT version can't be published to {}", repository.name)
                return@onlyIf false
            } else {
                return@onlyIf true
            }
        }

        onlyIf {
            parseExtendedPluginProperties()
            checkThatPluginIdsAreUnique()
            validateArtifacts()
            alreadyPublishedPlugins.forEach { logWarn("Plugin {} has been already published", it.id) }
            return@onlyIf notPublishedPlugins.isNotEmpty()
        }
    }

    private fun parseExtendedPluginProperties() {
        publication.artifacts.filter { it.classifier.isNullOrEmpty() }.forEach forEachArtifact@{ artifact ->
            project.autoFileTree(artifact.file)
                .include("META-INF/gradle-plugins/*.properties")
                .forEach forEachPluginProps@{ pluginPropsFile ->
                    val pluginId = pluginPropsFile.nameWithoutExtension

                    val pluginProps = loadProperties(pluginPropsFile)
                    if ("true" == pluginProps[IS_HIDDEN_DESCRIPTOR_KEY]) return@forEachPluginProps

                    val pluginInfo = repository.plugins.firstOrNull { it.id == pluginId }
                        ?: repository.plugins.createWithOptionalUniqueSuffix(pluginId) { it.id = pluginId }
                    if (pluginInfo.displayName.isNullOrEmpty()) {
                        pluginProps.getProperty(DISPLAY_NAME_PLUGIN_DESCRIPTOR_KEY).nullIfEmpty()?.let { pluginInfo.displayName = it }
                    }
                    if (pluginInfo.description.isNullOrEmpty()) {
                        pluginInfo.description = listOf(
                            pluginProps.getProperty(DESCRIPTION_PLUGIN_DESCRIPTOR_KEY),
                            pluginProps.getProperty(MIN_GRADLE_VERSION_PLUGIN_DESCRIPTOR_KEY)?.let { "Min Gradle version: $it." },
                            pluginProps.getProperty(MAX_GRADLE_VERSION_PLUGIN_DESCRIPTOR_KEY)?.let { "Max Gradle version: $it." }
                        )
                            .asSequence()
                            .filterNotNull()
                            .map(String::trim)
                            .filter(String::isNotEmpty)
                            .joinToString(" ")
                    }
                    if (pluginInfo.websiteUrl.isNullOrEmpty()) {
                        pluginProps.getProperty(WEBSITE_PLUGIN_DESCRIPTOR_KEY).nullIfEmpty()?.let { pluginInfo.websiteUrl = it }
                    }
                    pluginProps.getProperty(TAGS_PLUGIN_DESCRIPTOR_KEY).default().splitToSequence(',', ';')
                        .map(String::trim)
                        .filter(String::isNotEmpty)
                        .forEach { pluginInfo.tags.add(it) }
                }
        }
    }

    private fun checkThatPluginIdsAreUnique() {
        repository.plugins.groupBy(PluginInfo::id).forEach { id, infos ->
            if (2 <= infos.size) {
                throw IllegalStateException("There are ${infos.size} plugins with ID $id")
            }
        }
    }

    private fun validateArtifacts() {
        publication.artifacts.filter { it.classifier.isNullOrEmpty() }.forEach forEachArtifact@{ artifact ->
            val mappings = buildMap<String, String> {
                project.autoFileTree(artifact.file)
                    .include("META-INF/gradle-plugins/*.properties")
                    .visitFiles {
                        val pluginId = it.nameWithoutExtension
                        val pluginProps = loadProperties(it.open())
                        val implementationClassName = pluginProps.getProperty(IMPLEMENTATION_CLASS_PLUGIN_DESCRIPTOR_KEY).nullIfEmpty()
                            ?: throw IllegalStateException("$IMPLEMENTATION_CLASS_PLUGIN_DESCRIPTOR_KEY property can't be found in ${it.relativePath}")
                        put(pluginId, implementationClassName)
                    }
            }

            val allClassNames = buildSet<String> {
                project.autoFileTree(artifact.file).include("**/*.class").visitFiles {
                    add(resourceNameToClassName(it.relativePath.toString()))
                }
            }

            mappings.forEach { pluginId, implementationClassName ->
                if (implementationClassName !in allClassNames) {
                    throw IllegalStateException("Implementation class $implementationClassName for plugin $pluginId can't be found in ${artifact.file}")
                }
            }
        }
    }

    private val gradlePluginPublishPluginJarFile: File by lazy(NONE) {
        tempDir.resolve("gradle-plugin-publish-plugin.jar").also { file ->
            PublishToGradlePluginPortalRepository::class.java
                .getRequiredResourceAsStream("/gradle-plugin-publish-plugin/gradle-plugin-publish-plugin.jar")
                .use { inputStream ->
                    file.createParentDirectories().outputStream().use { inputStream.copyTo(it) }
                }
        }
    }

    @TaskAction
    protected fun doPublish() {
        listOf(gradlePluginPublishPluginJarFile).forClassLoader { classLoader ->
            classLoader.forInstantiatedWithPropagatedPackage(
                PublishToGradlePluginPortalInvoker::class.java,
                PublishToGradlePluginPortalInvokerImpl::class.java
            ) { invoker ->
                invoker.task = this
                invoker.pomFile = pomFile
                invoker.pomDocument = pomDocument
                invoker.publish()
            }
        }
    }

}
