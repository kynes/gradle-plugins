package name.remal.gradle_plugins.utils

import name.remal.gradle_plugins.plugins.vcs.UsernamePasswordVcsAuth
import org.eclipse.jgit.transport.CredentialsProvider
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider

fun UsernamePasswordVcsAuth.toCredentialsProvider(): CredentialsProvider = UsernamePasswordCredentialsProvider(username, password)
