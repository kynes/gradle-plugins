package name.remal.gradle_plugins.plugins.vcs

import org.eclipse.jgit.revwalk.RevCommit
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date

data class Commit(
    val id: String,
    val localDateTime: LocalDateTime,
    val tags: List<String>
) {

    val date: Date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is Commit) return false
        return id == other.id
    }

    override fun hashCode() = id.hashCode()

}


fun RevCommit.toCommit(tags: List<String>) = Commit(
    id = this.name,
    localDateTime = Instant.ofEpochSecond(this.commitTime.toLong()).atZone(ZoneId.systemDefault()).toLocalDateTime(),
    tags = tags
)
