package name.remal.gradle_plugins.dsl.artifact

import java.io.File
import java.io.InputStream

class CachedArtifactsCollection(val artifacts: Set<Artifact>) : BaseHasEntries() {

    constructor(files: Iterable<File>) : this(files.mapTo(mutableSetOf<Artifact>(), ArtifactsCache::get))
    constructor(vararg files: Iterable<File>) : this(files.toList().flatten())
    constructor(vararg files: File) : this(files.toList())
    constructor(file: File) : this(setOf(ArtifactsCache[file]))

    fun getArtifactForEntry(entryName: String): Artifact? {
        return artifacts.firstOrNull { entryName in it.entryNames }
    }

    val entryMapping: Map<String, Artifact> by lazy {
        val result = sortedMapOf<String, Artifact>()
        artifacts.forEach { artifact ->
            artifact.entryNames.forEach {
                if (it !in result) result[it] = artifact
            }
        }
        return@lazy result.toMap()
    }

    override val entryNames: Set<String> by lazy { entryMapping.keys }

    override fun openStream(entryName: String): InputStream {
        val artifact = entryMapping[entryName] ?: throw ArtifactEntryNotFoundException("Artifact entry not found: $entryName")
        return artifact.openStream(entryName)
    }

    override fun forEachEntry(pattern: String?, action: (entry: HasEntries.Entry) -> Unit) {
        artifacts.forEach { it.forEachEntry(pattern, action) }
    }

    override fun toString(): String {
        return "${javaClass.simpleName}[$artifacts]"
    }

}
