package name.remal.gradle_plugins.plugins.dependencies

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import name.remal.gradle_plugins.plugins.dependencies.filtered_dependencies.DependencyFilter
import name.remal.gradle_plugins.plugins.dependencies.filtered_dependencies.FilteredDependency
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import java.io.File

@Extension
class DependencyHandlerFilterExtension(
    private val dependencies: DependencyHandler,
    private val pluginCacheDir: File,
    private val project: Project
) {

    @JvmOverloads
    fun filter(notation: Any, configurer: Action<DependencyFilter> = Action {}): Dependency {
        val dependency: Dependency
        if (notation is Dependency) {
            dependency = notation
        } else {
            dependency = dependencies.create(notation)
        }
        val filter = DependencyFilter()
        configurer.execute(filter)
        return FilteredDependency(dependency, filter, pluginCacheDir, project)
    }

    fun filter(notation: Any, @DelegatesTo(DependencyFilter::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = filter(notation, configurer.toConfigureAction())

}
