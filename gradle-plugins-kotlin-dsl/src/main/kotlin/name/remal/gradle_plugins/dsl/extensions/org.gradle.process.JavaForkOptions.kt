package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.testing.Test
import org.gradle.process.JavaExecSpec
import org.gradle.process.JavaForkOptions

val JavaForkOptions.classpath: FileCollection
    get() = when (this) {
        is JavaExecSpec -> this.classpath
        is Test -> this.classpath
        else -> throw UnsupportedOperationException("Can't get classpath property for ${this.javaClass.name}")
    }
