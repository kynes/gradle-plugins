@file:Suppress("UNNECESSARY_SAFE_CALL")

package name.remal.gradle_plugins.dsl.extensions

import name.remal.default
import org.gradle.api.Project
import org.gradle.api.artifacts.component.ComponentIdentifier
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.gradle.api.artifacts.component.ProjectComponentIdentifier

fun ComponentIdentifier.calculateGroup(project: Project): String? = when (this) {
    is ModuleComponentIdentifier -> this.group.default()
    is ProjectComponentIdentifier -> project.evaluationDependsOn(this.projectPath).group?.toString().default()
    else -> null
}

fun ComponentIdentifier.calculateModule(project: Project): String? = when (this) {
    is ModuleComponentIdentifier -> this.module.default()
    is ProjectComponentIdentifier -> project.evaluationDependsOn(this.projectPath).name?.toString().default()
    else -> null
}

fun ComponentIdentifier.calculateVersion(project: Project): String? = when (this) {
    is ModuleComponentIdentifier -> this.version.default()
    is ProjectComponentIdentifier -> project.evaluationDependsOn(this.projectPath).version?.toString().default()
    else -> null
}
