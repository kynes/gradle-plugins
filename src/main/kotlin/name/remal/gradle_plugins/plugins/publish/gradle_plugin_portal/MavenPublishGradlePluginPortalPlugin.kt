package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.convention
import name.remal.gradle_plugins.dsl.extensions.invoke
import name.remal.gradle_plugins.plugins.environment_variables.EnvironmentVariablesPlugin
import name.remal.gradle_plugins.plugins.publish.MavenPublishPluginId
import name.remal.gradle_plugins.plugins.publish.MavenPublishSettingsPlugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.plugins.PublishingPlugin.PUBLISH_LIFECYCLE_TASK_NAME
import org.gradle.api.tasks.TaskContainer

@Plugin(
    id = "name.remal.maven-publish-gradle-plugin-portal",
    description = "Plugin that allows 'maven-publish' plugin publicate to Gradle Plugin Portal.",
    tags = ["gradle", "plugin", "publish", "publication", "maven", "maven-publish"]
)
@WithPlugins(MavenPublishPluginId::class)
@ApplyPluginClasses(MavenPublishSettingsPlugin::class, EnvironmentVariablesPlugin::class)
class MavenPublishGradlePluginPortalPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction("Allow to publish to gradlePluginPortalPublish repository", order = 1001)
    fun ExtensionContainer.gradlePluginPortalPublish(tasks: TaskContainer, project: Project) {
        invoke(PublishingExtension::class.java) { publishing ->

            publishing.repositories.also {
                it.convention.addPlugin("name.remal.maven-publish-settings.gradlePluginPortalPublish", RepositoryHandlerGradlePluginPortalPublishExtension(it, project))
            }

            publishing.repositories.all(GradlePluginPortalRepository::class.java) { repository ->
                publishing.publications.all(MavenPublication::class.java) { publication ->
                    val publishTaskName = "publish${publication.name.capitalize()}PublicationTo${repository.name.capitalize()}Repository"
                    tasks.create(publishTaskName, PublishToGradlePluginPortalRepository::class.java) { task ->
                        task.repository = repository
                        task.publication = publication
                        tasks.all(PUBLISH_LIFECYCLE_TASK_NAME) { it.dependsOn(task) }
                    }
                }
            }

        }
    }

}
