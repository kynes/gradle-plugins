This plugin creates `osdetector` extension of type [`OSDetectorExtension`](#nameremalgradle_pluginspluginscommonosdetectorextension).

It uses [os-maven-plugin](https://github.com/trustin/os-maven-plugin) to detect OS.

&nbsp;

### `name.remal.gradle_plugins.plugins.common.OSDetectorExtension`
| Property | Type | Description
| --- | :---: | --- |
| `name` | `String` | OS name. For example: `linux`, `windows`, `freebsd`. |
| `arch` | `String` | OS architecture. For example: `x86_32`, `x86_64`. |
| `classifier` | `String` | OS classifier. Basically it equals to `$name-$arch`. |
| `version` | `String?` | OS version. |
| `isLinux` | `boolean` | Is current OS Linux? |
| `isWindows` | `boolean` | Is current OS Windows? |
