package name.remal.gradle_plugins.plugins.ide.idea

import name.remal.gradle_plugins.dsl.AfterProjectEvaluation
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_7
import name.remal.gradle_plugins.dsl.LowestPriorityPluginAction
import name.remal.gradle_plugins.dsl.MinGradleVersion
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.compileOnly
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isRootProject
import name.remal.gradle_plugins.dsl.extensions.removeFromScope
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.testing.TestSourceSetContainer
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSet.TEST_SOURCE_SET_NAME
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.plugins.ide.idea.model.internal.GeneratedIdeaScope

@Plugin(
    id = "name.remal.idea-settings",
    description = "Plugin that configures 'idea' plugin if it's applied.",
    tags = ["idea"]
)
@WithPlugins(IdeaPluginId::class)
class IdeaSettingsPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'ideaSettings' extension`(project: Project) {
        if (project.isRootProject) {
            create(IdeaSettings::class.java, "ideaSettings", IdeaSettingsExtension::class.java, project)
        } else {
            create(IdeaSettings::class.java, "ideaSettings", IdeaSettingsDelegateToRoot::class.java)
        }
    }

    @PluginAction(order = Int.MIN_VALUE)
    fun Project.`Apply 'idea' and this plugins for all projects`() {
        rootProject.allprojects {
            it.applyPlugin(IdeaPluginId)
            it.applyPlugin(IdeaSettingsPlugin::class.java)
        }
    }

    @PluginAction
    fun ExtensionContainer.`Turn ON downloading dependency javadoc`() {
        this[IdeaModel::class.java].module?.apply {
            isDownloadJavadoc = true
        }
    }

    @PluginAction
    fun ExtensionContainer.`Turn ON downloading dependency sources`() {
        this[IdeaModel::class.java].module?.apply {
            isDownloadSources = true
        }
    }

    @PluginActionsGroup
    @WithPlugins(JavaPluginId::class)
    @AfterProjectEvaluation
    inner class `For java project` {

        @PluginAction
        fun ExtensionContainer.`Setup source dirs`(sourceSets: SourceSetContainer) {
            val testSourceSets: Set<SourceSet> = run {
                findByType(TestSourceSetContainer::class.java)?.let { return@run it }
                sourceSets.findByName(TEST_SOURCE_SET_NAME)?.let { return@run setOf(it) }
                return@run emptySet()
            }

            sourceSets.forEach { sourceSet ->
                val isTestSourceSet = sourceSet in testSourceSets
                getByType(IdeaModel::class.java).module?.apply {
                    if (isTestSourceSet) {
                        testSourceDirs = testSourceDirs + sourceSet.allSource.srcDirs
                    } else {
                        sourceDirs = sourceDirs + sourceSet.allSource.srcDirs
                    }
                }
            }
        }

        @LowestPriorityPluginAction
        fun ExtensionContainer.`Setup generated sources dirs`(project: Project) {
            val buildDirPath = project.buildDir.toPath()
            getByType(IdeaModel::class.java).module?.apply {
                generatedSourceDirs = generatedSourceDirs + sequenceOf(sourceDirs, testSourceDirs)
                    .flatten()
                    .filter { it.toPath().startsWith(buildDirPath) }
                    .toSet()
            }
        }

        @PluginAction
        @MinGradleVersion(GRADLE_VERSION_4_7)
        fun ExtensionContainer.`Setup resource dirs`(sourceSets: SourceSetContainer) {
            val testSourceSets: Set<SourceSet> = run {
                findByType(TestSourceSetContainer::class.java)?.let { return@run it }
                sourceSets.findByName(TEST_SOURCE_SET_NAME)?.let { return@run setOf(it) }
                return@run emptySet()
            }

            sourceSets.forEach { sourceSet ->
                val isTestSourceSet = sourceSet in testSourceSets
                getByType(IdeaModel::class.java).module?.apply {
                    if (isTestSourceSet) {
                        testResourceDirs = testResourceDirs + sourceSet.resources.srcDirs
                    } else {
                        resourceDirs = resourceDirs + sourceSet.resources.srcDirs
                    }
                }
            }
        }

        @LowestPriorityPluginAction
        @MinGradleVersion(GRADLE_VERSION_4_7)
        fun ExtensionContainer.`Setup generated resources dirs`(project: Project) {
            val buildDirPath = project.buildDir.toPath()
            getByType(IdeaModel::class.java).module?.apply {
                generatedSourceDirs = generatedSourceDirs + sequenceOf(resourceDirs, testResourceDirs)
                    .flatten()
                    .filter { it.toPath().startsWith(buildDirPath) }
                    .toSet()
            }
        }

        @PluginAction
        fun ExtensionContainer.`Remove compileOnly configuration from TEST scope`(configurations: ConfigurationContainer) {
            this[IdeaModel::class.java].module?.apply {
                removeFromScope(GeneratedIdeaScope.TEST, configurations.compileOnly)
            }
        }

    }

}
