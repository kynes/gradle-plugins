package name.remal.gradle_plugins.plugins.dependencies.component_capabilities

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.AbstractMavenCentralComponentCapabilities
import org.gradle.api.Project
import javax.inject.Inject

@AutoService
class LoggingCapabilities @Inject constructor(project: Project) : AbstractMavenCentralComponentCapabilities("logging", project)
