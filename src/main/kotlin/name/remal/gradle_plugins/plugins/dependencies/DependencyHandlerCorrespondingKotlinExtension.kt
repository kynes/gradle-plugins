package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.plugins.gradle_plugins.CrossVersionGradleLibrary.CORRESPONDING_KOTLIN
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

@Extension
class DependencyHandlerCorrespondingKotlinExtension(
    private val dependencies: DependencyHandler
) {

    fun correspondingKotlin(): Dependency {
        return CORRESPONDING_KOTLIN.dependencyFactory(dependencies)!!
    }

}
