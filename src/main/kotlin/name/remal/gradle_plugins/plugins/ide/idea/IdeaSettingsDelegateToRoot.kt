package name.remal.gradle_plugins.plugins.ide.idea

import name.remal.asSynchronized
import name.remal.gradle_plugins.dsl.Extension
import org.gradle.api.Action
import org.jdom2.Element

@Extension
internal class IdeaSettingsDelegateToRoot : IdeaSettings {

    internal data class Configurer(
        val componentName: String,
        val ideaDirRelativePath: String?,
        val action: Action<Element>
    )

    internal val configurers = mutableListOf<Configurer>().asSynchronized()

    override fun configureIDEAComponent(componentName: String, ideaDirRelativePath: String?, action: Action<Element>) {
        configurers.add(Configurer(componentName, ideaDirRelativePath, action))
    }

}
