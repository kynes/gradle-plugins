package name.remal.gradle_plugins.plugins

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPluginClassesAtTheEnd
import name.remal.gradle_plugins.dsl.ApplyPlugins
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginCondition
import name.remal.gradle_plugins.dsl.extensions.isBuildSrcProject
import name.remal.gradle_plugins.plugins.code_quality.jacoco.JacocoPluginId
import name.remal.gradle_plugins.plugins.code_quality.jacoco.MergedJacocoReportPlugin
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import org.gradle.api.Project

@Plugin(
    id = "name.remal.buildSrc-default-plugins",
    description = "Plugin that applies some plugins by default for buildSrc project. See documentation for full applied plugins list.",
    tags = ["common", "default", "buildSrc"]
)
@ApplyPlugins(
    JacocoPluginId::class
)
@ApplyPluginClasses(
    CommonSettingsPlugin::class,
    MergedJacocoReportPlugin::class
)
@ApplyPluginClassesAtTheEnd(DefaultPluginsPlugin::class)
class BuildSrcProjectDefaultPluginsPlugin : BaseReflectiveProjectPlugin() {

    @PluginCondition
    fun Project.`Current project is buildSrc project or a child of it`() = rootProject.isBuildSrcProject

}
