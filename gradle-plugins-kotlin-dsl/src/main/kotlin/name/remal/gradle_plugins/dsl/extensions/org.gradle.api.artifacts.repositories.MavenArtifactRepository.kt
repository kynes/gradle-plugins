package name.remal.gradle_plugins.dsl.extensions

import name.remal.findCompatibleMethod
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.warn
import org.gradle.api.Action
import org.gradle.api.artifacts.repositories.ArtifactRepository
import org.gradle.api.artifacts.repositories.MavenArtifactRepository

fun MavenArtifactRepository.releasesOnly() = forMavenContent { releasesOnly() }
fun MavenArtifactRepository.snapshotsOnly() = forMavenContent { snapshotsOnly() }


private class MavenArtifactRepositoryMavenContent(private val delegate: Any) {

    companion object {
        private val logger = getGradleLogger(MavenArtifactRepositoryMavenContent::class.java)
    }

    fun releasesOnly() {
        try {
            delegate.javaClass.getMethod("releasesOnly").apply {
                isAccessible = true
                invoke(delegate)
            }
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

    fun snapshotsOnly() {
        try {
            delegate.javaClass.getMethod("snapshotsOnly").apply {
                isAccessible = true
                invoke(delegate)
            }
        } catch (e: Exception) {
            logger.warn(e)
        }
    }

}

private val mavenContentMethod = ArtifactRepository::class.java.findCompatibleMethod("mavenContent", Action::class.java)
    ?.apply { isAccessible = true }

private fun ArtifactRepository.forMavenContent(action: MavenArtifactRepositoryMavenContent.() -> Unit) {
    mavenContentMethod?.invoke(this, Action<Any> { delegate ->
        MavenArtifactRepositoryMavenContent(delegate).apply(action)
    })
}
