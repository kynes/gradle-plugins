package name.remal.gradle_plugins.dsl.artifact

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import java.io.File

object ArtifactsCache {

    @JvmStatic
    private val logger = getGradleLogger(ArtifactsCache::class.java)

    private val cache: LoadingCache<File, Artifact> = CacheBuilder.newBuilder()
        .build(object : CacheLoader<File, Artifact>() {
            override fun load(file: File) = Artifact(file)
        })

    @JvmStatic
    operator fun get(file: File): Artifact = cache[file.absoluteFile]

    @JvmStatic
    fun invalidate(file: File) {
        logger.debug("Invalidating artifacts cache for {}", file)
        cache.invalidate(file)
    }

    @JvmStatic
    fun invalidateAll() {
        logger.debug("Invalidating whole artifacts cache")
        cache.invalidateAll()
    }

}
