package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test

class RepositoryHandlerExtensionsTest : BaseProjectTest() {

    @Suppress("DEPRECATION")
    @Test
    fun jcenterIfNotAdded_added() {
        assertEquals(0, project.repositories.size)
        project.repositories.jcenter()
        assertEquals(1, project.repositories.size)
        project.repositories.jcenterIfNotAdded()
        assertEquals(1, project.repositories.size)
    }

    @Suppress("DEPRECATION")
    @Test
    fun jcenterIfNotAdded_notAdded() {
        assertEquals(0, project.repositories.size)
        project.repositories.jcenterIfNotAdded()
        assertEquals(1, project.repositories.size)
        project.repositories.jcenterIfNotAdded()
        assertEquals(1, project.repositories.size)
    }

}
