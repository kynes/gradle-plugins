package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

data class ExtensionPropertyInfo(
    val name: String,
    val description: String = ""
)
