This plugin executes [SonarLint](https://www.sonarlint.org/) inspections without connecting to a SonarQube server. SonarLint's core JARs and plugins' JARs are downloaded via `sonarlintCore` and `sonarlintPlugins` configurations accordingly.

To resolve dependencies, the plugin adds [SonarSource Maven repository](@sonarsource.repository@) to the project's repositories.

By default, the plugin uses these Sonar plugins:

* [`java`](https://redirect.sonarsource.com/plugins/java.html) (if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) Gradle plugin is applied)
* [`kotlin`](https://redirect.sonarsource.com/plugins/kotlin.html) (if [`kotlin`](https://kotlinlang.org/docs/reference/using-gradle.html) or [`kotlin2js`](https://kotlinlang.org/docs/reference/using-gradle.html) Gradle plugins are applied)
* [`html`](https://redirect.sonarsource.com/plugins/html.html)
* [`javascript`](https://redirect.sonarsource.com/plugins/javascript.html)
* [`xml`](https://redirect.sonarsource.com/plugins/xml.html)

&nbsp;

If [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) Gradle plugin is applied, `sonarlint*` task is created for each [`SourceSet`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/SourceSet.html).

The plugin creates `sonarlint` extension of type [`SonarLintExtension`](#nameremalgradle_pluginspluginscode_qualitysonarsonarlintextension) (it extends [`CodeQualityExtension`](https://docs.gradle.org/current/javadoc/org/gradle/api/plugins/quality/CodeQualityExtension.html)) that allows to configure `sonarlint*` tasks globally.

&nbsp;

Usage:

```groovy tab="Groovy"
sonarlint {
    ignoreFailures = false
    excludes {
        message 'java:S1214'
        message 'kotlin:S100'
        message 'xml:S125'
    }
    includes {
        message 'java:S4266' // Enable java:S4266 which is disabled by default
    }
    ruleParameter('java:S119', 'format', '^[A-Z][a-zA-Z0-9]*$') // Allow upper camel-case for type parameter names
}
```

```kotlin tab="Kotlin"
import name.remal.gradle_plugins.plugins.code_quality.sonar.SonarLintExtension

configure<SonarLintExtension> {
    isIgnoreFailures = false
    excludes {
        message("java:S1214")
        message("kotlin:S100")
        message("xml:S125")
    }
    includes {
        message("java:S4266") // Enable java:S4266 which is disabled by default
    }
    ruleParameter("java:S119", "format", "^[A-Z][a-zA-Z0-9]*\$") // Allow upper camel-case for type parameter names
}
```

&nbsp;

### `name.remal.gradle_plugins.plugins.code_quality.sonar.SonarLintExtension`
| Property | Type | Description
| --- | :---: | --- |
| `excludes` | [`ExcludesExtension`](#nameremalgradle_pluginspluginscode_qualityexcludesextension) | Global excludes. |
| `includes` | [`ExcludesExtension`](#nameremalgradle_pluginspluginscode_qualityexcludesextension) | Global includes. |
| `sonarProperties` | `MutableMap<String, String?>` | Extra Sonar properties. |
| `ruleParameters` | `MutableMap<String, MutableMap<String, String?>>` | Sonar rule properties. |

| Method | Description
| --- | --- |
| `void sonarProperty(Object? name, Object? value)` | Adds new Sonar property. |
| `void sonarProperties(Map<Object?, Object?> map)` | Copy Sonar properties from the map. |
| `void ruleParameter(Object ruleId, Object? name, Object? value)` | Adds new Sonar rule property. |
| `void ruleParameters(Object ruleId, Map<Object?, Object?> map)` | Copy Sonar rule properties from the map. |

### `name.remal.gradle_plugins.plugins.code_quality.ExcludesExtension`
| Property | Type | Description
| --- | :---: | --- |
| `sources` | <nobr>`MutableSet<String>`<nobr> | ANT like exclude/include patterns of source files. |
| `messages` | <nobr>`MutableSet<String>`<nobr> | Excluded/included violation messages. |

| Method | Description
| --- | --- |
| `void source(String value)` | Add ANT like exclude/include pattern of source files. |
| `void sources(String... values)` | Add ANT like exclude/include patterns of source files. |
| `void sources(Iterable<String> values)` | Add ANT like exclude/include patterns of source files. |
| `void message(String value)` | Add violation message to excludes/includes. |
| `void messages(String... values)` | Add violation messages to excludes/includes. |
| `void messages(Iterable<String> values)` | Add violation messages to excludes/includes. |
