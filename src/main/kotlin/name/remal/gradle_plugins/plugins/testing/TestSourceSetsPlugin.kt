package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateConfigurationsPluginAction
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.SimpleTestAdditionalGradleScript
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.addToScope
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.containerWithFactory
import name.remal.gradle_plugins.dsl.extensions.dependsOn
import name.remal.gradle_plugins.dsl.extensions.forEachCorrespondingConfigurationName
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.getDelegateClassOf
import name.remal.gradle_plugins.dsl.extensions.isCompilingSourceSet
import name.remal.gradle_plugins.dsl.extensions.main
import name.remal.gradle_plugins.dsl.extensions.test
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.ide.idea.IdeaPluginId
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.kotlin.KotlinAnyPluginId
import name.remal.nullIfEmpty
import name.remal.superClassHierarchy
import name.remal.uncheckedCast
import name.remal.warn
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.JavaBasePlugin.VERIFICATION_GROUP
import org.gradle.api.plugins.JavaPlugin.TEST_TASK_NAME
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSet.TEST_SOURCE_SET_NAME
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.plugins.ide.idea.model.internal.GeneratedIdeaScope
import org.jetbrains.kotlin.gradle.dsl.KotlinCompile
import org.jetbrains.kotlin.gradle.tasks.AbstractKotlinCompile
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.full.declaredMemberProperties

@Plugin(
    id = "name.remal.test-source-sets",
    description = "Plugin that provides testSourceSet object for creating new source sets for testing. For all created source sets Test task is created. All dependencies are inherited from 'test' source set.",
    tags = ["java", "test"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, TestSettingsPlugin::class)
@SimpleTestAdditionalGradleScript(
    """
    testSourceSets { additional }
    project.setDefaultTasks(project.getDefaultTasks() + ['additionalClasses'])

    apply plugin: 'kotlin'
    project.file('src/main/kotlin/additional/InternalKotlinClass.kt').with {
        parentFile.mkdirs()
        write(""
            + "package additional\n"
            + "internal class InternalKotlinClass\n"
            , "UTF-8"
        )
    }
    project.file('src/additional/kotlin/additional/InternalKotlinClassTest.kt').with {
        parentFile.mkdirs()
        write(""
            + "package additional\n"
            + "class InternalKotlinClassTest {\n"
            + "    private val instance = InternalKotlinClass()\n"
            + "}\n"
            , "UTF-8"
        )
    }
"""
)
class TestSourceSetsPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create testSourceSets extension`(
        sourceSets: SourceSetContainer,
        project: Project,
        configurations: ConfigurationContainer
    ) {
        val container: NamedDomainObjectContainer<SourceSet> = project.containerWithFactory(SourceSet::class.java, sourceSets::create)
        container.add(sourceSets.test)
        container.whenObjectAdded { sourceSets.add(it) }

        val isRemovingRunning = AtomicBoolean(false)
        sourceSets.whenObjectRemoved { sourceSet ->
            if (isRemovingRunning.compareAndSet(false, true)) {
                try {
                    container.remove(sourceSet)
                } finally {
                    isRemovingRunning.set(false)
                }
            }
        }
        container.whenObjectRemoved { testSourceSet ->
            if (isRemovingRunning.compareAndSet(false, true)) {
                try {
                    sourceSets.remove(testSourceSet)
                } finally {
                    isRemovingRunning.set(false)
                }
            }
        }

        container.whenObjectAdded { testSourceSet ->
            testSourceSet.compileClasspath = project.files(
                sourceSets.main.output,
                configurations[testSourceSet.compileClasspathConfigurationName]
            )
            testSourceSet.runtimeClasspath = project.files(
                testSourceSet.output,
                sourceSets.main.output,
                configurations[testSourceSet.runtimeClasspathConfigurationName]
            )
        }

        create(
            TestSourceSetContainer::class.java,
            "testSourceSets",
            TestSourceSetContainer::class.java.getDelegateClassOf(NamedDomainObjectContainer::class.java),
            container
        )
    }

    @CreateConfigurationsPluginAction
    fun TestSourceSetContainer.`Make all testSourceSets configurations extends configurations of 'test' source set`(configurations: ConfigurationContainer) {
        val origTestSourceSet = this.test
        whenObjectAdded {
            it.forEachCorrespondingConfigurationName(origTestSourceSet) { itConfName, testConfName ->
                configurations.all(itConfName) { itConf ->
                    val testConf = configurations.findByName(testConfName) ?: return@all
                    itConf.extendsFrom(testConf)
                }
                configurations.all(testConfName) { testConf ->
                    val itConf = configurations.findByName(itConfName) ?: return@all
                    itConf.extendsFrom(testConf)
                }
            }
        }
    }

    @PluginAction
    fun TestSourceSetContainer.`Create Test task for all testSourceSets`(tasks: TaskContainer) {
        whenObjectAdded { testSourceSet ->
            tasks.create(testSourceSet.testTaskName, Test::class.java) {
                it.group = VERIFICATION_GROUP
                it.description = "Runs ${testSourceSet.name} tests"
                it.conventionMapping.map("testClassesDirs") { testSourceSet.output.classesDirs }
                it.conventionMapping.map("classpath") { testSourceSet.runtimeClasspath }
            }
        }
    }

    @PluginAction
    @WithPlugins(KotlinAnyPluginId::class)
    fun TestSourceSetContainer.`Union Kotlin modules`(tasks: TaskContainer, sourceSets: SourceSetContainer, project: Project) {
        val compileKotlinTasks = tasks.withType(AbstractKotlinCompile::class.java)

        val mainSourceSet: SourceSet by lazy(sourceSets::main)
        val compileMainKotlinTask: AbstractKotlinCompile<*> by lazy {
            compileKotlinTasks.first { it.isCompilingSourceSet(mainSourceSet) }
        }

        whenObjectAdded { testSourceSet ->
            compileKotlinTasks.matching { it.isCompilingSourceSet(testSourceSet) }.all task@{ compileTestKotlin ->
                compileTestKotlin.javaClass.superClassHierarchy.asSequence()
                    .map { it.uncheckedCast<Class<Any>>() }
                    .flatMap { it.kotlin.declaredMemberProperties.asSequence() }
                    .filterIsInstance(KMutableProperty1::class.java)
                    .map { it.uncheckedCast<KMutableProperty1<Any, Any>>() }
                    .filter { it.name == "friendTaskName" }
                    .toList()
                    .nullIfEmpty()
                    ?.let { properties ->
                        properties.forEach { property ->
                            try {
                                property.set(compileTestKotlin, compileMainKotlinTask.name)
                            } catch (e: Throwable) {
                                logger.warn(e)
                            }
                        }
                        return@task
                    }

                if (compileTestKotlin is KotlinCompile<*>) {
                    compileTestKotlin.kotlinOptions.freeCompilerArgs += listOf("-Xfriend-paths=" + mainSourceSet.output.classesDirs.joinToString(","))
                }
            }
        }
    }

    @PluginAction(order = Int.MAX_VALUE)
    fun TaskContainer.`Create runAllTests task`(testSourceSets: TestSourceSetContainer) {
        create("runAllTests") { task ->
            task.group = VERIFICATION_GROUP
            task.description = "Run all test tasks for every source-set"
            task.dependsOn {
                testSourceSets.asSequence()
                    .map(SourceSet::testTaskName)
                    .mapNotNull(this::findByName)
                    .toList()
            }
        }
    }

    @PluginAction(order = Int.MAX_VALUE)
    @WithPlugins(IdeaPluginId::class)
    fun ExtensionContainer.`Setup IDEA scopes`(configurations: ConfigurationContainer, testSourceSets: TestSourceSetContainer) {
        this.findByType(IdeaModel::class.java)?.module?.apply {
            testSourceSets.all { testSourceSet ->
                configurations.findByName(testSourceSet.compileClasspathConfigurationName)?.let { addToScope(GeneratedIdeaScope.TEST, it) }
                configurations.findByName(testSourceSet.runtimeClasspathConfigurationName)?.let { addToScope(GeneratedIdeaScope.TEST, it) }
            }
        }
    }

}

val SourceSet.testTaskName: String
    get() {
        if (TEST_SOURCE_SET_NAME == name) return TEST_TASK_NAME
        return TEST_TASK_NAME + name.capitalize()
    }
