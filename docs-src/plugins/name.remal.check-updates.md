The plugin applies these plugins [`name.remal.check-dependency-updates`](name.remal.check-dependency-updates.md) and  [`name.remal.check-gradle-updates`](name.remal.check-gradle-updates.md).

Also it creates `checkUpdates` task to execute `checkDependencyUpdates` and `checkGradleUpdates` tasks.
