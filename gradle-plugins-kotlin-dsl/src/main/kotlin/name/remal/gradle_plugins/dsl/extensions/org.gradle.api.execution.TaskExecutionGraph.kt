package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Task
import org.gradle.api.execution.TaskExecutionGraph

operator fun TaskExecutionGraph.contains(task: Task): Boolean = hasTask(task)
operator fun TaskExecutionGraph.contains(taskAbsolutePath: String): Boolean = hasTask(taskAbsolutePath)
