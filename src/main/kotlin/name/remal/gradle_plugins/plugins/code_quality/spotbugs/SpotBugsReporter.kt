package name.remal.gradle_plugins.plugins.code_quality.spotbugs

import com.github.spotbugs.SpotBugsExtension
import com.github.spotbugs.SpotBugsReports
import com.github.spotbugs.SpotBugsTask
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.code_quality.QualityTaskReporter
import name.remal.gradle_plugins.plugins.code_quality.findbugs.BaseFindBugsReporter

@AutoService(QualityTaskReporter::class)
class SpotBugsReporter : BaseFindBugsReporter<SpotBugsTask, SpotBugsReports, SpotBugsExtension>()
