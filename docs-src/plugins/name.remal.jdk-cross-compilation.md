**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

&nbsp;

This plugin helps with searching JDK of targetting version if you want to compile using another version then current Gradle process has been started on.

The plugin searches JDK paths using `name.remal.gradle_plugins.plugins.jdk_cross_compilation.JdkInfoProvider`. The service are loaded using [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html).

Currently the plugin supports these languages:

* Java
* Kotlin
* Groovy
