**This plugin works only if [`groovy`](https://docs.gradle.org/current/userguide/groovy_plugin.html) plugin is applied.**

The plugin applies [`name.remal.java-settings`](name.remal.java-settings.md) plugin.

&nbsp;

This plugin helps to configure [`groovy`](https://docs.gradle.org/current/userguide/groovy_plugin.html) plugin.

* Puts Kotlin compiled classes in classpath of [`GroovyCompile`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/compile/GroovyCompile.html) tasks.
* Sets sources default encoding to `UTF-8`.
* Enables displaying deprecation warnings.
* ~~Add `--parameters` compiler option if Groovy version 2.5 and above targetting Java 8 and above.~~ (Gradle still doesn't support it: https://github.com/gradle/gradle/issues/2487)
