package name.remal.gradle_plugins.plugins.prohibited_classes

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingPlugin
import org.gradle.api.plugins.ExtensionContainer

@Plugin(
    id = "name.remal.prohibited-classes",
    description = "Plugin that checks that prohibited classes are not used in compiled class files.",
    tags = ["java", "prohibited-classes"]
)
@ApplyPluginClasses(ClassesProcessingPlugin::class)
class ProhibitedClassesPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'prohibitedClasses' extension`() {
        create("prohibitedClasses", ProhibitedClassesExtension::class.java)
    }

}
