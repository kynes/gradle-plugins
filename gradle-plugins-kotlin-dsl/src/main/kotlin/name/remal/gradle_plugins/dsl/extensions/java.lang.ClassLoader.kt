package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.ClassName
import name.remal.uncheckedCast

private val defineClassMethod = ClassLoader::class.java.getDeclaredMethod(
    "defineClass",
    String::class.java,
    ByteArray::class.java,
    Int::class.javaPrimitiveType,
    Int::class.javaPrimitiveType
).makeAccessible()

fun <T> ClassLoader.defineClass(name: ClassName, bytecode: ByteArray): Class<T> {
    return defineClassMethod.invokeForInstance(this, name, bytecode, 0, bytecode.size)
}
