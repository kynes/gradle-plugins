package name.remal.gradle_plugins.plugins.signing

import name.remal.createParentDirectories
import name.remal.decodeBase64
import name.remal.gradle_plugins.dsl.AfterProjectEvaluation
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.EnvironmentVariable
import name.remal.gradle_plugins.dsl.EnvironmentVariable.EnvironmentVariables
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_8
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.ext
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.invoke
import name.remal.gradle_plugins.dsl.extensions.newTempFile
import name.remal.gradle_plugins.dsl.extensions.signTaskName
import name.remal.gradle_plugins.plugins.publish.MavenPublishPluginId
import name.remal.newTempFile
import name.remal.nullIfEmpty
import name.remal.use
import org.gradle.api.Project
import org.gradle.api.invocation.Gradle
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.tasks.PublishToMavenRepository
import org.gradle.api.tasks.TaskContainer
import org.gradle.plugins.signing.SigningExtension
import org.gradle.util.GradleVersion
import java.io.File
import java.lang.System.getenv
import java.util.concurrent.atomic.AtomicBoolean

@Plugin(
    id = "name.remal.signing-settings",
    description = "Plugin that configures 'signing' plugin if it's applied.",
    tags = ["signing", "sign"]
)
@WithPlugins(SigningPluginId::class)
class SigningSettingsPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private const val KEY_ID_PROPERTY = "signing.keyId"
        private const val PASSWORD_PROPERTY = "signing.password"
        private const val SECRET_KEY_RING_PROPERTY = "signing.secretKeyRingFile"

        private const val KEY_ID_ENV = "SIGNING_KEY_ID"
        private const val PASSWORD_ENV = "SIGNING_PASSWORD"
        private const val SECRET_KEY_RING_BASE64_ENV = "SIGNING_SECRET_KEY_RING_BASE64"
        private const val SIGNING_SECRET_KEY_RING_FILE_ENV = "SIGNING_SECRET_KEY_RING_FILE"

        private const val CLASSPATH_PREFIX = "classpath:"
    }

    @PluginAction("Set '$KEY_ID_PROPERTY' property from '$KEY_ID_ENV' environment variable", order = Int.MIN_VALUE)
    @EnvironmentVariable(KEY_ID_ENV, description = "If the variable is set, its value is used for '$KEY_ID_PROPERTY' property")
    fun Project.setSigningKeyIdProperty() {
        if (isPropertyNotSet(KEY_ID_PROPERTY)) {
            getenv(KEY_ID_ENV).nullIfEmpty()?.let {
                ext[KEY_ID_PROPERTY] = it
            }
        }
    }

    @PluginAction("Set '$PASSWORD_PROPERTY' property from '$PASSWORD_ENV' environment variable", order = Int.MIN_VALUE)
    @EnvironmentVariable(PASSWORD_ENV, description = "If the variable is set, its value is used for '$PASSWORD_PROPERTY' property")
    fun Project.setSigningPasswordProperty() {
        if (isPropertyNotSet(PASSWORD_PROPERTY)) {
            getenv(PASSWORD_ENV).nullIfEmpty()?.let {
                ext[PASSWORD_PROPERTY] = it
            }
        }
    }

    @PluginAction(
        "Set '$SECRET_KEY_RING_PROPERTY' property to a temp file with Base64-encoded content of '$SECRET_KEY_RING_BASE64_ENV' and '${SECRET_KEY_RING_BASE64_ENV}_1'..'${SECRET_KEY_RING_BASE64_ENV}_9' environment variables",
        order = Int.MIN_VALUE
    )
    @EnvironmentVariables(
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_1"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_2"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_3"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_4"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_5"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_6"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_7"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_8"),
        EnvironmentVariable(SECRET_KEY_RING_BASE64_ENV + "_9"),
        description = "If the variable is set, its value is written to a temp file that will be used for '$SECRET_KEY_RING_PROPERTY' property"
    )
    fun Project.setSigningSecretKeyRingFilePropertyToTempFileBase64Divided() {
        if (isPropertyNotSet(SECRET_KEY_RING_PROPERTY)) {
            val base64 = buildString {
                getenv(SECRET_KEY_RING_BASE64_ENV).nullIfEmpty()?.let { append(it) }
                for (n in (1..9)) {
                    getenv(SECRET_KEY_RING_BASE64_ENV + "_$n").nullIfEmpty()?.let { append(it) }
                }
            }
            base64.nullIfEmpty()?.let { content ->
                val secretKeyRingFile = newTempFile("secret-key-ring-", doDeleteOnExit = false).absoluteFile
                //gradle.buildFinished { secretKeyRingFile.delete() }
                secretKeyRingFile.createParentDirectories().writeBytes(decodeBase64(content))
                ext[SECRET_KEY_RING_PROPERTY] = secretKeyRingFile.path
            }
        }
    }

    @PluginAction("Set '$SECRET_KEY_RING_PROPERTY' property from '$SIGNING_SECRET_KEY_RING_FILE_ENV' environment variable", order = Int.MIN_VALUE + 10)
    @EnvironmentVariable(SIGNING_SECRET_KEY_RING_FILE_ENV, description = "If the variable is set, its value is used for '$SECRET_KEY_RING_PROPERTY' property")
    fun Project.setSigningSecretKeyRingFileProperty() {
        if (isPropertyNotSet(SECRET_KEY_RING_PROPERTY)) {
            getenv(SIGNING_SECRET_KEY_RING_FILE_ENV).nullIfEmpty()?.let {
                ext[SECRET_KEY_RING_PROPERTY] = it
            }
        }
    }


    @PluginAction("Support '$CLASSPATH_PREFIX' prefix for '$SECRET_KEY_RING_PROPERTY' property", order = Int.MIN_VALUE + 100)
    fun Project.secretKeyRingFileClasspathPrefix() {
        if (isPropertySet(SECRET_KEY_RING_PROPERTY)) {
            val secretKeyRingFilePath = property(SECRET_KEY_RING_PROPERTY).toString()
            if (secretKeyRingFilePath.startsWith(CLASSPATH_PREFIX)) {
                val secretKeyRingResource = secretKeyRingFilePath.substring(CLASSPATH_PREFIX.length).trimStart('/')
                val secretKeyRingResourceURL = SigningSettingsPlugin::class.java.classLoader.getResource(secretKeyRingResource)
                    ?: throw IllegalStateException("Classpath resource can't be found: $secretKeyRingResource")
                val secretKeyRingName = secretKeyRingResource.substringAfterLast('/')
                val secretKeyRingFile = newTempFile(
                    prefix = secretKeyRingName.substringBeforeLast('.') + '-',
                    suffix = '.' + secretKeyRingResource.substringAfterLast('.', "temp")
                ).absoluteFile
                gradle.buildFinished { secretKeyRingFile.delete() }
                secretKeyRingResourceURL.openStream().use { inputStream ->
                    secretKeyRingFile.createParentDirectories().outputStream().use { outputStream ->
                        inputStream.copyTo(outputStream)
                    }
                }
                ext[SECRET_KEY_RING_PROPERTY] = secretKeyRingFile.path
            }
        }
    }


    @PluginAction("Make '$SECRET_KEY_RING_PROPERTY' point to absolute file path", order = Int.MAX_VALUE)
    fun Project.secretKeyRingFileAbsolute() {
        if (isPropertySet(SECRET_KEY_RING_PROPERTY)) {
            ext[SECRET_KEY_RING_PROPERTY] = File(property(SECRET_KEY_RING_PROPERTY).toString()).absolutePath
        }
    }


    @WithPlugins(MavenPublishPluginId::class)
    @PluginActionsGroup
    inner class `If 'maven-publish' plugin is applied` {

        @PluginAction
        @AfterProjectEvaluation
        fun ExtensionContainer.`Sign all Maven publications`(tasks: TaskContainer, gradle: Gradle) {
            val isWarnLogged = AtomicBoolean(false)
            val signing = this[SigningExtension::class.java]
            invoke(PublishingExtension::class.java) { publishing ->
                publishing.publications.all(MavenPublication::class.java) { publication ->
                    if (publication.signTaskName !in tasks) {

                        if (GradleVersion.current() >= GRADLE_VERSION_4_8) {
                            val signTasks = signing.sign(publication)
                            signTasks.forEach { it.isRequired = false }

                            gradle.taskGraph.whenReady { graph ->
                                val hasPublishTasksInGraph = graph.allTasks.any {
                                    it is PublishToMavenRepository && it.publication == publication
                                }
                                if (hasPublishTasksInGraph) {
                                    signTasks.forEach { it.isRequired = true }
                                }
                            }

                        } else {
                            if (isWarnLogged.compareAndSet(false, true)) {
                                logger.warn("Publication signing available since ${GRADLE_VERSION_4_8.native}")
                            }
                        }

                    }
                }
            }
        }

    }


    private fun Project.isPropertySet(propertyName: String) = findProperty(propertyName)?.toString().nullIfEmpty() != null
    private fun Project.isPropertyNotSet(propertyName: String) = !isPropertySet(propertyName)

}
