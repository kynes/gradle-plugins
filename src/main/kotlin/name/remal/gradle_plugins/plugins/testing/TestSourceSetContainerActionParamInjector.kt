package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector.ActionParamInjector
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import org.gradle.api.Project

@AutoService
class TestSourceSetContainerActionParamInjector : ActionParamInjector<TestSourceSetContainer>() {

    override fun createValue(project: Project): TestSourceSetContainer {
        project.getOrNull(TestSourceSetContainer::class.java)?.let { return it }
        project.applyPlugin(JavaPluginId)
        project.applyPlugin(TestSourceSetsPlugin::class.java)
        return project[TestSourceSetContainer::class.java]
    }

}
