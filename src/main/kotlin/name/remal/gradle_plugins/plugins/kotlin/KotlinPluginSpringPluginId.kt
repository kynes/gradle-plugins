package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinPluginSpringPluginId : PluginId("kotlin-spring", "org.jetbrains.kotlin.plugin.spring")
