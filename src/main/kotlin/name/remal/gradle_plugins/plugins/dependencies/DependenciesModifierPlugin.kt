package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.loadServicesList
import org.gradle.api.artifacts.ConfigurationContainer

@Plugin(
    id = "name.remal.dependencies-modifier",
    description = "Plugin that modifies dependencies.",
    tags = ["common", "dependencies"]
)
class DependenciesModifierPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val dependencyModifiers = loadServicesList(DependencyModifier::class.java)
    }

    @PluginAction
    fun ConfigurationContainer.`Modify dependencies of all configurations`() {
        if (dependencyModifiers.isNotEmpty()) {
            all {
                it.dependencies.all { dep ->
                    dependencyModifiers.forEach { it.modify(dep) }
                }
            }
        }
    }

}
