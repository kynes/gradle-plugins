package name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.createWithAutoName
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.plugins.ExtensionContainer
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class ApplyOnRootProjectMixinTest : BaseProjectTest() {

    @Test
    fun `Is applied on root project`() {
        val childProject = project.newChildProject("child")
        childProject.applyPlugin(ApplyOnRootPlugin::class.java)
        assertFalse(ApplyOnRootExtension::class.java in childProject)
        assertTrue(ApplyOnRootExtension::class.java in project)
    }

}

@Plugin(id = "", description = "", tags = [])
private class ApplyOnRootPlugin : BaseReflectiveProjectPlugin(), ApplyOnRootProjectMixin {
    @PluginAction
    fun ExtensionContainer.addExtension() = createWithAutoName(ApplyOnRootExtension::class.java)
}

@Extension
private class ApplyOnRootExtension
