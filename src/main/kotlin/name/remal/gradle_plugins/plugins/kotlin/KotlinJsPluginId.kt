package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinJsPluginId : PluginId("kotlin2js", "kotlin-platform-js", "org.jetbrains.kotlin.js", "org.jetbrains.kotlin.platform.js")
