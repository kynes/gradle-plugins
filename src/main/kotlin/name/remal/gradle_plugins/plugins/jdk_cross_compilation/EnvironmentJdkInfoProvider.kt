package name.remal.gradle_plugins.plugins.jdk_cross_compilation

import name.remal.gradle_plugins.api.AutoService
import name.remal.nullIfEmpty
import org.gradle.api.JavaVersion
import java.io.File
import java.lang.System.getenv

@AutoService
class EnvironmentJdkInfoProvider : JdkInfoProvider {

    override fun provide(javaVersion: JavaVersion): JdkInfo? {
        getenv("JDK_${javaVersion.majorVersion}").nullIfEmpty()?.let { return JdkInfo(File(it)) }
        getenv("JDK_HOME_${javaVersion.majorVersion}").nullIfEmpty()?.let { return JdkInfo(File(it)) }
        return null
    }

    override fun getOrder(): Int {
        return Int.MIN_VALUE
    }

}
