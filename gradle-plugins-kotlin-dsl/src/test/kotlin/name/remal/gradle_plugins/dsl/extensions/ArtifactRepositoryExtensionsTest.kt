package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_5_1
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.gradle_plugins.testing.dsl.testMavenRepository
import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.artifacts.UnresolvedDependency
import org.gradle.util.GradleVersion
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class ArtifactRepositoryExtensionsTest : BaseProjectTest() {

    @Test
    fun testIncludeExcludeModule() {
        project.testMavenRepository {
            component("test", "a", "1") { jar() }
            component("test", "b", "1") { jar() }
            component("test", "c", "1") { jar() }
        }.apply {
            includeModule("test", "a")
        }

        project.testMavenRepository {
            component("test", "a", "2") { jar() }
            component("test", "b", "2") { jar() }
        }.apply {
            excludeModule("test", "a")
        }

        val lenientConfiguration = project.configurations.detachedConfiguration(
            project.dependencies.create("test:a:+"),
            project.dependencies.create("test:b:+"),
            project.dependencies.create("test:c:+")
        ).resolvedConfiguration.lenientConfiguration

        val resolvedNotations = lenientConfiguration
            .allModuleDependencies.asSequence()
            .map(ResolvedDependency::notation)
            .sorted()
            .toList()
        val unresolvedNotations = lenientConfiguration
            .unresolvedModuleDependencies.asSequence()
            .map(UnresolvedDependency::notation)
            .sorted()
            .toList()

        if (GradleVersion.current() < GRADLE_VERSION_5_1) {
            assertEquals(3, resolvedNotations.size)
            assertEquals(DependencyNotation("test", "a", "2"), resolvedNotations[0])
            assertEquals(DependencyNotation("test", "b", "2"), resolvedNotations[1])
            assertEquals(DependencyNotation("test", "c", "1"), resolvedNotations[2])

            assertTrue(unresolvedNotations.isEmpty())

        } else {
            assertEquals(2, resolvedNotations.size)
            assertEquals(DependencyNotation("test", "a", "1"), resolvedNotations[0])
            assertEquals(DependencyNotation("test", "b", "2"), resolvedNotations[1])

            assertEquals(1, unresolvedNotations.size)
            assertTrue(DependencyNotationMatcher("test:c").matches(unresolvedNotations[0]))
        }
    }

    @Test
    fun testIncludeExcludeVersion() {
        project.testMavenRepository {
            component("test", "a", "1") { jar() }
            component("test", "b", "2-suffix") { jar() }
        }.apply {
            includeVersionByRegex(".*", ".*", "1")
        }

        project.testMavenRepository {
            component("test", "a", "2-suffix") { jar() }
            component("test", "b", "1") { jar() }
        }.apply {
            includeVersionByRegex(".*", ".*", "^[^-]+((?!-suffix)-[^-]+)*\$")
        }

        val lenientConfiguration = project.configurations.detachedConfiguration(
            project.dependencies.create("test:a:+"),
            project.dependencies.create("test:b:+")
        ).resolvedConfiguration.lenientConfiguration

        val resolvedNotations = lenientConfiguration
            .allModuleDependencies.asSequence()
            .map(ResolvedDependency::notation)
            .sorted()
            .toList()
        val unresolvedNotations = lenientConfiguration
            .unresolvedModuleDependencies.asSequence()
            .map(UnresolvedDependency::notation)
            .sorted()
            .toList()

        if (GradleVersion.current() < GRADLE_VERSION_5_1) {
            assertThat(resolvedNotations)
                .contains(
                    DependencyNotation("test", "a", "2-suffix"),
                    DependencyNotation("test", "b", "2-suffix")
                )

            assertThat(unresolvedNotations).isEmpty()

        } else {
            assertThat(resolvedNotations)
                .contains(
                    DependencyNotation("test", "a", "1"),
                    DependencyNotation("test", "b", "1")
                )

            assertThat(unresolvedNotations).isEmpty()
        }
    }

}
