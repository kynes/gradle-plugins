package name.remal.gradle_plugins.plugins.publish.nexus_staging

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.setupTasksAfterEvaluateOrNow
import name.remal.gradle_plugins.dsl.extensions.setupTasksDependenciesAfterEvaluateOrNow
import name.remal.gradle_plugins.plugins.publish.MavenPublishPluginId
import name.remal.withFragment
import name.remal.withPath
import name.remal.withQuery
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.publish.maven.tasks.PublishToMavenRepository
import org.gradle.api.publish.plugins.PublishingPlugin.PUBLISH_LIFECYCLE_TASK_NAME
import org.gradle.api.publish.plugins.PublishingPlugin.PUBLISH_TASK_GROUP
import org.gradle.api.tasks.TaskContainer
import java.net.URI
import java.util.concurrent.atomic.AtomicInteger

@Plugin(
    id = "name.remal.maven-publish-nexus-staging",
    description = "Plugin that releases staging Nexus Maven repositories.",
    tags = ["publish", "publication", "maven", "maven-publish", "nexus", "staging"]
)
@WithPlugins(MavenPublishPluginId::class)
class MavenPublishNexusStagingPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction(order = -10)
    fun TaskContainer.`Create 'releaseNexusRepositories' task`(project: Project) {
        create("releaseNexusRepositories") { task ->
            task.group = PUBLISH_TASK_GROUP
            project.setupTasksAfterEvaluateOrNow { _ ->
                findByName(PUBLISH_LIFECYCLE_TASK_NAME)?.group?.let { task.group = it }
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Make the last PublishToMavenRepository task in tasks graph release staging repositories`(project: Project, tasks: TaskContainer) {
        val releaseNexusRepositories: Task = get("releaseNexusRepositories")

        project.setupTasksAfterEvaluateOrNow { _ ->
            all(PublishToMavenRepository::class.java) forEachPublishTask@{ publishTask ->
                val repository = publishTask.repository
                if (repository == null) {
                    logger.warn("{}: repository == null", publishTask)
                    return@forEachPublishTask
                }

                val stagingNexusURI = repository.stagingNexusURI
                if (stagingNexusURI == null) {
                    return@forEachPublishTask
                }

                tasks.create(publishTask.nexusReleaseTaskName, ReleaseNexusStagingRepository::class.java) { releaseTask ->
                    releaseTask.publication = publishTask.publication
                    releaseTask.repository = repository
                    releaseTask.stagingNexusURI = stagingNexusURI

                    releaseTask.group = publishTask.group

                    releaseNexusRepositories.dependsOn(releaseTask)
                }
            }
        }
    }

    @PluginAction(order = Int.MAX_VALUE)
    fun `All release tasks must run after publish tasks`(currentTasks: TaskContainer, currentProject: Project) {
        currentProject.rootProject.allprojects { project ->
            project.tasks.all(PublishToMavenRepository::class.java) { publishTask ->
                currentTasks.all(ReleaseNexusStagingRepository::class.java) { releaseTask ->
                    releaseTask.mustRunAfter(publishTask)
                }
            }
        }
    }

    @PluginAction(order = Int.MAX_VALUE)
    fun `Execute release task sequentially`(currentTasks: TaskContainer, currentProject: Project) {
        val allprojects = currentProject.rootProject.allprojects
        val notEvaluatedProjectsCounter = AtomicInteger(allprojects.size)

        fun setupTasksDependenciesAfterEvaluateOrNow() {
            if (notEvaluatedProjectsCounter.decrementAndGet() > 0) return

            var prevTask: Task? = null
            allprojects.asSequence()
                .flatMap { it.tasks.asSequence() }
                .filterIsInstance(ReleaseNexusStagingRepository::class.java)
                .forEach { task ->
                    if (prevTask == null) {
                        prevTask = task
                    } else {
                        task.shouldRunAfter(prevTask)
                    }
                }
        }

        allprojects.forEach {
            it.setupTasksDependenciesAfterEvaluateOrNow(Int.MAX_VALUE) { _ ->
                setupTasksDependenciesAfterEvaluateOrNow()
            }
        }
    }


    private val PublishToMavenRepository.nexusReleaseTaskName: String
        get() {
            if (name.startsWith("publish") && name.endsWith("Repository")) {
                return "release" + name.substring("publish".length, name.length - "Repository".length) + "NexusRepository"
            }
            return name + "NexusRelease"
        }

    private val MavenArtifactRepository.stagingNexusURI: URI?
        get() {
            val repositoryURI = url
            val basePath = "/service/local/staging/deploy/maven2".let { suffix ->
                val normalizedPath = repositoryURI.path.trimEnd('/')
                if (!normalizedPath.endsWith(suffix)) {
                    return null
                }
                normalizedPath.substring(0, normalizedPath.length - suffix.length) + '/'
            }
            return repositoryURI
                .withPath(basePath)
                .withQuery(null)
                .withFragment(null)
        }

}
