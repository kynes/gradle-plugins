package name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin

import name.remal.gradle_plugins.dsl.PluginCondition
import name.remal.gradle_plugins.dsl.extensions.isRootProject
import org.gradle.api.Project

interface CurrentProjectIsARootProjectMixin {

    @PluginCondition
    fun Project.`Current project is a root project`(): Boolean = isRootProject

}
