package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.SourceSet

val SourceSet.kotlin: SourceDirectorySet
    get() {
        val extension: Any = convention["kotlin"]
        val getter = extension.javaClass.getMethod("getKotlin").apply { isAccessible = true }
        return getter.invoke(extension) as SourceDirectorySet
    }

val SourceSet.compileKotlinTaskName: String get() = this.getCompileTaskName("Kotlin")
val SourceSet.compileKotlin2JsTaskName: String get() = this.getCompileTaskName("Kotlin2Js")
