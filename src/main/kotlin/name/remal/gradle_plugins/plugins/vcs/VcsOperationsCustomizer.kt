package name.remal.gradle_plugins.plugins.vcs

import name.remal.copyOnWriteListOf

interface VcsOperationsCustomizer {

    companion object {
        val VCS_OPERATIONS_CUSTOMIZERS = copyOnWriteListOf<VcsOperationsCustomizer>()
    }

    fun customize(vcsOperations: VcsOperations)

}
