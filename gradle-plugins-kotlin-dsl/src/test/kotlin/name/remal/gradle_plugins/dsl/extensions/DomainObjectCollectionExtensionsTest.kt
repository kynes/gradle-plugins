package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_CENTRAL_REPO_NAME
import org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_LOCAL_REPO_NAME
import org.gradle.api.internal.artifacts.dsl.DefaultRepositoryHandler.DEFAULT_BINTRAY_JCENTER_REPO_NAME
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.ivy.IvyPublication
import org.gradle.api.publish.maven.MavenPublication
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class DomainObjectCollectionExtensionsTest : BaseProjectTest() {

    @Test
    fun `simple useDefault not empty`() {
        val repositories = project.repositories

        repositories.mavenLocal()

        repositories.useDefault {
            jcenter()
            mavenCentral()
        }
        assertEquals(1, repositories.size)
        assertEquals(DEFAULT_MAVEN_LOCAL_REPO_NAME, repositories[0].name)
    }

    @Test
    fun `simple useDefault add`() {
        val repositories = project.repositories

        repositories.useDefault {
            jcenter()
            mavenCentral()
        }
        assertEquals(2, repositories.size)
        assertEquals(DEFAULT_BINTRAY_JCENTER_REPO_NAME, repositories[0].name)
        assertEquals(DEFAULT_MAVEN_CENTRAL_REPO_NAME, repositories[1].name)

        repositories.mavenLocal()
        assertEquals(1, repositories.size)
        assertEquals(DEFAULT_MAVEN_LOCAL_REPO_NAME, repositories[0].name)
    }

    @Test
    fun `typed useDefault not empty`() {
        project.applyPlugin("java")
        project.applyPlugin("ivy-publish")
        project.applyPlugin("maven-publish")
        project.invoke(PublishingExtension::class.java) { publishing ->
            val publications = publishing.publications

            publications.useDefault(IvyPublication::class.java) {
                create("ivy-default", IvyPublication::class.java)
            }

            publications.create("maven", MavenPublication::class.java)

            publications.useDefault(MavenPublication::class.java) {
                create("maven-default-1", MavenPublication::class.java)
                create("maven-default-2", MavenPublication::class.java)
            }

            assertEquals(1, publications.filterIsInstance(IvyPublication::class.java).size)
            assertEquals("ivy-default", publications.filterIsInstance(IvyPublication::class.java)[0].name)

            assertEquals(1, publications.filterIsInstance(MavenPublication::class.java).size)
            assertEquals("maven", publications.filterIsInstance(MavenPublication::class.java)[0].name)
        }
    }

    @Test
    fun `typed useDefault add`() {
        project.applyPlugin("java")
        project.applyPlugin("ivy-publish")
        project.applyPlugin("maven-publish")
        project.invoke(PublishingExtension::class.java) { publishing ->
            val publications = publishing.publications

            publications.useDefault(IvyPublication::class.java) {
                create("ivy-default", IvyPublication::class.java)
            }

            publications.useDefault(MavenPublication::class.java) {
                create("maven-default-1", MavenPublication::class.java)
                create("maven-default-2", MavenPublication::class.java)
            }

            publications.create("maven", MavenPublication::class.java)

            assertEquals(1, publications.filterIsInstance(IvyPublication::class.java).size)
            assertEquals("ivy-default", publications.filterIsInstance(IvyPublication::class.java)[0].name)

            assertEquals(1, publications.filterIsInstance(MavenPublication::class.java).size)
            assertEquals("maven", publications.filterIsInstance(MavenPublication::class.java)[0].name)
        }
    }

    @Test
    fun removeDefaultObjects() {
        val repositories = project.repositories

        repositories.useDefault {
            jcenter()
        }
        assertEquals(1, repositories.size)
        assertEquals(DEFAULT_BINTRAY_JCENTER_REPO_NAME, repositories[0].name)

        assertTrue(repositories.removeDefaultObjects())
        assertTrue(repositories.isEmpty())

        repositories.mavenCentral()
        assertEquals(1, repositories.size)
        assertEquals(DEFAULT_MAVEN_CENTRAL_REPO_NAME, repositories[0].name)
        assertFalse(repositories.removeDefaultObjects())
        assertFalse(repositories.isEmpty())
    }

}
