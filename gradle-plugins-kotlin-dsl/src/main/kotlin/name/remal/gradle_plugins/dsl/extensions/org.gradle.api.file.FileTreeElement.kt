package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.FileTreeElement

val FileTreeElement.isFile get() = !isDirectory

val FileTreeElement.nameWithoutExtension: String get() = name.substringBeforeLast('.')

fun FileTreeElement.readBytes(): ByteArray = open().readAll()
