package name.remal.gradle_plugins.plugins.code_quality.detekt

import org.gradle.api.reporting.ReportContainer
import org.gradle.api.reporting.SingleFileReport
import org.gradle.api.tasks.Internal

interface DetektReports : ReportContainer<SingleFileReport> {

    @get:Internal
    val findBugsXml: SingleFileReport

    @get:Internal
    val html: SingleFileReport

}
