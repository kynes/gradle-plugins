package name.remal.gradle_plugins.dsl.extensions

import name.remal.debug
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.dsl.utils.getGradleLoggerForCurrentClass
import org.gradle.api.JavaVersion
import org.gradle.api.file.FileVisitDetails
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.compile.AbstractCompile

val AbstractCompile.sourceJavaVersion: JavaVersion get() = sourceCompatibility?.let(JavaVersion::toVersion) ?: JavaVersion.current()
val AbstractCompile.targetJavaVersion: JavaVersion get() = targetCompatibility?.let(JavaVersion::toVersion) ?: JavaVersion.current()

val AbstractCompile.isSourceJava5Compatible: Boolean get() = sourceJavaVersion.isJava5Compatible
val AbstractCompile.isSourceJava6Compatible: Boolean get() = sourceJavaVersion.isJava6Compatible
val AbstractCompile.isSourceJava7Compatible: Boolean get() = sourceJavaVersion.isJava7Compatible
val AbstractCompile.isSourceJava8Compatible: Boolean get() = sourceJavaVersion.isJava8Compatible
val AbstractCompile.isSourceJava9Compatible: Boolean get() = sourceJavaVersion.isJava9Compatible
val AbstractCompile.isSourceJava10Compatible: Boolean get() = sourceJavaVersion.isJava10Compatible
val AbstractCompile.isSourceJava11Compatible: Boolean get() = sourceJavaVersion.isJava11Compatible
val AbstractCompile.isSourceJava12Compatible: Boolean get() = sourceJavaVersion.isJava12Compatible
val AbstractCompile.isSourceJava13Compatible: Boolean get() = sourceJavaVersion.isJava13Compatible

val AbstractCompile.isTargetJava5Compatible: Boolean get() = targetJavaVersion.isJava5Compatible
val AbstractCompile.isTargetJava6Compatible: Boolean get() = targetJavaVersion.isJava6Compatible
val AbstractCompile.isTargetJava7Compatible: Boolean get() = targetJavaVersion.isJava7Compatible
val AbstractCompile.isTargetJava8Compatible: Boolean get() = targetJavaVersion.isJava8Compatible
val AbstractCompile.isTargetJava9Compatible: Boolean get() = targetJavaVersion.isJava9Compatible
val AbstractCompile.isTargetJava10Compatible: Boolean get() = targetJavaVersion.isJava10Compatible
val AbstractCompile.isTargetJava11Compatible: Boolean get() = targetJavaVersion.isJava11Compatible
val AbstractCompile.isTargetJava12Compatible: Boolean get() = targetJavaVersion.isJava12Compatible
val AbstractCompile.isTargetJava13Compatible: Boolean get() = targetJavaVersion.isJava13Compatible


fun AbstractCompile.isCompilingSourceSet(sourceSet: SourceSet): Boolean {
    val destinationDir = try {
        this.destinationDir ?: return false
    } catch (e: UninitializedPropertyAccessException) {
        getGradleLogger(javaClass).debug(e)
        return false
    }
    val absoluteDestinationDir = destinationDir.absoluteFile
    return sourceSet.output.classesDirs.any { it.absoluteFile == absoluteDestinationDir }
}

fun AbstractCompile.isNotCompilingSourceSet(sourceSet: SourceSet) = !isCompilingSourceSet(sourceSet)

fun AbstractCompile.forEachCreatedClassFile(action: (fileVisitDetails: FileVisitDetails) -> Unit) {
    val destinationDir = try {
        this.destinationDir ?: return
    } catch (e: UninitializedPropertyAccessException) {
        fileLogger.debug(e)
        return
    }
    project.fileTree(destinationDir).visitFiles {
        if (it.name.endsWith(".class") && it.isFile) {
            action(it)
        }
    }
}


private val fileLogger = getGradleLoggerForCurrentClass()
