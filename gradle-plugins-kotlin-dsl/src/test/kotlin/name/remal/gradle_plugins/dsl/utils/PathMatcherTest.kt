package name.remal.gradle_plugins.dsl.utils

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class PathMatcherTest {

    @Test
    fun test() {
        assertTrue(PathMatcher("com/test.jsp").matches("com/test.jsp"))
        assertFalse(PathMatcher("com/test.jsp").matches("com/TEST.jsp"))
        assertTrue(PathMatcher("com/test.jsp", false).matches("com/TEST.jsp"))
        assertFalse(PathMatcher("com/test.jsp").matches("com/tast.jsp"))

        assertTrue(PathMatcher("com/t?st.jsp").matches("com/test.jsp"))
        assertTrue(PathMatcher("com/t?st.jsp").matches("com/tast.jsp"))
        assertFalse(PathMatcher("com/t?st.jsp").matches("com/tst.jsp"))
        assertFalse(PathMatcher("com/t?st.jsp").matches("com/taaast.jsp"))

        assertTrue(PathMatcher("com/t*st.jsp").matches("com/tst.jsp"))
        assertTrue(PathMatcher("com/t*st.jsp").matches("com/test.jsp"))
        assertTrue(PathMatcher("com/t*st.jsp").matches("com/teeest.jsp"))
        assertFalse(PathMatcher("com/t*st.jsp").matches("com/t/st.jsp"))

        assertTrue(PathMatcher("com/**/test.jsp").matches("com/test.jsp"))
        assertTrue(PathMatcher("com/**/test.jsp").matches("com/inner/test.jsp"))
        assertTrue(PathMatcher("com/a**/test.jsp").matches("com/a/test.jsp"))
        assertTrue(PathMatcher("com/a**/test.jsp").matches("com/abc/test.jsp"))
        assertFalse(PathMatcher("com/a**/test.jsp").matches("com/abc/inner/test.jsp"))

        assertTrue(PathMatcher("**/test.jsp").matches("test.jsp"))
        assertTrue(PathMatcher("**/test.jsp").matches("com/test.jsp"))
        assertTrue(PathMatcher("**//test.jsp").matches("test.jsp"))
        assertTrue(PathMatcher("**//test.jsp").matches("com/test.jsp"))
        assertTrue(PathMatcher("**//test.jsp").matches("com//test.jsp"))

        assertTrue(PathMatcher("com/").matches("com/test.jsp"))
        assertTrue(PathMatcher("com/").matches("com/inner/test.jsp"))
        assertFalse(PathMatcher("com").matches("com/test.jsp"))
        assertFalse(PathMatcher("com").matches("com/inner/test.jsp"))
    }

}
