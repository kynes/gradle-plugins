package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.EnvironmentVariable
import name.remal.gradle_plugins.dsl.EnvironmentVariable.EnvironmentVariables
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.invoke
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.nullIfEmpty
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.ArtifactRepository
import java.lang.System.getProperty
import java.lang.System.getenv

@EnvironmentVariables(
    value = [
        EnvironmentVariable("GRADLE_PUBLISH_KEY", description = "Gradle Plugin Portal publish key"),
        EnvironmentVariable("GRADLE_PUBLISH_SECRET", description = "Gradle Plugin Portal publish secret")
    ],
    pluginClass = MavenPublishGradlePluginPortalPlugin::class
)
class RepositoryHandlerGradlePluginPortalPublishExtension(private val repositories: RepositoryHandler, private val project: Project) {

    @JvmOverloads
    fun gradlePluginPortalPublish(configurer: Action<GradlePluginPortalRepository> = Action {}): ArtifactRepository {
        val repo = GradlePluginPortalRepositoryDefault(project)
        repo.name = "gradlePluginPortal"

        repo.publishKey = getProperty("gradle.publish.key").nullIfEmpty()
            ?: project.findProperty("gradle.publish.key").unwrapProviders()?.toString().nullIfEmpty()
                ?: getenv("GRADLE_PUBLISH_KEY").nullIfEmpty()
        repo.publishSecret = getProperty("gradle.publish.secret").nullIfEmpty()
            ?: project.findProperty("gradle.publish.secret").unwrapProviders()?.toString().nullIfEmpty()
                ?: getenv("GRADLE_PUBLISH_SECRET").nullIfEmpty()

        configurer(repo)

        var name = repo.name
        var counter = 1
        while (name in repositories) {
            name = repo.name + (++counter)
        }
        repo.name = name
        repositories.add(repo)
        repo.isPartOfContainer = true
        return repo
    }

    fun gradlePluginPortalPublish(@DelegatesTo(GradlePluginPortalRepository::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = gradlePluginPortalPublish(configurer.toConfigureAction())

}
