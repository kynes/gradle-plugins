package name.remal.gradle_plugins.plugins.dependencies

import name.remal.concurrentSetOf
import name.remal.default
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.beforeResolve
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.matches
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.plugins.ExtensionContainer

@Plugin(
    id = "name.remal.dependency-versions",
    description = "Plugin that provides 'dependencyVersions' extension.",
    tags = ["common", "dependencies"]
)
class DependencyVersionsPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'dependencyVersions' extension`(dependencies: DependencyHandler, configurations: ConfigurationContainer, repositories: RepositoryHandler) {
        create("dependencyVersions", DependencyVersionsExtension::class.java, dependencies, configurations, repositories)
    }

    @PluginAction
    fun ConfigurationContainer.`Apply Nebula Gradle Resolution Rules`() {
        all {
            it.beforeResolve {
                it.resolutionStrategy {
                    NebulaResolutionRules.addDependencySubstitution(it.dependencySubstitution)
                    NebulaResolutionRules.addComponentSelection(it.componentSelection)
                }
            }
        }
    }

    @PluginAction(order = Int.MAX_VALUE)
    fun ConfigurationContainer.`Reject dynamic first-level dependencies with invalid version tokens`(extensions: ExtensionContainer) {
        val dependencyVersions = extensions[DependencyVersionsExtension::class.java]
        all {
            it.beforeResolve {
                val rejectMatchers = dependencyVersions.rejectVersions.mapTo(mutableSetOf(), ::DependencyNotationMatcher)
                val allowMatchers = dependencyVersions.allowAllVersionsFor.mapTo(mutableSetOf(), ::DependencyNotationMatcher)

                it.resolutionStrategy {
                    val dynamicNotations = concurrentSetOf<DependencyNotation>()
                    it.eachDependency {
                        if (it.target.version.default().contains('+')) {
                            dynamicNotations.add(it.target.notation.withOnlyGroupAndModule())
                        }
                    }

                    it.componentSelection {
                        it.all selection@{
                            with(it) {
                                rejectMatchers.firstOrNull { it.matches(candidate) }?.let { matcher ->
                                    reject("Plugin $pluginId: Rejected version: $matcher")
                                    return@selection
                                }

                                if (allowMatchers.any { it.matches(candidate) }) {
                                    return@selection
                                }

                                if (candidate.notation.withOnlyGroupAndModule() in dynamicNotations) {
                                    val invalidToken = dependencyVersions.getFirstInvalidToken(candidate.version)
                                    if (invalidToken != null) {
                                        reject("Plugin $pluginId: invalid version token: $invalidToken")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
