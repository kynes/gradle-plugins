The plugin applies plugin [`name.remal.common-settings`](name.remal.common-settings.md).

&nbsp;

This plugin add output classes processing for all tasks of type [`AbstractCompile`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/compile/AbstractCompile.html). Basically sources of all JVM languages are built by such task.

Classes processing is implemented by services of type [`ClassesProcessor`](#nameremalgradle_pluginsapiclasses_processingclassesprocessor). Such services can be also created by [`ClassesProcessorsGradleTaskFactory`](#nameremalgradle_pluginsapiclasses_processingclassesprocessorsgradletaskfactory) factories.

The services and factories are loaded using [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html). Their class files are accessible using `name.remal:gradle-plugins-api:@gradle-plugins-api.version@` Maven compile-only dependency.

&nbsp;

### `name.remal.gradle_plugins.api.classes_processing.ClassesProcessor`
| Method | Description
| --- | --- |
| <code>void process(byte[] bytecode, <a href="#nameremalgradle_pluginsapiclasses_processingbytecodemodifier">BytecodeModifier</a> bytecodeModifier, String className, String resourceName, <a href="#nameremalgradle_pluginsapiclasses_processingprocesscontext">ProcessContext</a> context)</code> | This method is executed for each being processed class file. |
| `int getStage()` | Returns stage number for the processor. Processors are sorted by stage first and then by order. |
| `int getOrder()` | Returns order number within stage for the processor. Processors are sorted by stage first and then by order. |

### `name.remal.gradle_plugins.api.classes_processing.BytecodeModifier`
| Method | Description
| --- | --- |
| `void modify(byte[] modifiedBytecode)` | Write modified bytecode. |

### `name.remal.gradle_plugins.api.classes_processing.ProcessContext`
| Property | Type | Description
| --- | :---: | --- |
| `classesDir` | `File` | Class files root directory. |
| `classpath` | <nobr>`List<File>`</nobr> | Compilation classpath. |
| `classpathClassLoader` | `ClassLoader` | Compilation classpath class loader. |

| Method | Description
| --- | --- |
| `boolean doesResourceExist(String relativePath)` | Checks if resource exists in `classesDir`. |
| `byte[]? readBinaryResource(String relativePath)` | Read binary resource from `classesDir`. Returns `null` if it doesn't exist. |
| `String? readTextResource(String relativePath)` | Read text resource from `classesDir`. Returns `null` if it doesn't exist. |
| `void writeBinaryResource(String relativePath, byte[] content)` | Write binary resource to `classesDir`. |
| `void writeTextResource(String relativePath, String content)` | Write text resource to `classesDir` using UTF-8 charset. |
| `void appendTextResource(String relativePath, String content)` | Append text resource to `classesDir` using UTF-8 charset. |
| `void writeService(String serviceName, String implementationName)` | Add implementation class name to corresponding  file in `META-INF/services/<service-name>`. It's used for [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html). |
| `void writeService(Class<?> serviceClass, String implementationName)` | Add implementation class name to corresponding  file in `META-INF/services/<service-name>`. It's used for [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html). |
| `void writeService(String serviceName, Class<?> implementationClass)` | Add implementation class name to corresponding  file in `META-INF/services/<service-name>`. It's used for [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html). |
| `void writeService(Class<?> serviceClass, Class<?> implementationClass)` | Add implementation class name to corresponding  file in `META-INF/services/<service-name>`. It's used for [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html). |
| `boolean doesClasspathResourceExist(String relativePath)` | Checks if resource exists in `classpath`. |
| `byte[]? readClasspathBinaryResource(String relativePath)` | Read binary resource from `classpath`. Returns `null` if it doesn't exist. |
| `String? readClasspathTextResource(String relativePath)` | Read text resource from `classpath`. Returns `null` if it doesn't exist. |

### `name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory`
| Method | Description
| --- | --- |
| <code>List&lt;<a href="#nameremalgradle_pluginsapiclasses_processingclassesprocessor">ClassesProcessor</a>> createClassesProcessors(<a href="https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/compile/AbstractCompile.html">AbstractCompile</a> compileTask)</code> | Create a list of [`ClassesProcessor`](#nameremalgradle_pluginsapiclasses_processingclassesprocessor) instances for specified [`AbstractCompile`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/compile/AbstractCompile.html) task. |
