**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies [`name.remal.test-source-sets`](name.remal.test-source-sets.md) plugin.

&nbsp;

* It disables all [`AbstractTestTask`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/AbstractTestTask.html) tasks.
* It disables all [`AssertJGenerate`](name.remal.assertj-generator.md#assertj-generate) tasks.
* It disables `check` task.
* It disables all tasks that compiles/processes [`testSourceSets`](name.remal.test-source-sets.md#test-source-sets).
