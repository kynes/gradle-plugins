The plugin applies these plugins:

* [`java`](https://docs.gradle.org/current/userguide/java_plugin.html)
* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.test-source-sets`](name.remal.test-source-sets.md)

&nbsp;

This plugin configures [AssertJ Assertions Generator](https://joel-costigliola.github.io/assertj/assertj-assertions-generator.html). For AssertJ Assertions Generator classpath resolution it creates `assertjGenerator` configuration with `org.assertj:assertj-assertions-generator:@assertj-assertions-generator.version@` default dependency.

&nbsp;

<a id="assertj-generate"></a>
The plugin creates `assertjGenerate` task of type [`AssertJGenerate`](#nameremalgradle_pluginspluginsassertjassertjgenerate). Output of all tasks of this type will become sources for all [`testSourceSets`](name.remal.test-source-sets.md#testSourceSets).

&nbsp;

### `name.remal.gradle_plugins.plugins.assertj.AssertJGenerate`
| Property | Type | Description
| --- | :---: | --- |
| `classNames` | <nobr>`MutableSet<String>`</nobr> | Class names to generate AssertJ assertions from. |
| `packageNames` | <nobr>`MutableSet<String>`</nobr> | Package names to generate AssertJ assertions from. |
| `includes` | <nobr>`MutableSet<String>`</nobr> | ANT like include patterns of class files to generate AssertJ assertions from. |
| `excludes` | <nobr>`MutableSet<String>`</nobr> | ANT like excludes patterns of class files to generate AssertJ assertions from. |
| `assertjGeneratorClasspath` | <nobr>[`FileCollection`](https://docs.gradle.org/current/javadoc/org/gradle/api/file/FileCollection.html)</nobr> | AssertJ Assertions Generator classpath. `assertjGenerator` configuration by default. |

| Method | Description
| --- | --- |
| `void className(String... classNames)` | Add class names to generate AssertJ assertions from. |
| `void className(Iterable<String> classNames)` | Add class names to generate AssertJ assertions from. |
| `void packageName(String... packageNames)` | Add package names to generate AssertJ assertions from. |
| `void packageName(Iterable<String> packageNames)` | Add package names to generate AssertJ assertions from. |
| `void include(String... includes)` | Add ANT like include patterns of class files to generate AssertJ assertions from. |
| `void include(Iterable<String> includes)` | Add ANT like include patterns of class files to generate AssertJ assertions from. |
| `void exclude(String... excludes)` | Add ANT like exclude patterns of class files to generate AssertJ assertions from. |
| `void exclude(Iterable<String> excludes)` | Add ANT like exclude patterns of class files to generate AssertJ assertions from. |
