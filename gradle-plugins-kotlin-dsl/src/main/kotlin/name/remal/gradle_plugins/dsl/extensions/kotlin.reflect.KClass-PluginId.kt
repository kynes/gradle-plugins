package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.PluginId
import kotlin.reflect.KClass

fun <T : PluginId> KClass<T>.getOrInstantiate(): T {
    try {
        this.objectInstance?.let { return it }
    } catch (ignored: IllegalAccessException) {
        // do nothing
    }

    return this.java.getDeclaredConstructor().makeAccessible().newInstance()
}
