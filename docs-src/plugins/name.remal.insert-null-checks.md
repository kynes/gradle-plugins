**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies these plugins:

* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.classes-processing`](name.remal.classes-processing.md)

&nbsp;

This plugin processes all compiled JVM classes and for all non-null parameters adds `Objects.requireNonNull()` check.

The plugin support all annotations listed [here](https://checkerframework.org/manual/#nullness-related-work). It's done by comparing simple class name, so exact package name can be anything.

JSR 305 annotations are also supported including [`javax.annotation.meta.TypeQualifierNickname`](https://www.javadoc.io/doc/com.google.code.findbugs/jsr305/latest/javax/annotation/meta/TypeQualifierNickname.html) and [`javax.annotation.meta.TypeQualifierDefault`](https://www.javadoc.io/doc/com.google.code.findbugs/jsr305/latest/javax/annotation/meta/TypeQualifierDefault.html). For example package-info annotation [`org.springframework.lang.NonNullApi`](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/lang/NonNullApi.html) is handled correctly for all classes inside the package.

JDT annotations are also supported including [`org.eclipse.jdt.annotation.NonNullByDefault`](https://javadoc.io/doc/org.eclipse.jdt/org.eclipse.jdt.annotation/latest/org/eclipse/jdt/annotation/NonNullByDefault.html).

The plugin doesn't process Kotlin classes, as Kotlin produces null checks itself.
