package name.remal.gradle_plugins.plugins.ide.idea

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.getOrCreateChild
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import name.remal.setAttribute
import name.remal.setAttributes
import org.gradle.api.Action
import org.jdom2.Element

interface IdeaSettings {

    fun configureIDEAComponent(componentName: String, ideaDirRelativePath: String?, action: Action<Element>)

    fun configureIDEAComponent(componentName: String, ideaDirRelativePath: String?, @DelegatesTo(Element::class, strategy = DELEGATE_FIRST) closure: Closure<*>) = configureIDEAComponent(
        componentName,
        ideaDirRelativePath,
        closure.toConfigureAction()
    )


    fun removeIDEAComponent(componentName: String) = configureIDEAComponent(componentName, null, Action {
        it.detach()
    })


    fun entryPointAnnotation(annotationCanonicalName: String) = entryPointAnnotations(annotationCanonicalName)

    fun entryPointAnnotations(vararg annotationCanonicalNames: String) = entryPointAnnotations(annotationCanonicalNames.toList())

    fun entryPointAnnotations(annotationCanonicalNames: Collection<String>) {
        if (annotationCanonicalNames.isEmpty()) return
        configureIDEAComponent("EntryPointsManager", "misc.xml", Action { componentElement ->
            addToStringsList(componentElement, annotationCanonicalNames)
        })
    }


    fun writeAnnotation(annotationCanonicalName: String) = writeAnnotations(annotationCanonicalName)

    fun writeAnnotations(vararg annotationCanonicalNames: String) = writeAnnotations(annotationCanonicalNames.toList())

    fun writeAnnotations(annotationCanonicalNames: Collection<String>) {
        if (annotationCanonicalNames.isEmpty()) return
        configureIDEAComponent("EntryPointsManager", "misc.xml", Action { componentElement ->
            val writeAnnotationsElement = componentElement.getOrCreateChild("writeAnnotations")

            val annotations = writeAnnotationsElement.getChildren("writeAnnotation").asSequence()
                .map { it.getAttributeValue("name") }
                .filterNotNull()
                .toMutableSet()
            annotations.addAll(annotationCanonicalNames)

            writeAnnotationsElement.removeContent()
            annotations.forEach { value ->
                writeAnnotationsElement.addContent(
                    Element("writeAnnotation").setAttribute("name", value)
                )
            }
        })
    }


    fun nullableAnnotation(annotationCanonicalName: String) = nullableAnnotations(annotationCanonicalName)

    fun nullableAnnotations(vararg annotationCanonicalNames: String) = nullableAnnotations(annotationCanonicalNames.toList())

    fun nullableAnnotations(annotationCanonicalNames: Collection<String>) {
        if (annotationCanonicalNames.isEmpty()) return
        configureIDEAComponent("NullableNotNullManager", "misc.xml", Action { componentElement ->
            addToStringsList(componentElement.getOrCreateChild("option", mapOf("name" to "myNullables")).getOrCreateChild("value"), annotationCanonicalNames)
        })
    }


    fun notNullAnnotation(annotationCanonicalName: String) = notNullAnnotations(annotationCanonicalName)

    fun notNullAnnotations(vararg annotationCanonicalNames: String) = notNullAnnotations(annotationCanonicalNames.toList())

    fun notNullAnnotations(annotationCanonicalNames: Collection<String>) {
        if (annotationCanonicalNames.isEmpty()) return
        configureIDEAComponent("NullableNotNullManager", "misc.xml", Action { componentElement ->
            addToStringsList(componentElement.getOrCreateChild("option", mapOf("name" to "myNotNulls")).getOrCreateChild("value"), annotationCanonicalNames)
        })
    }


    fun addDefaultAnnotationProcessingProfile() {
        configureIDEAComponent("CompilerConfiguration", "compiler.xml", Action { componentElement ->
            val annotationProcessingElement = componentElement.getOrCreateChild("annotationProcessing")
            if (annotationProcessingElement.getChild("profile") == null) {
                annotationProcessingElement.addContent(
                    Element("profile").setAttributes(
                        mapOf(
                            "default" to "true",
                            "name" to "Default",
                            "enabled" to "true"
                        )
                    )
                )
            }
        })
    }

}

private fun addToStringsList(parentElement: Element, valuesToAdd: Collection<String>) {
    if (valuesToAdd.isEmpty()) return

    val listElement = parentElement.getOrCreateChild("list")

    val values = listElement.getChildren("item").asSequence()
        .map { it.getAttributeValue("itemvalue") }
        .filterNotNull()
        .toMutableSet()
    values.addAll(valuesToAdd)

    listElement.setAttribute("size", values.size)
    listElement.removeContent()
    values.forEachIndexed { i, value ->
        listElement.addContent(
            Element("item")
                .setAttribute("index", i)
                .setAttribute("class", "java.lang.String")
                .setAttribute("itemvalue", value)
        )
    }
}
