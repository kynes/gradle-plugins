package name.remal.gradle_plugins.plugins.classes_processing.processors.auto_service

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.dsl.extensions.toHasEntries
import org.gradle.api.tasks.compile.AbstractCompile

@AutoService
class AutoServiceClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {

    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.classpath.toHasEntries().containsClass(AutoService::class.java)) return emptyList()
        return listOf(AutoServiceClassesProcessor())
    }

}
