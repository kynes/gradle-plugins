package name.remal.gradle_plugins.plugins.code_quality.sonarqube

import name.remal.gradle_plugins.dsl.PluginId

object SonarQubePluginId : PluginId("org.sonarqube")
