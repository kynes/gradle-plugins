package name.remal.gradle_plugins.plugins.common

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.joinToString
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.wrapper.Wrapper

@Plugin(
    id = "name.remal.gradle-wrapper-settings",
    description = "Plugin that configures Gradle 'wrapper' task",
    tags = ["common"]
)
class GradleWrapperSettingsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun TaskContainer.`Remove comments in generated properties file by Wrapper task`() {
        all(Wrapper::class.java) { task ->
            task.doLast {
                task.propertiesFile.writeText(
                    task.propertiesFile.readLines().stream()
                        .map { it.substringBefore('#') }
                        .map(String::trim)
                        .filter(String::isNotEmpty)
                        .joinToString("\n") + '\n'
                )
            }
        }
    }

}
