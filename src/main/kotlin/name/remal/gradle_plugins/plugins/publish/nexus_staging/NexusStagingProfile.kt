package name.remal.gradle_plugins.plugins.publish.nexus_staging

data class NexusStagingProfile(
    val id: String,
    val name: String,
    val repositoryType: String,
    val promotionTargetRepository: String,
    val targetGroups: List<String> = emptyList()
)
