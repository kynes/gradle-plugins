package name.remal.gradle_plugins.plugins.jdk_cross_compilation

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.doSetupIf
import name.remal.gradle_plugins.dsl.extensions.sourceJavaVersion
import name.remal.gradle_plugins.plugins.groovy.GroovyPluginId
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmPluginId
import name.remal.loadServicesList
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.AbstractCompile
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.compile.JavaCompile
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

@Deprecated("Use Gradle JVM toolchains functionality: https://docs.gradle.org/current/userguide/toolchains.html")
@Plugin(
    id = "name.remal.jdk-cross-compilation",
    description = "Plugin that configures JVM languages cross-compilation functionality.",
    tags = ["cross-compilation"]
)
@WithPlugins(JavaPluginId::class)
class JdkCrossCompilationPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val jdkInfoProviders = loadServicesList(JdkInfoProvider::class.java)

        private fun getJdkInfoOrNull(javaVersion: JavaVersion): JdkInfo? {
            for (jdkInfoProvider in jdkInfoProviders) {
                jdkInfoProvider.provide(javaVersion)?.let { return it }
            }
            return null
        }

        private fun getJdkInfo(javaVersion: JavaVersion): JdkInfo {
            return getJdkInfoOrNull(javaVersion) ?: throw IllegalStateException("JDK can't be found for version $javaVersion")
        }
    }

    @PluginAction
    fun TaskContainer.`Setup JavaCompile tasks`(project: Project) {
        all(JavaCompile::class.java) {
            it.doSetupIf(AbstractCompile::canNotBeCompiledByCurrentJvm) { task ->
                val sourceJdkInfo = getJdkInfo(task.sourceJavaVersion)
                task.options.bootstrapClasspath = project.files(sourceJdkInfo.bootstrapClasspath)
            }
        }
    }

    @PluginAction
    @WithPlugins(KotlinJvmPluginId::class)
    fun TaskContainer.`Setup KotlinCompile tasks`() {
        all(KotlinCompile::class.java) {
            it.doSetupIf(AbstractCompile::canNotBeCompiledByCurrentJvm) { task ->
                val jdkInfo = getJdkInfo(task.sourceJavaVersion)
                task.kotlinOptions.jdkHome = jdkInfo.jdkHome.path
            }
        }
    }

    @PluginAction
    @WithPlugins(GroovyPluginId::class)
    fun TaskContainer.`Setup GroovyCompile tasks`(project: Project) {
        all(GroovyCompile::class.java) {
            it.doSetupIf(AbstractCompile::canNotBeCompiledByCurrentJvm) { task ->
                val sourceJdkInfo = getJdkInfo(task.sourceJavaVersion)
                task.options.bootstrapClasspath = project.files(sourceJdkInfo.bootstrapClasspath)
            }
        }
    }

}

private val AbstractCompile.canNotBeCompiledByCurrentJvm: Boolean get() = sourceJavaVersion != JavaVersion.current()
