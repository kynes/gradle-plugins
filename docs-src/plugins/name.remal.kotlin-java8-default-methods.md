**This plugin works only if [`kotlin`](https://kotlinlang.org/docs/reference/using-gradle.html) plugin is applied.**

The plugin applies these plugins:

* [`name.remal.kotlin-settings`](name.remal.kotlin-settings.md)
* [`name.remal.classes-processing`](name.remal.classes-processing.md)

&nbsp;

This plugins enable class files processing. It processes all compiled Kotlin interfaces and creates Java 8 default methods that delegate invocation to Kotlin's default methods.

Basically the plugin works like annotating every interface with `@JvmDefault` and passing `-Xjvm-default=compatibility` compiler parameter.
