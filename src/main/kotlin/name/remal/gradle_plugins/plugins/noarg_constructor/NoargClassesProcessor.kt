package name.remal.gradle_plugins.plugins.noarg_constructor

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import name.remal.CLASS_FILE_NAME_SUFFIX
import name.remal.accept
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.isConstructor
import name.remal.isNotStaticInnerClass
import name.remal.isProtected
import name.remal.isPublic
import name.remal.set
import org.gradle.api.tasks.compile.AbstractCompile
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassReader.SKIP_CODE
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes.ACC_ANNOTATION
import org.objectweb.asm.Opcodes.ACC_ENUM
import org.objectweb.asm.Opcodes.ACC_INTERFACE
import org.objectweb.asm.Opcodes.ACC_MODULE
import org.objectweb.asm.Opcodes.ACC_PROTECTED
import org.objectweb.asm.Opcodes.ACC_SYNTHETIC
import org.objectweb.asm.Opcodes.ALOAD
import org.objectweb.asm.Opcodes.INVOKESPECIAL
import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.InsnNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.MethodInsnNode
import org.objectweb.asm.tree.MethodNode
import org.objectweb.asm.tree.VarInsnNode
import java.util.concurrent.atomic.AtomicReference

class NoargClassesProcessor : ClassesProcessor {

    private val contextRef = AtomicReference<ProcessContext>()

    private val hasPublicOrProtectedNoargCtorCache: LoadingCache<String, Boolean> = CacheBuilder.newBuilder()
        .build<String, Boolean>(object : CacheLoader<String, Boolean>() {
            override fun load(relativePath: String): Boolean {
                if ("java/lang/Object.class" == relativePath) return true
                val context = contextRef.get()
                val bytecode = context.readBinaryResource(relativePath)
                    ?: context.readClasspathBinaryResource(relativePath)
                    ?: return false
                val classNode = ClassNode().also { ClassReader(bytecode).accept(it, SKIP_CODE) }
                return classNode.hasPublicOrProtectedNoargConstructor
            }
        })

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        contextRef.compareAndSet(null, context)

        val classReader = ClassReader(bytecode)
        val classNode = ClassNode().also { classReader.accept(it) }

        if ((classNode.access and ACC_INTERFACE) != 0) return
        if ((classNode.access and ACC_ANNOTATION) != 0) return
        if ((classNode.access and ACC_ENUM) != 0) return
        if ((classNode.access and ACC_MODULE) != 0 || classNode.module != null) return
        if (classNode.isNotStaticInnerClass) return
        if (classNode.outerMethod != null || classNode.outerMethodDesc != null) return

        if (classNode.hasNoargConstructor) {
            hasPublicOrProtectedNoargCtorCache.put(resourceName, true)
            return
        }

        if (!hasPublicOrProtectedNoargCtorCache[classNode.superName + CLASS_FILE_NAME_SUFFIX]) {
            hasPublicOrProtectedNoargCtorCache.put(resourceName, false)
            return
        }

        hasPublicOrProtectedNoargCtorCache.put(resourceName, true)

        classNode.methods.add(MethodNode(ACC_PROTECTED or ACC_SYNTHETIC, "<init>", "()V", null, null).also { method ->
            method.invisibleAnnotations = mutableListOf<AnnotationNode>().also { annotations ->
                annotations.add(AnnotationNode("Ledu/umd/cs/findbugs/annotations/SuppressFBWarnings;"))
                annotations.add(AnnotationNode("Ljavax/annotation/Generated;").apply {
                    set("value", listOf(NoargClassesProcessor::class.java.name))
                })
            }

            method.instructions = InsnList().also {
                it.add(LabelNode())
                it.add(VarInsnNode(ALOAD, 0))
                it.add(MethodInsnNode(INVOKESPECIAL, classNode.superName, method.name, method.desc, false))
                it.add(InsnNode(RETURN))
            }
            method.maxLocals = 1
            method.maxStack = 1
        })

        val classWriter = ClassWriter(classReader, 0)
        classNode.accept(classWriter)
        bytecodeModifier.modify(classWriter.toByteArray())
    }


    private val ClassNode.noargConstructorNode get() = methods?.firstOrNull { it.isConstructor && it.desc == "()V" }
    private val ClassNode.hasNoargConstructor get() = noargConstructorNode != null
    private val ClassNode.hasPublicOrProtectedNoargConstructor get() = noargConstructorNode.let { it != null && (it.isPublic || it.isProtected) }

}


@AutoService
class NoargClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {
    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.project.isPluginAppliedAndNotDisabled(NoargConstructorPlugin::class.java)) return emptyList()
        return listOf(NoargClassesProcessor())
    }
}
