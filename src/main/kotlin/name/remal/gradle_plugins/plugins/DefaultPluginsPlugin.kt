package name.remal.gradle_plugins.plugins

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.plugins.ci.AllCIPlugin
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingPlugin
import name.remal.gradle_plugins.plugins.code_quality.sonarqube.SonarQubeSettingsPlugin
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.common.FilteringSettingsPlugin
import name.remal.gradle_plugins.plugins.dependencies.ComponentCapabilitiesPlugin
import name.remal.gradle_plugins.plugins.dependencies.ComponentMetadataPlugin
import name.remal.gradle_plugins.plugins.dependencies.DefaultDependencyVersionsPlugin
import name.remal.gradle_plugins.plugins.dependencies.DependenciesModifierPlugin
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsPlugin
import name.remal.gradle_plugins.plugins.dependencies.TransitiveDependenciesPlugin
import name.remal.gradle_plugins.plugins.groovy.GroovySettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaSettingsPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinJava8DefaultMethodsPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinJsSettingsPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmSettingsPlugin
import name.remal.gradle_plugins.plugins.prohibited_classes.ProhibitedClassesPlugin
import name.remal.gradle_plugins.plugins.publish.MavenPublishSettingsPlugin
import name.remal.gradle_plugins.plugins.publish.bintray.MavenPublishBintrayPlugin
import name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal.MavenPublishGradlePluginPortalPlugin
import name.remal.gradle_plugins.plugins.publish.ossrh.MavenPublishOssrhPlugin
import name.remal.gradle_plugins.plugins.signing.SigningSettingsPlugin
import name.remal.gradle_plugins.plugins.testing.RandomizeTestClassesPlugin
import name.remal.gradle_plugins.plugins.testing.TestSettingsPlugin

@Plugin(
    id = "name.remal.default-plugins",
    description = "Plugin that applies many different plugins by default. See documentation for full applied plugins list.",
    tags = ["common", "default"]
)
@ApplyPluginClasses(
    CommonSettingsPlugin::class,
    RootProjectDefaultPluginsPlugin::class,
    BuildSrcProjectDefaultPluginsPlugin::class,

    DependenciesModifierPlugin::class,
    DefaultDependencyVersionsPlugin::class,
    TransitiveDependenciesPlugin::class,
    DependencyVersionsPlugin::class,
    ComponentCapabilitiesPlugin::class,
    ComponentMetadataPlugin::class,

    FilteringSettingsPlugin::class,

    JavaSettingsPlugin::class,

    KotlinJvmSettingsPlugin::class,
    KotlinJava8DefaultMethodsPlugin::class,
    KotlinJsSettingsPlugin::class,

    GroovySettingsPlugin::class,

    ClassesProcessingPlugin::class,
    ProhibitedClassesPlugin::class,

    TestSettingsPlugin::class,
    RandomizeTestClassesPlugin::class,

    SonarQubeSettingsPlugin::class,

    SigningSettingsPlugin::class,
    MavenPublishSettingsPlugin::class,
    MavenPublishBintrayPlugin::class,
    MavenPublishOssrhPlugin::class,
    MavenPublishGradlePluginPortalPlugin::class,

    AllCIPlugin::class
)
class DefaultPluginsPlugin : BaseReflectiveProjectPlugin()
