package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_6_1
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.util.GradleVersion
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class GroovyCompileOptionsExtensionsTest : BaseProjectTest() {

    @Test
    fun parameters() {
        val task = project.tasks.createWithUniqueName(GroovyCompile::class.java)
        val options = task.groovyOptions
        if (GradleVersion.current() >= GRADLE_VERSION_6_1) {
            assertEquals(false, options.parameters)
            options.parameters = true
            assertEquals(true, options.parameters)
            options.parameters = false
            assertEquals(false, options.parameters)

        } else {
            assertNull(options.parameters)
            options.parameters = true
            assertNull(options.parameters)
            options.parameters = false
            assertNull(options.parameters)
        }
    }

}
