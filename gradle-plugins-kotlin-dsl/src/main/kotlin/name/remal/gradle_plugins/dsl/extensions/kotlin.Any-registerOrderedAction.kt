package name.remal.gradle_plugins.dsl.extensions

import name.remal.KotlinAllOpen
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.uncheckedCast
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong

internal const val DEFAULT_ACTIONS_ORDER = 0

internal fun <T : Any> T.registerOrderedAction(scope: String, startActionsConfigurer: T.(execute: () -> Unit) -> Unit, superOrder: Int, order: Int, action: (obj: T) -> Unit) {
    val scopedOrderedActions = convention.getOrCreateWithAutoName(ScopedOrderedActions::class.java).uncheckedCast<ScopedOrderedActions<T>>()
    val orderedActions = scopedOrderedActions.actions.computeIfAbsent(scope) {
        val actions = OrderedActions<T>()
        startActionsConfigurer(this, {
            actions.run(this)
        })
        return@computeIfAbsent actions
    }
    orderedActions.addAction(OrderedAction(superOrder, order, action))
}

private class OrderedAction<T>(
    val superOrder: Int,
    val order: Int,
    val action: (obj: T) -> Unit
) : Comparable<OrderedAction<T>> {

    companion object {
        private val orderedActionsCount = AtomicLong()
    }

    private val index = orderedActionsCount.getAndIncrement()

    override fun compareTo(other: OrderedAction<T>): Int {
        superOrder.compareTo(other.superOrder).let { if (it != 0) return it }
        order.compareTo(other.order).let { if (it != 0) return it }
        return index.compareTo(other.index)
    }

}

private class OrderedActions<T> {
    private val actions: MutableList<OrderedAction<T>> = mutableListOf()
    private val isExecuted = AtomicBoolean()

    fun addAction(action: OrderedAction<T>) {
        if (isExecuted.get()) {
            getGradleLogger(OrderedActions::class.java).warn("Actions have been already executed")
            return
        }

        actions.add(action)
    }

    fun run(obj: T) {
        if (isExecuted.compareAndSet(false, true)) {
            val actions = this.actions.sorted()
            this.actions.clear()
            actions.forEach { it.action(obj) }
        }
    }

}

@KotlinAllOpen
private class ScopedOrderedActions<T> {
    val actions: MutableMap<String, OrderedActions<T>> = mutableMapOf()
}
