package name.remal.gradle_plugins.dsl

import org.gradle.api.Plugin
import org.gradle.api.Project

typealias ProjectPluginClass = Class<out Plugin<out Project>>
