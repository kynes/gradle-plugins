package name.remal.gradle_plugins.plugins.code_quality.detekt

import name.remal.gradle_plugins.dsl.BuildTask
import java.io.File

@BuildTask
class WriteDefaultDetektConfig : BaseHandleDefaultDetektConfig() {

    public override var targetFile: File = project.rootDir.resolve("gradle/detekt/config.yml")

    init {
        doFirst {
            logger.lifecycle("Writing default Detekt config to $targetFile")
        }
    }

}
