**This plugin works only if [`signing`](https://docs.gradle.org/current/userguide/signing_plugin.html) plugin is applied.**

&nbsp;

This plugin helps to configure [`signing`](https://docs.gradle.org/current/userguide/signing_plugin.html) plugin.

The plugin:

* Sets `signing.keyId` property from `SIGNING_KEY_ID` environment variable.
* Sets `signing.password` property from `SIGNING_PASSWORD` environment variable.
* Sets `signing.secretKeyRingFile` property to a temp file with Base64-encoded content of `SIGNING_SECRET_KEY_RING_BASE64` and `SIGNING_SECRET_KEY_RING_BASE64_1`..`SIGNING_SECRET_KEY_RING_BASE64_9` environment variables.
* Sets `signing.secretKeyRingFile` property from `SIGNING_SECRET_KEY_RING_FILE` environment variable.
* Supports `classpath:` prefix for `signing.secretKeyRingFile` property. It uses classpath of current build script.

If [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) plugin is applied, the plugin sign every [`MavenPublication`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html).
