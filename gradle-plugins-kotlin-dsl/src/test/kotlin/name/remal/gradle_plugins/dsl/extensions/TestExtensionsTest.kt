package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class TestExtensionsTest : BaseProjectTest() {

    @Test
    fun isTestFrameworkSet() {
        project.applyPlugin("java")
        val testTask = project.tasks[org.gradle.api.tasks.testing.Test::class.java, "test"]
        assertFalse(testTask.isTestFrameworkSet)
        testTask.useJUnit()
        assertTrue(testTask.isTestFrameworkSet)
    }

}
