package name.remal.gradle_plugins.dsl.extensions

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test

class ClassExtensionsTest {

    @Test
    fun unwrapGradleGeneratedCompatibility() {
        assertEquals(String::class.java, String::class.java.unwrapGradleGenerated())
    }


    @Test
    fun newDelegateOf() {
        val wrapper = Interface::class.java.newDelegateOf(Impl("charSequence"))
        assertEquals("charSequence", wrapper.charSequence.toString())
        assertEquals("default", wrapper.defaultMethod())
        assertEquals("impl", wrapper.toString())
        try {
            wrapper.unsupported()
            fail()
        } catch (e: UnsupportedOperationException) {
            // OK
        }
    }

    class Impl(val charSequence: String) {
        override fun toString() = "impl"
    }

    interface Interface {
        val charSequence: CharSequence
        fun defaultMethod() = "default"
        fun unsupported()
    }

}
