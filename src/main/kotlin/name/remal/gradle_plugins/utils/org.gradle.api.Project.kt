package name.remal.gradle_plugins.utils

import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperties
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.nullIfEmpty
import org.gradle.api.Project

private val versionProperties = getStringProperties("*.version").mapKeys { it.key.substring(0, it.key.length - ".version".length) }

internal fun Project.findVersionProperty(id: String): String? {
    findProperty("$id.version").unwrapProviders()?.toString().nullIfEmpty()?.let { return it }
    versionProperties[id]?.let { return it }
    return null
}

internal fun Project.getVersionProperty(id: String): String {
    return findVersionProperty(id) ?: throw IllegalArgumentException("Version can't be found: $id")
}
